import { MinusOutlined, PlusOutlined, ReloadOutlined } from '@ant-design/icons';
import { addUserCredit, getCreditAccount } from '@api';
import { openNotification } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Button, Card, Col, Divider, Input, InputNumber, Modal, Row, Table, Space } from 'antd';
import classnames from 'classnames';
import React from 'react';

const { Search } = Input;

export default class CreditManagement extends React.Component {
  _subscriptions = [];

  constructor(props) {
    super(props);

    this.state = {
      keyword: '',
      loading: false,
      activeUserName: '',
      creditDialogVisible: false,
      creditAdd: false,
      credit: 0,
      page: 0,
      pageSize: 10,
      accounts: [],
      count: 0,
      creditNote: '',
      _checkCredit: false,
      _checkCreditNote: false,
    };
  }
  componentDidMount() {
    const { page, pageSize } = this.state;
    this.loadAllAccount(page, pageSize);
  }
  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  _reloadPage = () => {
    const { pageSize } = this.state;
    this.loadAllAccount(0, pageSize);
  };

  updateCreditChange = () => {
    const { page, pageSize } = this.state;
    this.loadAllAccount(page, pageSize);
  };

  loadAllAccount = (page, pageSize) => {
    const { keyword } = this.state;
    this._subscriptions.push(
      getCreditAccount(page, pageSize, keyword).subscribe(
        (data) => {
          const { dataSources: dataSource, count: totalCount, page, size } = data;
          const mapDataSources = dataSource
            ? dataSource.map((data, key) => {
              data.key = key;
              return data;
            })
            : [];
          this.setState({
            accounts: mapDataSources,
            count: totalCount,
            page: page,
            pageSize: size,
          });
        },
        (err) => {
          openNotification('error', 'Occurred error when load list lab from server!');
        },
        () => {
          this.setState({ loading: false });
        }
      )
    );
  };

  handleSearchAction = () => {
    const { pageSize } = this.state;
    this.loadAllAccount(0, pageSize);
  };

  handleKeywordOnChange = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    this.setState({ keyword });
  };

  handlePageNumberOnChange = (page) => {
    const { pageSize } = this.state;
    const newPage = page > 0 ? page - 1 : 0;
    this.loadAllAccount(newPage, pageSize);
  };

  onShowSizeChange = (page, pageSize) => {
    this.loadAllAccount(0, pageSize);
  };

  onAddMoreCredit = (userName) => {
    this.setState({
      activeUserName: userName,
      creditDialogVisible: true,
      creditAdd: true,
      credit: 0,
      creditNote: '',
    });
  };
  onDecreaseCredit = (userName) => {
    this.setState({
      activeUserName: userName,
      creditDialogVisible: true,
      creditAdd: false,
      credit: 0,
      creditNote: '',
    });
  };

  handleDialogCancel = () => {
    this.setState({ creditDialogVisible: false });
  };

  handleCreditUpdate = (e) => {
    this.setState({ credit: e });
  };

  handleCreditNoteUpdate = (e) => {
    this.setState({ creditNote: e.target.value });
  };

  _validateField = () => {
    const { credit, creditNote } = this.state;
    let validate = false;
    let checkCredit = false;
    let checkCreditNote = false;
    if (credit <= 0) {
      validate = true;
      checkCredit = true;
    }
    if (creditNote.trim().length <= 0) {
      validate = true;
      checkCreditNote = true;
    }
    this.setState({ _checkCreditNote: checkCreditNote, _checkCredit: checkCredit });
    return validate;
  };

  handleCreditSubmit = () => {
    const { creditAdd, credit, activeUserName, creditNote } = this.state;
    const creditPoint = creditAdd ? credit : -credit;
    if (this._validateField()) {
      return openNotification('error', 'Some Field is Require!!!');
    }
    this.setState({ creditDialogVisible: false });
    this._subscriptions.push(
      addUserCredit(activeUserName, creditPoint, creditNote).subscribe(
        () => {
          openNotification('success', 'Update credit success');
          this.updateCreditChange();
        },
        () => openNotification('error', 'Update credit fail')
      )
    );
  };

  buildCreditDialog = () => {
    const {
      creditAdd,
      activeUserName,
      credit,
      creditNote,
      creditDialogVisible,
      _checkCredit,
      _checkCreditNote,
    } = this.state;
    const creditNumberValid = classnames('credit-number', { 'credit-valid': _checkCredit });
    const creditNoteValid = classnames('credit-notes', { 'credit-valid': _checkCreditNote });
    const title = creditAdd ? 'Add credit for ' + activeUserName : 'Decrease credit for ' + activeUserName;
    return (
      <Modal 
        title={title}
        visible={creditDialogVisible}
        onOk={this.handleCreditSubmit}
        onCancel={this.handleDialogCancel}
      >
        <div className="credit-dialog-container">
          <div className="form-group">
            <label className={creditNumberValid}>
              Credit number
            </label>
            <br />
            <InputNumber min={0} max={500} value={credit} defaultValue={credit} onChange={this.handleCreditUpdate} />
          </div>
          <div className="form-group">
            <span className={creditNoteValid}>
              Notes
            </span>
            <Input defaultValue={creditNote} value={creditNote} onChange={this.handleCreditNoteUpdate} />
          </div>
        </div>
      </Modal>
    );
  };

  creditManagementTable = () => {
    const { accounts, count, pageSize, page, loading } = this.state;
    const from = page * pageSize + 1;

    const pagination = {
      total: count,
      pageSize,
      current: page + 1,
      onChange: this.handlePageNumberOnChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: this.onShowSizeChange,
    };

    const columns = [
      {
        key: 'index',
        width: '8%',
        render: (ext, record, index) => {
          return from + index;
        },
      },
      {
        key: 'user-name',
        title: 'User name',
        align: 'center',
        width: '25%',
        dataIndex: 'userName',
      },
      {
        key: 'balance',
        title: 'Ready to use',
        width: '14%',
        align: 'center',
        render: (text, record) => {
          const credit = record['balance'];
          const creditClassnames = classnames('credit', {
            minus: credit < 0,
          });
          return (
            <p className={creditClassnames}>
              {credit}
            </p>);
        },
      },
      {
        key: 'debit',
        title: 'Added',
        align: 'center',
        width: '14%',
        dataIndex: 'debit',
        render: (text, record, index) => {
          const credit = record['debit'];
          return (
            <p className="credit">
              {credit}
            </p>);
        },
      },
      {
        key: 'credit',
        title: 'Used',
        width: '14%',
        align: 'center',
        render: (text, record) => {
          const credit = record['credit'];

          return (
            <p className="credit minus">
              {'-' + credit}
            </p>);
        },
      },
      {
        key: 'credit',
        title: 'Actions',
        width: '25%',
        align: 'center',
        fixed: 'right',
        render: (text, record) => {
          return (
            <Space>
              <button
                className="btn btn-primary"
                title="Add more credits"
                onClick={() => this.onAddMoreCredit(record.userName)}
              >
                <PlusOutlined />
              </button>
              <button
                className="btn btn-danger"
                title="Decrease credits"
                onClick={() => this.onDecreaseCredit(record.userName)}
              >
                <MinusOutlined />
              </button>
            </Space>
          );
        },
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={accounts}
        columns={columns}
        pagination={pagination}
        loading={loading}
      />
    );
  };
  render() {
    return (
      <div className="credit-management">
        <div className="header">
          <Breadcrumbs list={[{ title: 'My Account', to: '/portals/accounts' }, { title: 'Credit Management' }]} />
          <div className="title">
            Credit Management
          </div>
        </div>
        <Card className="gx-card">
          <Row align="end" className="action">
            <Col span={12}>
              <Space direction="horizontal" align="start" style={{float: 'right'}}>
                <Search
                  placeholder="input search text"
                  onChange={this.handleKeywordOnChange}
                  onSearch={this.handleSearchAction}
                  enterButton={true}
                />
                <Button type="primary" onClick={() => this._reloadPage()}>
                  <ReloadOutlined />
                </Button>
              </Space>
            </Col>
          </Row>
          <Divider />
          {this.creditManagementTable()}
        </Card>
        {this.buildCreditDialog()}
      </div>
    );
  }
}
