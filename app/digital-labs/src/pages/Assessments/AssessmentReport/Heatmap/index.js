import { ArrowRightOutlined } from '@ant-design/icons';
import TableOfContent from '@components/TableOfContent';
import { Col, Row } from 'antd';
import * as React from 'react';

import CategoryHeatmap from './CategoryHeatmap';

const titleToId = (title, tab) => title.split(' ').join('-') + tab;

const Heatmap = (props) => {
  const { report, toc } = props;
  return (
    <Row>
      <Col md={24} xl={20}>
        <div className="heatmap-report report-tab">
          <div className="heatmap-header">
            <div className="heatmap-title tab-title">Cloud Readiness Radar Chart</div>
            <div className="heatmap-caf">
              Assessment scores shown in a a heatmap across the six CAF perspectives.
              <p className="caf-link">
                <a href="#top">
                  Learn more about CAF <ArrowRightOutlined />
                </a>
              </p>
            </div>

            <div className="heatmap-description">
              <div className="description">
                <div className="description-button pass-icon">
                  <span>Green</span>
                </div>
                <div className="content">
                  Questions and Sections that are green indicate a high level of cloud adoption readiness.
                </div>
              </div>
              <div className="description">
                <div className="description-button fail-icon">
                  <span>Yellow</span>
                </div>
                <div className="content">
                  Questions and Sections that are yellow indicate that additional prep-work is recommended. Additional
                  resources to help address areas for improvement are provided in this report.
                </div>
              </div>
            </div>
          </div>
          <div className="heatmap-domain">
            <div className="heatmap-detail">
              {report &&
                report.map((r, index) => (
                  <div className="subdomain" id={titleToId(r.category, '2')} key={index}>
                    <CategoryHeatmap category={`${index + 1}. ${r.category}`} readiness={r.readiness} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </Col>
      <Col className="anchor" xs={0} xl={4}>
        <div className="anchor-detail">
          <TableOfContent items={toc} />
        </div>
      </Col>
    </Row>
  );
};

export default Heatmap;
