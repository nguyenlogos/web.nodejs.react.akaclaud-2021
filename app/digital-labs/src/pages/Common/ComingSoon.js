import ComingSoonContainer from '@components/ComingSoon';
import React from 'react';

const ComingSoon = ({ content, children }) => {
  return (
    <>
      {content == null || content.length === 0
        ? <ComingSoonContainer />
        : (
          <>
            {children}
          </>
        )}
    </>
  );
};

export default ComingSoon;
