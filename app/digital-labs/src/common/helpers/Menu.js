export function filterChildren(child, roles) {
  if (child.acceptRoles == null || child.acceptRoles.length === 0 || child.acceptRoles === '*') {
    return child;
  }

  if (roles == null || roles.length === 0) {
    return null;
  }

  const currentRoles = roles.split(',');
  const matches = child.acceptRoles.split(',').some((r) => currentRoles.indexOf(r) >= 0);

  return matches ? child : null;
}

export function filterAllowItems(items, roles) {
  if (items.length === 0) {
    return [];
  }

  const allowItems = items.map((g) => {
    g.children = g.children.map((c) => filterChildren(c, roles)).filter((c) => c != null);

    return g;
  });

  return allowItems.filter((g) => g.children && g.children.length > 0);
}
