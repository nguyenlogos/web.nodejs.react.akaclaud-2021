import { PlusOutlined } from '@ant-design/icons';
import { getCategories, getLevels } from '@api';
import awsLogo from '@assets/images/aws_logo.svg';
import azureLogo from '@assets/images/azure_logo.svg';
import gcpLogo from '@assets/images/gcp_logo.svg';
import { hideAuthLoader, showAuthLoader } from '@appRedux/actions';
import { CloudProvider } from '@common/constants';
import { openNotification } from '@common/helpers';
import InputImages from '@components/InputImages';
import SearchBox from '@components/SearchBox';
import { DigitalLabType } from '@constants';
import { Col, Divider, Input, Radio, Row, Tree, Upload } from 'antd';
import classnames from 'classnames';
import { debounce, flattenDeep } from 'lodash';
import { CKEditor } from 'octopus-editor';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forkJoin } from 'rxjs';

const TreeNode = Tree.TreeNode;

class LabOverview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      levels: [],
      categories: [],
      labName: props.defaultValue ? props.defaultValue.labName : '',
      description: props.defaultValue ? props.defaultValue.description : '',
      businessStory: props.defaultValue ? props.defaultValue.businessStory : '',
      childCategoryId: props.defaultValue ? props.defaultValue.childCategoryId : null,
      complexityId: props.defaultValue ? props.defaultValue.complexityId : '',
      digitalLabType: props.defaultValue ? props.defaultValue.digitalLabType : null,
      vendor: props.defaultValue ? props.defaultValue.vendor : 'azure',
      image: props.defaultValue ? props.defaultValue.image : null,
      timeInMinutes: props.defaultValue ? props.defaultValue.timeInMinutes : 0,
      estimateCost: props.defaultValue ? props.defaultValue.estimateCost : 0,
      notes: props.defaultValue ? props.defaultValue.notes : '',
      attachments: props.defaultValue ? props.defaultValue.attachments : [],
      expandedKeys: [],
      searchValue: '',
      rating: props.defaultValue ? props.defaultValue.rating : 100,
    };
    this.subscriptions = [];
    this.keys = [];
    this.dataCategories = [];
  }

  componentDidMount() {
    this.loadCommonInfo();
  }

  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  validateInput = () => {
    const {
      labName,
      description,
      businessStory,
      timeInMinutes,
      estimateCost,
      childCategoryId,
      complexityId,
      vendor,
      digitalLabType,
      rating,
    } = this.state;

    return (
      labName.trim().length > 0 &&
      description.trim().length > 0 &&
      businessStory.trim().length > 0 &&
      vendor.trim().length > 0 &&
      timeInMinutes > 0 &&
      estimateCost >= 0 &&
      digitalLabType != null &&
      childCategoryId.length > 0 &&
      complexityId != null &&
      rating >= 0
    );
  };

  loadCommonInfo = () => {
    showAuthLoader();
    this.subscriptions.push(
      forkJoin(getCategories(), getLevels()).subscribe(
        ([resCategories, levels]) => {
          const categories = [];

          if (resCategories && resCategories.length > 0) {
            resCategories
              .filter((e) => e.parentId === null)
              .map((e) => {
                const _category = {};
                _category.title = e.name;
                _category.key = e.categoryId;
                _category.children = [];
                categories.push(_category);
                return e;
              });

            resCategories
              .filter((e) => e.parentId !== null)
              .map((e) => {
                const index = categories.findIndex((category) => category.key === e.parentId);
                if (index !== -1) {
                  const _category = {};
                  _category.title = e.name;
                  _category.key = e.categoryId;
                  categories[index].children.push(_category);
                }
                return e;
              });
            this.dataCategories = resCategories;
          } else {
            this.dataCategories = [];
          }

          this.keys = this.getAllKeys(categories);
          this.setState(
            {
              categories,
              levels,
            },
            () => {
              hideAuthLoader();
            }
          );
        },
        (err) => {
          hideAuthLoader();
          openNotification('error', 'Load common fail!');
        }
      )
    );
  };

  getParentKey = (key, tree) => {
    let parentKey;
    for (let i = 0; i < tree.length; i++) {
      const node = tree[i];
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          parentKey = node.key;
        } else if (this.getParentKey(key, node.children)) {
          parentKey = this.getParentKey(key, node.children);
        }
      }
    }
    return parentKey;
  };

  renderTreeNodes = (data) =>
    data
      .map((i) => {
        i.index = parseInt(i.key.split('.')[1]);
        return i;
      })
      .sort((a, b) => a.index - b.index)
      .map((item) => {
        const { searchValue } = this.state;
        const index = item.title.indexOf(searchValue);
        const beforeStr = item.title.substr(0, index);
        const afterStr = item.title.substr(index + searchValue.length);
        const title =
          index > -1 && searchValue.length > 0 ? (
            <span>
              {beforeStr}
              <span style={{ color: '#f50' }}>{searchValue}</span>
              {afterStr}
            </span>
          ) : (
            <span>{item.title}</span>
          );

        if (item.children) {
          return (
            <TreeNode key={item.key} title={title}>
              {this.renderTreeNodes(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode key={item.key} title={title} />;
      });

  getAllKeys = (data) => {
    const nestedKeys = data.map((node) => {
      let childKeys = [];
      if (node.children) {
        childKeys = this.getAllKeys(node.children);
      }
      return [childKeys, node.key];
    });
    return flattenDeep(nestedKeys);
  };

  onHandleImageAvatar = (e) => {
    this.setState({ image: e }, () => {
      this.raiseOnChange();
    });
  };

  onUploadChange = ({ file, fileList }) => {
    this.setState(
      {
        attachments: fileList,
      },
      () => {
        this.raiseOnChange();
      }
    );
  };

  onChangeInput = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        this.raiseOnChange();
      }
    );
  };

  onChangeEditor = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        this.raiseOnChange();
      }
    );
  };

  onProviderChange = (provider) => {
    this.setState(
      {
        vendor: provider,
      },
      () => {
        this.raiseOnChange();
      }
    );
  };

  onChangeSearch = (e) => {
    const value = e.target.value;
    this.searchCategories(value);
  };

  searchCategories = debounce((value) => {
    const { categories } = this.state;
    const expandedKeys = this.dataCategories
      ? this.dataCategories
          .map((item) => {
            if (item.name.indexOf(value) > -1) {
              return this.getParentKey(item.categoryId, categories);
            }
            return null;
          })
          .filter((item, i, self) => item && self.indexOf(item) === i)
      : [];

    this.setState({
      expandedKeys,
      searchValue: value,
    });
  }, 150);

  onExpandTree = (expandedKeys) => {
    this.setState({
      expandedKeys,
    });
  };

  onCategoryChange = (value) => {
    this.setState({ childCategoryId: value.filter((e) => e.length > 1) }, () => {
      this.raiseOnChange();
    });
  };

  onCollapse = () => {
    this.setState({
      expandedKeys: [],
    });
  };

  onExpand = () => {
    this.setState({
      expandedKeys: this.keys,
    });
  };

  raiseOnChange = () => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(this.state);
    }
  };

  render() {
    const {
      labName,
      description,
      businessStory,
      notes,
      vendor,
      image,
      levels,
      complexityId,
      digitalLabType,
      timeInMinutes,
      estimateCost,
      categories,
      childCategoryId,
      attachments,
      expandedKeys,
      searchValue,
      rating,
    } = this.state;

    const awsClassnames = classnames('gx-icon-views vendor-provider', {
      'active-provider': vendor === CloudProvider.Aws,
    });
    const azureClassnames = classnames('gx-icon-views vendor-provider', {
      'active-provider': vendor === CloudProvider.Azure,
    });
    const gcpClassnames = classnames('gx-icon-views vendor-provider', {
      'active-provider': vendor === CloudProvider.Gcp,
    });
    const formName = this.props.isScenario ? 'Scenario' : 'Lab';

    return (
      <div className="lab-overview-form">
        <Row>
          <Col span={24}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="labName">
                  {formName} Name
                </label>
                <Input name="labName" id="labName" onChange={this.onChangeInput} allowClear={true} value={labName} />
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="business-story">
                  {formName} Overview
                </label>
                <CKEditor
                  defaultValue={businessStory}
                  onChange={(value) => this.onChangeEditor('businessStory', value)}
                />
              </div>
              {!this.props.isScenario && (
                <div className="lab-overview-form_item">
                  <label className="required" htmlFor="description">
                    Lab Description
                  </label>
                  <CKEditor
                    defaultValue={description}
                    onChange={(value) => this.onChangeEditor('description', value)}
                  />
                </div>
              )}
              {!this.props.isScenario && (
                <div className="lab-overview-form_item">
                  <label htmlFor="note">Note</label>
                  <CKEditor defaultValue={notes} onChange={(value) => this.onChangeEditor('notes', value)} />
                </div>
              )}
            </div>
          </Col>
        </Row>
        <Row>
          <Divider />
        </Row>
        <Row>
          <Col lg={24} xs={24} sm={24} xl={12}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="vendor">
                  Vendor
                </label>
                <Row>
                  <Col lg={24} xs={24} sm={24} xl={6}>
                    <div className={awsClassnames} onClick={() => this.onProviderChange(CloudProvider.Aws)}>
                      <img src={awsLogo} alt={CloudProvider.Aws} />
                    </div>
                  </Col>
                  <Col lg={24} xs={24} sm={24} xl={6}>
                    <div className={azureClassnames} onClick={() => this.onProviderChange(CloudProvider.Azure)}>
                      <img src={azureLogo} alt={CloudProvider.Azure} />
                    </div>
                  </Col>
                  <Col lg={24} xs={24} sm={24} xl={6}>
                    <div className={gcpClassnames} onClick={() => this.onProviderChange(CloudProvider.Gcp)}>
                      <img src={gcpLogo} alt={CloudProvider.Gcp} />
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="level">
                  Levels
                </label>
                <Radio.Group onChange={this.onChangeInput} value={complexityId} name="complexityId">
                  {levels &&
                    levels.map((level, i) => (
                      <Radio
                        style={{
                          display: 'block',
                        }}
                        value={level.complexityId}
                        key={i}
                      >
                        {level.name}
                      </Radio>
                    ))}
                </Radio.Group>
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="duration">
                  Duration (minutes)
                </label>
                <Input
                  type="number"
                  min={1}
                  name="timeInMinutes"
                  value={timeInMinutes}
                  onChange={this.onChangeInput}
                  addonAfter="minutes"
                />
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="cost">
                  Cost Estimate (USD)
                </label>
                <Input
                  type="number"
                  min={1}
                  name="estimateCost"
                  value={estimateCost}
                  onChange={this.onChangeInput}
                  addonAfter="US dollar"
                />
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="cost">
                  {formName} Digital Type
                </label>
                <Radio.Group onChange={this.onChangeInput} value={digitalLabType} name="digitalLabType">
                  {DigitalLabType &&
                    DigitalLabType.map((type, i) => (
                      <Radio
                        style={{
                          display: 'block',
                        }}
                        value={type}
                        key={i}
                      >
                        {type}
                      </Radio>
                    ))}
                </Radio.Group>
              </div>
            </div>
          </Col>
          <Col lg={24} xs={24} sm={24} xl={12}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="category">
                  Category
                </label>
                <div className="category-tree">
                  <Row className="category-tree-search">
                    <Col span={13} className="category-tree-search-wrapper">
                      <SearchBox
                        styleName="gx-lt-icon-search-bar-lg"
                        placeholder="Type in filter..."
                        onChange={this.onChangeSearch}
                        value={searchValue}
                      />
                    </Col>
                    <Col span={11} className="category-tree-search-button">
                      <div className="collapse" onClick={this.onCollapse}>
                        Collapse
                      </div>
                      <div className="expand" onClick={this.onExpand}>
                        Expand
                      </div>
                    </Col>
                  </Row>
                  <Row className="category-tree-content">
                    <Tree
                      checkable={true}
                      onCheck={this.onCategoryChange}
                      checkedKeys={childCategoryId}
                      onExpand={this.onExpandTree}
                      expandedKeys={expandedKeys}
                      className="tree-categories"
                    >
                      {this.renderTreeNodes(categories)}
                    </Tree>
                  </Row>
                </div>
              </div>
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="rating">
                  {formName} Order
                </label>
                <Input name="rating" id="rating" type="number" onChange={this.onChangeInput} value={rating} />
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Divider />
        </Row>
        <Row>
          <Col lg={24} xs={24} sm={24} xl={12}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label htmlFor="image">Cover avatar image</label>
                {typeof image === 'string' ? (
                  <InputImages imagesURL={image} onHandleImage={this.onHandleImageAvatar} />
                ) : (
                  <InputImages image={image} onHandleImage={this.onHandleImageAvatar} />
                )}
              </div>
            </div>
          </Col>
          {!this.props.isScenario && (
            <Col lg={24} xs={24} sm={24} xl={12}>
              <div className="lab-overview-form_group">
                <div className="lab-overview-form_item">
                  <label htmlFor="attachments">Attach resources</label>
                  <Upload
                    multiple={true}
                    defaultFileList={attachments}
                    beforeUpload={() => false}
                    onChange={this.onUploadChange}
                    className="upload-box"
                  >
                    <PlusOutlined className="upload-button" />
                  </Upload>
                </div>
              </div>
            </Col>
          )}
        </Row>
      </div>
    );
  }
}

export default connect(
  null,
  {
    showAuthLoader: showAuthLoader,
    hideAuthLoader: hideAuthLoader,
  },
  null,
  { forwardRef: true }
)(LabOverview);
