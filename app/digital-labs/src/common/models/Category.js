export class Category {
  constructor() {
    this.id = '';
    this.categoryId = '';
    this.parentId = '';
    this.name = '';
    this.context = '';
    this.active = true;
    this.createdBy = '';
    this.updatedBy = '';
    this.createdTime = new Date();
    this.updatedTime = new Date();
  }

  fromJson(json) {
    if (!json) {
      return this;
    }

    this.id = json._id;
    this.categoryId = json.categoryId;
    this.parentId = json.parentId ? json.parentId : null;
    this.name = json.name;
    this.context = json.context;
    this.active = json.active;
    this.createdBy = json.createdBy;
    this.updatedBy = json.updatedBy;
    this.updatedBy = json.updatedBy;
    this.createdTime = json.createdTime ? new Date(json.createdTime) : null;
    this.updatedTime = json.updatedTime ? new Date(json.updatedTime) : null;

    return this;
  }
}

export class CategoryStats {
  constructor() {
    this.category = new Category();
    this.countLabs = 0;
  }

  fromJson(json) {
    if (!json) {
      return this;
    }

    this.category = json.category ? new Category().fromJson(json.category) : null;
    this.countLabs = +json.countLabs;

    return this;
  }
}
