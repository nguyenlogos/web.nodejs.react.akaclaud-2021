import { addMBI, getTemplate } from '@api';
import awsLogo from '@assets/images/aws_logo.svg';
import azureLogo from '@assets/images/azure_logo.svg';
import { openNotification } from '@common/helpers';
import { CloudProvider } from '@constants';
import { Button, Card, Col, Form, Input, Row, Select } from 'antd';
import classnames from 'classnames';
import React, { useEffect, useState } from 'react';

const FormItem = Form.Item;

const CloudMBI = () => {
  const [templates, setTemplates] = useState([]);
  const [provider, setProvider] = useState('aws');
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const subscriptions = [];

  const fnEffectHook = () => {
    subscriptions.push(
      getTemplate().subscribe((templ) => {
        setTemplates(templ);
      })
    );

    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const onSave = (values) => {
    setLoading(true);
    subscriptions.push(
      addMBI(values).subscribe(
        () => {
          setLoading(false);
          openNotification('success', 'Setup cloud account MBI successfully');
        },
        () => {
          setLoading(false);
          openNotification('error', 'Setup cloud account MBI failed!');
        }
      )
    );
  };

  const onProviderChange = (provider) => {
    setProvider(provider);
  };

  const renderTemplate = (providerName) => {
    const options = templates
      ? templates.map((template) => {
        return (
          <Select.Option value={template.id} key={template.id} disabled={loading}>
            {template.name}
          </Select.Option>
        );
      })
      : [];

    return (<Select placeholder="Please select a template">
      {options}
    </Select>);
  };

  const renderFormAzure = () => {
    return (
      <Form form={form} name="cloudMBI" onFinish={onSave} scrollToFirstError={true}>
        <FormItem rules={[{ required: true, message: 'Please input your Assess key' }]} name="assessKey">
          <Input placeholder="Enter Azure Assess key" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please input your Secret key!' }]} name="secretKey">
          <Input placeholder="Enter Azure Secret key" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please input your Ternant!' }]} name="tenantId">
          <Input placeholder="Enter Azure Ternant" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please input your Subscription!' }]} name="subscriptionId">
          <Input placeholder="Enter Azure Subscription" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please choose the template!' }]} name="templateId">
          {renderTemplate(CloudProvider.Azure)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" loading={loading} style={{ width: '100%' }}>
            Save
          </Button>
        </FormItem>
      </Form>
    );
  };

  const renderFormAws = () => {
    return (
      <Form form={form} name="cloudMBI" onFinish={onSave} scrollToFirstError={true}>
        <FormItem rules={[{ required: true, message: 'Please input your Assess key' }]} name="assessKey">
          <Input placeholder="Enter AWS Assess key" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please input your Secret key!' }]} name="secretKey">
          <Input placeholder="Enter AWS Secret key" disabled={loading} />
        </FormItem>
        <FormItem rules={[{ required: true, message: 'Please choose the template!' }]} name="templateId">
          {renderTemplate(CloudProvider.Aws)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" loading={loading} style={{ width: '100%' }}>
            Save
          </Button>
        </FormItem>
      </Form>
    );
  };

  const awsClassnames = classnames('gx-icon-views provider', {
    'active-provider': provider === CloudProvider.Aws,
  });
  const azureClassnames = classnames('gx-icon-views provider', {
    'active-provider': provider === CloudProvider.Azure,
  });

  return (
    <Card>
      <Row justify="center">
        <Col span={24} className="provider-title">
          <h3>
            Choose Your Provider
          </h3>
        </Col>
      </Row>
      <Row justify="center">
        <Col span={6}>
          <div className={awsClassnames} onClick={() => onProviderChange(CloudProvider.Aws)}>
            <img src={awsLogo} alt="Amazon Web Service" />
          </div>
        </Col>
        <Col span={6}>
          <div className={azureClassnames} onClick={() => onProviderChange(CloudProvider.Azure)}>
            <img src={azureLogo} alt="Microsoft Azure" />
          </div>
        </Col>
      </Row>
      <Row justify="center">
        <Col span={24} className="provider-title">
          <h3>
            Fill Cloud Account Information
          </h3>
        </Col>
      </Row>
      <Row justify="center">
        <Col span={12}>
          {provider === CloudProvider.Azure && renderFormAzure()}
          {provider === CloudProvider.Aws && renderFormAws()}
        </Col>
      </Row>
    </Card>
  );
};

export default CloudMBI;
