import { Modal, Upload } from 'antd';
import Compressor from 'compressorjs';
import React, { Component } from 'react';

class InputImages extends Component {
  constructor(props) {
    super(props);

    const { image, imagesURL } = this.props;
    let fileList = [];
    if (image) {
      fileList = this._fileList(URL.createObjectURL(image));
    }

    if (imagesURL && !image) {
      fileList = this._fileList(imagesURL);
    }

    this.state = {
      previewVisible: false,
      previewImage: '',
      previewTitle: '',
      fileList: fileList,
    };
  }

  _fileList = (url) => {
    return [{ uid: '1', url: url, size: 1, type: '', name: '' }];
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (typeof nextProps.imagesURL == 'string' && !nextProps.image) {
      this.setState({ fileList: this._fileList(nextProps.imagesURL) });
    }

    if (typeof nextProps.image == 'object' && nextProps.image) {
      this.setState({
        fileList: this._fileList(URL.createObjectURL(nextProps.image)),
      });
    }
  }

  handleCancel = () => this.setState({ previewVisible: false });

  getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };

  handlePreview = (file) => {
    if (!file.url && !file.preview) {
      this.getBase64(file.originFileObj)
        .then((preview) => (file.preview = preview))
        .catch((err) => console.log('error', err));
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    });
  };

  handleChange = ({ fileList }) => {
    const { onHandleImage } = this.props;
    if (fileList && fileList.length > 0) {
      let file = fileList[0];

      if (file) {
        new Compressor(file.originFileObj, {
          quality: 0.8,
          mimeType: 'image/jpeg',
          maxWidth: 1024,
          success: (result) => {
            file.originFileObj = result;
            this.setState({
              fileList: this._fileList(URL.createObjectURL(result)),
            });
            onHandleImage(result);
          },
          error(err) {
            console.log(err.message);
          },
        });
      }
    }

    this.setState({
      fileList: fileList,
    });
  };

  render() {
    const { previewVisible, previewImage, fileList, previewTitle } = this.state;
    const uploadButton = (
      <div>
        <div className="ant-upload-text">
          Upload
        </div>
      </div>
    );
    return (
      <div>
        <Upload
          listType="picture-card"
          fileList={fileList}
          beforeUpload={() => false}
          onPreview={this.handlePreview}
          onChange={this.handleChange}
        >
          {fileList.length >= 1 ? null : uploadButton}
        </Upload>
        <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={this.handleCancel} width="800px">
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default InputImages;
