import HtmlElement from '@components/HtmlElement';
import { Button, Col, Divider, Popover } from 'antd';
import React from 'react';

import FlexLayout from './Layout/FlexLayout';
import VerticalLayout from './Layout/VerticalLayout';

const renderHover = (businessStory, labName, labItem, onClick) => {
  return (
    <div className="popup-lab-hover">
      <div className="title">
        {labName}
      </div>
      <Divider />
      <div className="description">
        <HtmlElement htmlString={businessStory} />
      </div>
      <Button className="read-more-btn" type="danger" onClick={() => onClick(labItem)}>
        Read more
      </Button>
    </div>
  );
};
const LabItem = ({ labItem, onClick, isFlexLayout }) => {
  const { labName, businessStory } = labItem;

  if (isFlexLayout) {
    return (
      <Col xl={6} lg={8} md={12} sm={12} xs={24}>
        <Popover placement="rightTop" content={renderHover(businessStory, labName, labItem, onClick)}>
          <FlexLayout labItem={labItem} onClick={onClick} />
        </Popover>
      </Col>
    );
  }
  return <VerticalLayout labItem={labItem} onClick={onClick} />;
};
export default LabItem;
