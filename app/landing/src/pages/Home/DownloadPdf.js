import React from "react";
import { Row, Col, Button } from "antd";
import { CloudDownloadOutlined } from "@ant-design/icons";

const DownloadPdf = ({ fileName, btnText, title, subTitle }) => (
  <Row justify="center">
      <Col xs={22} md={18} lg={14} xl={12} className="download-offer-container">
          <div className="download-offering">
              <div className="download-text">
                  <h3>{title}</h3>
                  <p>{subTitle}</p>
              </div>
              <div className="download-button">
                  <Button
                      type="primary"
                      size="small"
                      onClick={() => window.open('/documents/'+fileName)}
                  >
                      {btnText}
                      <CloudDownloadOutlined />
                  </Button>
              </div>
          </div>
      </Col>
  </Row>
);
export default DownloadPdf;