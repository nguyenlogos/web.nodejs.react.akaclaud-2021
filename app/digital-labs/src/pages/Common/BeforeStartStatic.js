import React from 'react';

const BeforeStartStatic = ({ story }) => {
  return (
    <div className="before-story" id="before-start">
      <h1>
        BEFORE YOU GET STARTED
      </h1>
      <p>
        There are some points you need to consider before launching the lab:
      </p>
      <h2 id="buy-credit">
        BUY CREDIT
      </h2>
      <p>
        Labs typically cost 0 to 15 credits depend on type of Cloud resource. Minimum purchase is 1 credits. Basically,
        the credit includes the (
        <span className="info-highlight">
          cost of lab resource
        </span>
        ) plus (cost of lab
        engine). In case, you
        <span className="info-highlight">
          [Use existing Cloud Account]
        </span>
        {' '}
        mode, you just have to pay the
        <span className="info-highlight">
          cost of lab engine
        </span>
        .
      </p>
      <h2 id="account-onboarding">
        ACCOUNT ONBOARDING PROCESS
      </h2>
      <p>
        You need to enable Cloud Account to ready for launching the lab. Select mode to enable Account in the Cloud
        environment:
      </p>
      <ul className="chid-info">
        <li>
          <span className="info-highlight">
            Use existing Cloud Account:
          </span>
          {' '}
          Customer can leverage existing Cloud
          Account to experience labs. Following our guide to set up IAM user in the Cloud environment to ready for lab
          provisioning.
        </li>
        <li>
          <span className="info-highlight">
            Use FPT Cloud Account (default):
          </span>
          {' '}
          In case you don’t own any Cloud
          Account, you can select this mode. Our system will automatically generate the Cloud Account for your lab
          experience’s process.
        </li>
      </ul>
      <h2 id="before-you-click">
        BEFORE YOU CLICK THE START LAB BUTTON
      </h2>
      <p>
        Read these instructions. Labs are timed and you cannot pause them. The timer, which starts when you click
        {' '}
        <span className="info-highlight">
          Start Lab
        </span>
        , shows how long Cloud resources will be made available to you.
      </p>
      <p>
        This hands-on lab lets you do the lab activities yourself in a real Cloud environment, not in a simulation or
        demo environment. It does so by giving you new, temporary credentials that you use to sign in and access Cloud
        Web Console for the duration of the lab.
      </p>
      <h2 id="end-lab">
        END THE LAB
      </h2>
      <p>
        After the period of time, the system automatically cleans up all resource. You also consider to end lab to
        optimize cost in case you finish the lab before the duration.
      </p>
      <p>
        To complete this lab, you need:
      </p>
      <ul className="chid-info">
        <li>
          Click End Lab button.
        </li>
        <li>
          Time to complete the lab.
          {' '}
        </li>
      </ul>
    </div>
  );
};

export default BeforeStartStatic;
