import AppLocale from '@common/languages';
import { ConfigProvider } from 'antd';
import React, { memo } from 'react';
import { IntlProvider } from 'react-intl';
import { useSelector } from 'react-redux';
import { Route, Switch, useLocation, useRouteMatch } from 'react-router-dom';

import MainApp from './MainApp';

const App = () => {
  const location = useLocation();
  const match = useRouteMatch();
  const { locale } = useSelector(({ settings }) => settings);
  const currentAppLocale = AppLocale[locale.locale];

  return (
    <ConfigProvider locale={currentAppLocale.antd}>
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}
      >
        <Switch>
          <Route path={`${match.url}`} location={location} component={MainApp} />
        </Switch>
      </IntlProvider>
    </ConfigProvider>
  );
};

export default memo(App);
