import React from 'react';

const HtmlElement = ({ htmlString }) => {
  const safeElement = () => {
    if (htmlString == null) {
      return '';
    }

    // remove script tags
    return htmlString.replace(/<script.*>.*?<\/script>/g, '');
  };

  return <div className="safe-html-injector" dangerouslySetInnerHTML={{ __html: safeElement() }} />;
};

export default HtmlElement;
