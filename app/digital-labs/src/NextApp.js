import '@styles/index.less';
import '@assets/vendors/style';

import React from 'react';
import ReactDOM from 'react-dom';
import { loadCurrentUserInfo } from '@appRedux/actions';
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch } from 'react-router-dom';
import { AppContainer } from 'react-hot-loader';
import { history } from '@appRedux/store';
import { Provider } from 'react-redux';
import App from './shared/containers/App';

const renderApp = (store) => {
  store.dispatch(loadCurrentUserInfo());
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path="/" component={App} />
          </Switch>
        </ConnectedRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  );
};

export default renderApp;
