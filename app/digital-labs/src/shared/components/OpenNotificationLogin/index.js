import { Button, notification } from 'antd';
import { login } from 'aka-keycloak-login';
import React from 'react';

const OpenNotificationLogin = () => {
  const btn = (
    <Button type="primary" onClick={() => login()}>
      Login
    </Button>
  );

  return notification.warning({
    message: 'Notification',
    description: 'Session Timeout',
    btn,
  });
};

export default OpenNotificationLogin;
