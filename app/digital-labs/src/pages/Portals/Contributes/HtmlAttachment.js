import { Button, Modal } from 'antd';
import classnames from 'classnames';
import { MarkdownEditor } from 'octopus-editor';
import React, { Component } from 'react';

const { confirm } = Modal;

class HtmlAttachment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nameFile: '',
      typing: props.source && typeof props.source === 'string',
      file: props.source && typeof props.source !== 'string' ? props.source : null,
      content: props.source && typeof props.source === 'string' ? props.source : null,
    };

    this.myRef = React.createRef();
    this.editorMinHeight = '200px';
  }

  UNSAFE_componentWillReceiveProps(props) {
    const { source } = props;

    if (source && typeof source === 'string') {
      this.setState({
        typing: true,
        content: source,
      });
    }
    if (source && typeof source === 'object') {
      this.setState({
        typing: false,
        file: source,
        nameFile: source.name,
      });
    }
  }

  clear = () => {
    this.setState({ typing: false, file: null, content: null });
  };

  onChange = (e) => {
    const { onChange } = this.props;
    const { typing, file, content } = this.state;

    if (onChange) {
      onChange(typing ? content : file);
    }
  };

  onBlur = (content) => {
    const { onBlur } = this.props;

    if (onBlur) {
      onBlur(content);
    }
  };

  onInputTypeChange = (e, isTyping) => {
    e.persist();
    const { typing, file, content } = this.state;
    if ((typing && content && content.length > 0) || (!typing && file)) {
      confirm({
        title: 'Do you want to change input type?',
        content: 'Your current input will be lost',
        onOk: () => {
          this.setState(
            {
              typing: isTyping,
              content: null,
              file: null,
            },
            () => {
              const { typing } = this.state;
              if (!typing) {
                e.preventDefault();
                if (this.myRef.current) {
                  this.myRef.current.click();
                }
              }
            }
          );
        },
        okType: 'danger',
      });
    } else {
      this.setState(
        {
          typing: isTyping,
          content: null,
          file: null,
        },
        () => {
          const { typing } = this.state;
          if (!typing) {
            e.preventDefault();
            if (this.myRef.current) {
              this.myRef.current.click();
            }
          }
        }
      );
    }
  };

  renderUploadFile = () => {
    const { nameFile } = this.state;
    const handleFile = (e) => {
      const { files } = e.target;
      const file = files[0];
      if (file) {
        this.setState({
          file,
          nameFile: file.name,
        });
        this.onChange(file);
        this.onBlur(file);
      } else {
        this.setState({
          file: null,
          nameFile: '',
        });
      }
    };

    return (
      <div className="upload-file">
        {nameFile && (
          <p>
            CHOSEN FILE : 
            {' '}
            <span>
              {nameFile}
            </span>
          </p>
        )}
        <input type="file" onChange={handleFile} accept=".md" ref={this.myRef} hidden={true} />
      </div>
    );
  };

  renderEditor = () => {
    const { content } = this.state;
    const { editorMinHeight } = this;
    const onEditorChange = (e, html) => {
      this.setState({
        content: html,
      });

      this.onChange(html);
    };

    const onEditorBlur = (e, text) => {
      this.setState({
        content: text,
      });
      this.onBlur(text);
    };
    return (
      <MarkdownEditor
        preview="vertical"
        content={content}
        minHeight={editorMinHeight}
        onChange={onEditorChange}
        onBlur={onEditorBlur}
      />
    );
  };

  render() {
    const { typing } = this.state;
    const { classNames } = this.props;
    const classes = classnames('html-attachment', classNames);
    return (
      <div className={classes}>
        <div className="html-attachments-selections">
          <Button type={!typing ? 'primary' : null} onClick={(e) => this.onInputTypeChange(e, false)}>
            Upload
          </Button>

          <Button type={typing ? 'primary' : null} onClick={(e) => this.onInputTypeChange(e, true)}>
            Create new
          </Button>
        </div>
        <div className="html-attachment-controls">
          {!typing && this.renderUploadFile()}
          {typing && this.renderEditor()}
        </div>
      </div>
    );
  }
}

export default HtmlAttachment;
