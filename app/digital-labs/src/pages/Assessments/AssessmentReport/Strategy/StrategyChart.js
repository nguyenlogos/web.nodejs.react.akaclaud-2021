import React from 'react';
import {
  Legend,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart as Chart,
  ResponsiveContainer,
} from 'recharts';

const StrategyChart = (props) => {
  const { questions, decision } = props;
  const question = questions.find((q) => q.category === decision.domain);
  const alternativeOption = question.readiness[decision.name];

  const data =
    alternativeOption &&
    alternativeOption.migration_Objectives.map(({ content, point, actualPoint }) =>
      alternativeOption.finalOption
        ? {
            category: content,
            actual: actualPoint,
            expected: point,
            fullMark: alternativeOption.migration_Objectives.totalPoint,
          }
        : {
            category: content,
            expected: point,
            fullMark: alternativeOption.migration_Objectives.totalPoint,
          }
    );

  return (
    <ResponsiveContainer outerRadius={150} width="100%" height={400}>
      <Chart data={data}>
        <Radar name={decision.name} dataKey="actual" stroke="#ffcc00" fill="#ffcc00" fillOpacity={0.5} />
        <Radar name="Expected" dataKey="expected" stroke="#007aff" fill="#5AC8FA" fillOpacity={0.5} />
        <PolarGrid />
        <PolarAngleAxis dataKey="category" />
        <PolarRadiusAxis />
        <Legend />
      </Chart>
    </ResponsiveContainer>
  );
};

export default StrategyChart;
