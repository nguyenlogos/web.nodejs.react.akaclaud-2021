import { filterChildren } from '@common/helpers';
import { ICON_NAVIGATION } from '@constants';
import classnames from 'classnames';
import React from 'react';
import { Link } from 'react-router-dom';

const AppsNavigation = ({ roles, className }) => {
  className = classnames('gx-app-nav', className);

  return (
    <ul className={className}>
      {ICON_NAVIGATION.map(
        (item) =>
          filterChildren(item, roles) && (
            <li key={item.url}>
              <Link to={item.url}>
                {item.icon}
              </Link>
            </li>
          )
      )}
    </ul>
  );
};

export default AppsNavigation;
