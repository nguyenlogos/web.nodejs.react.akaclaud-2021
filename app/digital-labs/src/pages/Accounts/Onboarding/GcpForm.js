import { GCP_GUIDELINE_URL } from '@common';
import { Button, Form, Input, Spin } from 'antd';
import classnames from 'classnames';
import React from 'react';

const FormItem = Form.Item;

const GcpOnBoardingForm = ({ className, onSubmit, onSubmitFailed, isLoading }) => {
  const formClassnames = classnames('onboarding-form', className);

  return (
    <Form name="gcp-onboarding" className={formClassnames} onFinish={onSubmit} onFinishFailed={onSubmitFailed}>
      <p className="sm-notes">
        <b>
          Note:
        </b>
        &nbsp;You can use existing cloud account to study&nbsp;
        <a href={GCP_GUIDELINE_URL} target="_blank" className="guideline" rel="noopener noreferrer">
          (Guideline)
        </a>
      </p>

      <FormItem name="accessKey" rules={[{ required: true, message: 'Please input your Service Account' }]}>
        <Input placeholder="Enter GCP Service Account" />
      </FormItem>

      <FormItem name="secretKey" rules={[{ required: true, message: 'Please input your Private key' }]}>
        <Input placeholder="Enter GCP Private key" />
      </FormItem>

      <FormItem name="subscriptionId" rules={[{ required: true, message: 'Please input your Project ID' }]}>
        <Input placeholder="Enter GCP Project ID" />
      </FormItem>

      {isLoading && (
        <div className="gx-text-center">
          <Spin tip="Account is creating. Please wait until process is complete" />
        </div>
      )}

      <FormItem>
        <Button className="gx-full-width" type="primary" htmlType="submit">
          Save
        </Button>
      </FormItem>
    </Form>
  );
};

export default GcpOnBoardingForm;
