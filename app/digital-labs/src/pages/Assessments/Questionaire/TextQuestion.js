import { saveAnswer } from '@api';
import { Input } from 'antd';
import * as React from 'react';

const { TextArea } = Input;

export default class TextQuestion extends React.Component {
  subscriptions = [];
  constructor(props) {
    super(props);

    this.state = {
      answer: (props.question && props.question.answer) || '',
    };
  }
  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  handleValue = (e) => {
    const { question, assessmentId } = this.props;

    if (e.target.value.trim().length >= 0) {
      question.answer = e.target.value.trim();
      const questionObject = {
        id: question.id,
        domain: question.domain,
        subDomain: question.subDomain,
        type: question.type,
        answer: e.target.value,
      };

      this.subscriptions.push(
        saveAnswer(assessmentId, questionObject).subscribe(
          () => {
            this.props.callbackFromParent(question);
          },
          (error) => {
            console.log('ERROR: ', error);
          }
        )
      );
    }

    this.setState({ answer: e.target.value });
  };

  render() {
    const { answer } = this.state;
    const { question, unanswered } = this.props;
    return (
      <div className={unanswered === question.key ? 'unanswered' : null}>
        <div ref={this._questionRef} className="title-question">
          <div className="content-question">{question.question}</div>
          <TextArea
            placeholder="Autosize height with minimum and maximum number of lines"
            autoSize={{ minRows: 2, maxRows: 6 }}
            allowClear={true}
            onChange={this.handleValue}
            value={answer}
          />
        </div>
      </div>
    );
  }
}
