import MarkdownViewer from '@components/MdViewer';
import classnames from 'classnames';
import React from 'react';

const ContentViewer = ({ isLoading, content, className }) => {
  const elClassNames = classnames('content-viewer', className);

  return (
    <div className={elClassNames}>
      <div className="content-wrapper">
        {content && <MarkdownViewer src={content} />}
      </div>
      {isLoading && (
        <div className="overlay">
          loading...
        </div>
      )}
    </div>
  );
};

export default ContentViewer;
