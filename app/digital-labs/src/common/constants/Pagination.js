export const DEFAULT_PAGE_INDEX = 0;
export const DEFAULT_PAGE_SIZE = 12;
export const DEFAULT_PAGE_SIZES = ['12', '20', '50', '100'];

export const DEFAULT_ADMIN_PAGE_INDEX = 0;
export const DEFAULT_ADMIN_PAGE_SIZE = 10;
export const DEFAULT_ADMIN_PAGE_SIZES = ['10', '20', '50', '100'];
