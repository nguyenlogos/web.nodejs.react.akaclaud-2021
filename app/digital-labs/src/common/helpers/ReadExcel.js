import readXlsxFile from 'read-excel-file';

export function readQuestions(file, options) {
  return new Promise((resolve, reject) => {
    readExcelFile(file, options).then(
      function (rows) {
        let { headerSlice } = options;
        if (headerSlice == null) {
          headerSlice = 1;
        }

        // remove header lines
        rows = rows.slice(headerSlice, rows.length);

        // map question data
        const questions = [];
        let domain = null,
            subdomain = null;
        let id = 0,
            subid = 0,
            qid = 0;

        rows.forEach((item) => {
          // check domain row
          if (item[0]) {
            id++;
            subid = 0;
            qid = 0;
            domain = item[0];
            questions.push({
              id: '' + id,
              type: 'domain',
              domain: domain,
              subDomains: [],
            });
          } else {
            // check sub-domain row
            if (item[1]) {
              subid++;
              qid = 0;

              subdomain = item[1];
              questions[questions.length - 1].subDomains.push({
                id: id + '.' + subid,
                type: 'subDomain',
                context: item[2],
                name: subdomain,
                questions: [],
              });
            }

            // question row
            qid++;
            var d_item = questions[questions.length - 1];

            d_item.subDomains[d_item.subDomains.length - 1].questions.push({
              _id: item[3], // for update question
              id: id + '.' + subid + '.' + qid,
              type: 'question',
              question: item[4],
              questionType: item[5],
              guide: item[6],
              negative: item[7],
            });
          }
        });

        const allQuestions = questions
          .map((domain) => {
            const subDomains = domain.subDomains;
            const subQuestions = subDomains
              .map((s) => {
                return s.questions.map((q) => {
                  return {
                    _id: q._id,
                    id: q.id,
                    type: q.questionType,
                    content: q.question,
                    guide: q.guide,
                    negative: q.negative,
                    domain: {
                      id: domain.id,
                      name: domain.domain,
                    },
                    subDomain: {
                      id: s.id,
                      name: s.name,
                      context: s.context,
                    },
                  };
                });
              })
              .reduce((prev, curr) => {
                prev.push(...curr);
                return prev;
              }, []);

            return subQuestions;
          })
          .reduce((prev, curr) => {
            prev.push(...curr);

            return prev;
          }, []);

        resolve(allQuestions);
      },
      function (error) {
        reject(error);
      }
    );
  });
}

function readExcelFile(file, options) {
  return new Promise((resolve, reject) => {
    const { sheet } = options;

    readXlsxFile(file, { sheet: sheet || 1 }).then(
      function (rows) {
        resolve(rows);
      },
      function (error) {
        reject(error);
      }
    );
  });
}
