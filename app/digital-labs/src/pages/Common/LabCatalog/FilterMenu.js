import { getCategories, getLevels, getVendors } from '@api';
import { FilterType } from '@common/constants';
import { Checkbox, Divider, Dropdown, Input, Menu } from 'antd';
import classnames from 'classnames';
import React, { Component } from 'react';
import { forkJoin } from 'rxjs';

const FILTER_GROUPS = [FilterType.category, FilterType.vendor, FilterType.level];

export default class FilterMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      selectedItems: this.mapSelectedItems(props.selectedItems),
      keyword: null,
    };
  }

  componentDidMount() {
    this.loadFilterItems();
  }

  mapSelectedItems = (selectedItems) => {
    if (selectedItems) {
      const groupItems = [];
      selectedItems.forEach((item) => {
        const group = groupItems.find((g) => g.type === item.type);

        if (group) {
          group.items.push(item);
        } else {
          groupItems.push({
            type: item.type,
            items: item,
          });
        }
      });

      return groupItems;
    }

    return [];
  };

  mapFilterItems = (selectedItems) => {
    const groupTypes = (type) => {
      switch (type) {
        case FilterType.category:
          return 'childCategoryId';
        case FilterType.vendor:
          return 'vendor';
        case FilterType.level:
          return 'complexityId';
        default:
          return null;
      }
    };

    const items = selectedItems
      ? selectedItems.map((item) => {
          const filter = {
            key: groupTypes(item.type),
            value: item.items
              .reduce((prev, curr) => {
                prev.push(curr.objectId);
                return prev;
              }, [])
              .join(','),
          };
          return filter;
        })
      : [];
    return items;
  };

  listFilterItems = (selectedItems) => {
    if (selectedItems.length === 0) {
      return [];
    }
    const items = selectedItems.map((item) => {
      const filter = {
        key: item.type,
        value: item.items,
      };
      return filter;
    });
    return items;
  };

  loadFilterItems = () => {
    forkJoin(getVendors(), getLevels(), getCategories()).subscribe(([vendors, levels, categories]) => {
      const vendorItems = this.loadVendorItems(vendors);
      const levelItems = this.loadLevelItems(levels);
      const categoryItems = this.loadCategoryItems(categories);
      const { items } = this.state;
      items.push(...vendorItems);
      items.push(...levelItems);
      items.push(...categoryItems);
      this.setState({
        items,
      });
    });
  };

  loadLevelItems = (levels) => {
    if (levels.length === 0) {
      return [];
    }

    const levelItems = levels.map((v) => {
      const item = {
        type: FilterType.level,
        title: v.name,
        objectId: v.levelId,
        children: [],
      };
      return item;
    });

    return levelItems;
  };

  loadVendorItems = (vendors) => {
    if (vendors.length === 0) {
      return [];
    }

    const vendorItems = vendors.map((v) => {
      const item = {
        type: FilterType.vendor,
        title: v.name?.toUpperCase(),
        objectId: v.name,
        children: [],
      };
      return item;
    });
    return vendorItems;
  };

  loadCategoryItems = (categories) => {
    if (categories.length === 0) {
      return [];
    }

    const parentItems = categories
      .filter((c) => c.parentId == null)
      .map((c) => {
        const item = {
          type: FilterType.category,
          title: c.name,
          objectId: c.categoryId,
          children: [],
        };
        return item;
      });
    categories
      .filter((c) => c.parentId != null)
      .forEach((c) => {
        const item = {
          type: FilterType.category,
          title: c.name,
          objectId: c.categoryId,
          children: [],
        };
        const parent = parentItems.find((p) => p.objectId === c.parentId);
        if (parent) {
          parent.children.push(item);
        }
      });
    return parentItems;
  };

  onCheck = (checkedValues, group) => {
    const { selectedItems, keyword } = this.state;
    const { onSelectedChange } = this.props;

    if (checkedValues && checkedValues.length === 0) {
      const type = selectedItems.find((g) => g.type === group);
      if (type) {
        const index = selectedItems.indexOf(type);
        if (index > -1) {
          selectedItems.splice(index, 1);
        }
        this.setState({
          selectedItems: selectedItems,
        });
      }
      if (onSelectedChange) {
        onSelectedChange(keyword, this.mapFilterItems(selectedItems));
      }
      return;
    }

    if (group) {
      const groupItem = selectedItems.find((g) => g.type === group);
      if (groupItem) {
        groupItem.items = checkedValues;
      } else {
        selectedItems.push({
          type: group,
          items: checkedValues,
        });
      }

      this.setState({
        selectedItems: selectedItems,
      });
      if (onSelectedChange) {
        onSelectedChange(keyword, this.mapFilterItems(selectedItems));
      }
    }
  };

  onKeywordSearchKeypress = (e) => {
    const { selectedItems, keyword } = this.state;
    const { onSelectedChange } = this.props;
    if (e.key === 'Enter') {
      if (onSelectedChange) {
        onSelectedChange(keyword, this.mapFilterItems(selectedItems));
      }
      e.preventDefault();
    }
  };

  onKeywordChange = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();

    this.setState({
      keyword: keyword,
    });
  };

  onClickSearch = (e) => {
    const { selectedItems, keyword } = this.state;
    const { onSelectedChange } = this.props;
    if (onSelectedChange) {
      onSelectedChange(keyword, this.mapFilterItems(selectedItems));
    }
    e.preventDefault();
  };

  removeFilter = (item) => {
    const { selectedItems, keyword } = this.state;
    const { onSelectedChange } = this.props;
    const type = item && item.type.length > 0 ? item.type : null;
    if (type) {
      const group = selectedItems.find((g) => g.type === type);
      if (group) {
        const items = group.items;
        const index = items.indexOf(item);
        if (index > -1) {
          items.splice(index, 1);
        }
      }
      this.setState({
        selectedItems: selectedItems,
      });
      if (onSelectedChange) {
        onSelectedChange(keyword, this.mapFilterItems(selectedItems));
      }
    }
  };

  renderGroups = (group, index) => {
    const { items, selectedItems } = this.state;

    const treeData = items.length > 0 ? items.filter((i) => i.type === group) : [];
    const filterCategoryClass = classnames('filter-items', {
      'category-width-box': group === FilterType.category,
    });
    const filterCategoryChechbox = classnames('checkbox-group', {
      'category-filter': group === FilterType.category,
    });
    const selectedGroup = selectedItems.find((g) => g.type === group);
    const selectedKeys = selectedGroup ? selectedGroup.items : [];

    const menu = (
      <Menu>
        <div className={filterCategoryClass}>
          <div className="filter-item-content">
            <Checkbox.Group
              className={filterCategoryChechbox}
              defaultValue={selectedKeys}
              onChange={(values) => this.onCheck(values, group)}
            >
              {treeData &&
                treeData.map((item, index) => {
                  if (item.type === FilterType.category) {
                    return (
                      <div key={index} className="category-checkbox-group">
                        <h5>{item.title}</h5>
                        {item.children &&
                          item.children
                            .map((i) => {
                              i.index = parseInt(i.objectId ? i.objectId.split('.')[1] : 0);
                              return i;
                            })
                            .sort((a, b) => a.index - b.index)
                            .map((child, index) => {
                              return (
                                <div className="checkbox-item" key={index}>
                                  <Checkbox value={child}>{child.title}</Checkbox>
                                </div>
                              );
                            })}
                      </div>
                    );
                  } else {
                    return (
                      <div className="checkbox-item" key={index}>
                        <Checkbox value={item}>{item.title}</Checkbox>
                      </div>
                    );
                  }
                })}
            </Checkbox.Group>
          </div>
        </div>
      </Menu>
    );

    return (
      <div className="lab-filter-type" key={index}>
        <Dropdown overlay={menu} mouseLeaveDelay={0.2}>
          <button className="dropdown-button">
            <div className="dropdown-text">
              {group}
              <i className="icon icon-menu-down" />
            </div>
          </button>
        </Dropdown>
        <Divider className="lab-filter-type-divider" type="vertical" />
      </div>
    );
  };

  changeLayout = () => {
    const { onChangeLayout } = this.props;
    onChangeLayout();
  };

  render() {
    const { isFlexLayout } = this.props;
    const iconChangeLayout = classnames('lab-loading-ion', 'icon', {
      'icon-all-contacts': !isFlexLayout,
      'icon-apps': isFlexLayout,
    });
    return (
      <div className="lab">
        <div className="lab-search">
          <Input
            type="text"
            onKeyPress={this.onKeywordSearchKeypress}
            onChange={this.onKeywordChange}
            className="search-input"
            placeholder="Type keyword to find"
          />
          <div className="search-icon" onClick={this.onClickSearch}>
            <i className="icon icon-search-new" />
          </div>
        </div>
        <div className="lab-filter">
          {FILTER_GROUPS && FILTER_GROUPS.map((group, index) => this.renderGroups(group, index))}
        </div>
        <div className="lab-loading" onClick={this.changeLayout}>
          <i className={iconChangeLayout} />
        </div>
      </div>
    );
  }
}
