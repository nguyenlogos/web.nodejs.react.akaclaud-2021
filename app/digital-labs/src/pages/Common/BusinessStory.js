import React from 'react';

import ContentViewer from './ContentViewer';

const BusinessStory = ({ story }) => {
  return (
    <div className="business-story">
      <ContentViewer className="story" content={story} />
    </div>
  );
};

export default BusinessStory;
