const path = require('path');
const fs = require('fs');
const dotenv = require('dotenv');
const { override, addLessLoader, addBabelPlugins } = require('customize-cra');

const overrideNightlyEnv = (config) => {
  if (fs.existsSync(".env.nightly")) {
    const nightlyConfig = dotenv.parse(fs.readFileSync(".env.nightly"));

    const replacementsIndex = config.plugins.findIndex(p => p.replacements != null);
    const definitionsIndex = config.plugins.findIndex(p => p.definitions != null);

    if (replacementsIndex >= 0) {
      config.plugins[replacementsIndex].replacements = Object.assign(config.plugins[replacementsIndex].replacements, nightlyConfig);
    }

    if (definitionsIndex >= 0) {
      const definitions = Object.assign(nightlyConfig, {});
      for (let i in definitions) {
        definitions[i] = `\"${definitions[i]}\"`
      }
      config.plugins[definitionsIndex].definitions["process.env"] = Object.assign(config.plugins[definitionsIndex].definitions["process.env"], definitions);
    }
  }
};

const overrideStagingEnv = (config) => {
  if (fs.existsSync(".env.staging")) {
    const nightlyConfig = dotenv.parse(fs.readFileSync(".env.staging"));

    const replacementsIndex = config.plugins.findIndex(p => p.replacements != null);
    const definitionsIndex = config.plugins.findIndex(p => p.definitions != null);

    if (replacementsIndex >= 0) {
      config.plugins[replacementsIndex].replacements = Object.assign(config.plugins[replacementsIndex].replacements, nightlyConfig);
    }

    if (definitionsIndex >= 0) {
      const definitions = Object.assign(nightlyConfig, {});
      for (let i in definitions) {
        definitions[i] = `\"${definitions[i]}\"`
      }
      config.plugins[definitionsIndex].definitions["process.env"] = Object.assign(config.plugins[definitionsIndex].definitions["process.env"], definitions);
    }
  }
};

const overrideNightlyProcessEnv = () => config => {
  if (process.env.npm_lifecycle_event != null) {
    const scriptEvent = process.env.npm_lifecycle_event.replace(/(:|\s)/g, "_");
    const script = process.env["npm_package_scripts_" + scriptEvent];

    if (/--nightly/g.test(script)) {
      overrideNightlyEnv(config);
    }

    if (/--staging/g.test(script)) {
      overrideStagingEnv(config);
    }
  }

  return config;
};

module.exports = override(
  addLessLoader({
    javascriptEnabled: true,
  }),
  addBabelPlugins(
    [
      'module-resolver',
      {
        root: ["./src"],
        alias: {
          "@api": path.resolve(__dirname, 'src/api'),
          "@components": path.resolve(__dirname, 'src/shared/components'),
          "@styles": path.resolve(__dirname, 'src/shared/styles'),
          "@assets": path.resolve(__dirname, 'src/shared/assets'),
          "@constants": path.resolve(__dirname, 'src/common/constants'),
          "@appRedux": path.resolve(__dirname, 'src/common/appRedux'),
          "@lngProvider": path.resolve(__dirname, 'src/common/lngProvider'),
          "@common": path.resolve(__dirname, 'src/common'),
          "@util": path.resolve(__dirname, 'src/common/util'),
        }
      }
    ],
  ),
  overrideNightlyProcessEnv()
);
