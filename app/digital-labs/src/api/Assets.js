import { from } from 'rxjs';

import { AuthHttp } from './client';

// GET: /api/v1/assets?_page&_limit
export function getAllAssets(page, pageSize) {
  let url = `assets?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

// DELETE: /api/v1/assets/
export function deleteAsset(id, resourceId, resourceType) {
  let url = 'assets/';
  return from(AuthHttp.delete(url, { data: { id: id, resourceId: resourceId, resourceType: resourceType } }));
}
