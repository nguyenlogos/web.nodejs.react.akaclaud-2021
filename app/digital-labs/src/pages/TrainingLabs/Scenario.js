import { getCategories, getScenarioContent } from '@api';
import Breadcrumbs from '@components/Breadcrumbs';
import MarkdownViewer from '@components/MdViewer';
import TableOfContent from '@components/TableOfContent';
import { Col, Row, Timeline } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forkJoin } from 'rxjs';

import ScenarioLabel from '../Common/ScenarioLabel';
import ScenarioStep from '../Common/ScenarioStep';

const toc = [
  { title: 'Introduction', href: '#introduction' },
  { title: 'Scenario Lab', href: '#scenariolab' },
];

class LabScenario extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scenario: null,
      steps: [],
      categories: null,
    };

    this.subscriptions = [];
  }

  componentDidMount() {
    this.subscriptions.push(
      forkJoin([getScenarioContent(this.props.match.params.id), getCategories()]).subscribe(
        ([{ scenario, steps }, categories]) => {
          this.setState({ scenario, steps, categories });
        }
      )
    );
  }

  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  navigateToLab(id) {
    const { history } = this.props;
    history.push(`/labs/work/${id}`);
  }

  render() {
    const { scenario, steps, categories } = this.state;
    return (
      <div className="lab-scenario">
        <Row align="middle" className="lab-scenario-banner">
          <Col span={20}>
            <div className="banner-title">
              <Breadcrumbs
                list={[
                  { title: 'Digital Lab', to: '/' },
                  { title: 'Lab Catalog', to: '/labs' },
                  { title: 'Scenario Lab' },
                ]}
              />
              {scenario && (
                <div className="lab-scenario-nav-bar">
                  <div className="lab-scenario-title">
                    <span>
                      {scenario.labName}
                    </span>
                  </div>
                </div>
              )}
            </div>
          </Col>
        </Row>
        <div className="lab-scenario-card">
          <Row>
            <Col xs={24} md={18} lg={20}>
              {scenario && (
                <div id="introduction" className="scenario-intro">
                  <h2>
                    Introduction
                  </h2>
                  <MarkdownViewer src={scenario.businessStory} />
                </div>
              )}
              {scenario && (
                <div id="scenariolab" className="scenario-intro">
                  <h2>
                    Scenario Labs
                  </h2>
                  <p>
                    {scenario.description}
                  </p>
                </div>
              )}

              <div className="steps">
                <Timeline mode="left">
                  {steps && steps.map((step, i) => (
                    <Timeline.Item
                      key={step._id}
                      color={step.status === 0 ? 'blue' : 'green'}
                      label={<ScenarioLabel index={i + 1} />}
                      onClick={() => this.navigateToLab(step._id)}
                    >
                      <ScenarioStep step={step} categories={categories} />
                    </Timeline.Item>
                  ))}
                </Timeline>
              </div>
            </Col>

            <Col className="anchor" xs={0} md={6} lg={4}>
              <div className="anchor-detail">
                <TableOfContent items={toc} />
              </div>
            </Col>
          </Row>
        </div>

        <Row className="lab-scenario-footer">
          <div className="title" />
          <div className="content">
            Copyright 2020 FPT Software All rights reserved FPT Software and the FPT Software logo are trademarks of FPT
            Software All other company and product names may be trademarks of the respective
          </div>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(LabScenario);
