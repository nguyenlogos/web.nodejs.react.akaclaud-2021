import { getRecommend, getReportById, sendEmailReport } from '@api';
import { onNavStyleChange } from '@appRedux/actions';
import { ReportAssessment } from '@common/constants/AssessmentTypes';
import { NAV_STYLE_DARK_HORIZONTAL } from '@common/constants/ThemeSetting';
import { formatDate, hideSpinner, showErrorToast, showSpinner, showSuccessToast } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Button, Col, Row, Tabs } from 'antd';
import * as React from 'react';
import { connect } from 'react-redux';
import { map } from 'rxjs/operators';

import Decision from './Decision';
import DownloadForm from './DownloadForm';
import Heatmap from './Heatmap';
import Radar from './Radar';
import Score from './Score';
import Strategy from './Strategy';

const titleToId = (title, tab) => title.split(' ').join('-') + tab;
const { TabPane } = Tabs;
const DEFAULT_COUNTRY = 'Viet Nam';

class AssessmentReport extends React.Component {
  _subscriptions = [];

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      report: [],
      savescore: [],
      totalscore: 0,
      recommend: [],
      reportName: '',
      activeTab: '1',
      assessmentId: undefined,
      showReportForm: false,
    };
  }

  componentDidMount() {
    const { navStyle, changeNavStyle } = this.props;
    if (navStyle !== NAV_STYLE_DARK_HORIZONTAL) {
      changeNavStyle();
    }
    this._subscriptions.push(
      getReportById(this.props.match.params.id)
        .pipe(
          map((report) => {
            const assessmentId = report.assessmentId;
            const savescore = [];
            let totalscore = 0;
            if (!report.report) {
              showErrorToast('Could not load assessment report');
            } else if (report.report && report.assessmentName !== ReportAssessment.Strategy) {
              report.report.map((r) => {
                const reducer = (accumulator, currentValue) => accumulator + currentValue;
                const sumscore = Object.values(r.readiness).reduce(reducer) + '';
                let totalcore = parseInt(sumscore) / Object.values(r.readiness).length;
                savescore.push(Math.ceil(totalcore));
                return r;
              });
              const totalreducer = (accumulator, currentValue) => accumulator + currentValue;
              totalscore = Math.ceil(savescore.reduce(totalreducer) / savescore.length);
              return { savescore, totalscore, report, assessmentId };
            }
            return { savescore, totalscore, report, assessmentId };
          }),
          map(({ savescore, totalscore, report, assessmentId }) => {
            if (assessmentId) {
              return getRecommend(assessmentId).subscribe((res) => {
                this.setState({
                  title: '#' + report.assessmentName + ' * ' + formatDate(report.updatedTime),
                  report: report.report,
                  savescore,
                  totalscore,
                  recommend: res,
                  reportName: report.assessmentName,
                  assessmentId,
                });
              });
            }
          })
        )
        .subscribe((res) => {})
    );
  }

  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  _openRecommendation = (activeRecommendation) => {
    this.setState({ activeRecommendation });
  };

  _closeRecommendation = () => {
    this.setState({ activeRecommendation: null });
  };

  onClickPrint = (form) => {
    const { fullName, phoneNumber, email, jobTitle, country } = form;
    showSpinner();
    this.onShowReportForm(false);
    this._subscriptions.push(
      sendEmailReport(this.props.match.params.id, {
        nameAssessmentTool: this.state.reportName, // currently nameAssessmentTool is assessmentName . Update latter
        fullName,
        phoneNumber,
        email,
        jobTitle,
        country,
      }).subscribe(
        (res) => {
          showSuccessToast(`We have already sent you an email (${email}) with attached report`);
          this.setState({
            fullName: '',
            phoneNumber: '',
            email: '',
            jobTitle: '',
            country: DEFAULT_COUNTRY,
            agreeTerm: false,
          });
        },
        () => {
          showErrorToast('Has error occured, please try again');
        },
        () => {
          hideSpinner();
        }
      )
    );
  };

  onShowReportForm = (show) => {
    this.setState({
      showReportForm: show,
    });
  };

  render() {
    const {
      report,
      title,
      totalscore,
      reportName,
      activeTab,
      showReportForm,
      savescore,
      percent,
      activeRecommendation,
      recommend,
      assessmentId,
    } = this.state;
    const toc = [
      { title: 'Overall Assessment Score', href: '#top' },
      ...report.map((r, i) => ({
        title: `${i + 1}. ${r.category}`,
        href: '#' + titleToId(r.category, activeTab),
      })),
    ];
    return (
      <div className="report-detail">
        <Row className="assessment-report-nav" align="middle">
          <Col className="assessment-report-banner">
            <div className="assessment-title">
              <div>
                {reportName && (
                  <Breadcrumbs
                    list={[
                      { title: 'Cloud Assessment', to: '/' },
                      { title: 'My Assessments', to: '/assessments' },
                      { title: reportName, to: `/assessments/do/${assessmentId}` },
                      { title: 'Report' },
                    ]}
                  />
                )}
                <div className="assessment-report-title">{reportName && `${reportName} Report`}</div>
              </div>
              <Button
                disabled={reportName === ReportAssessment.Strategy}
                type="success"
                className="download-report-button"
                onClick={() => this.onShowReportForm(true)}
              >
                Download
              </Button>
            </div>
          </Col>
        </Row>
        <Row className="tabs-menu" justify="center">
          <Col className="tabs-body">
            {reportName && reportName !== ReportAssessment.Strategy ? (
              <Tabs
                defaultActiveKey="1"
                activeKey={activeTab}
                onChange={(activeTab) => this.setState({ activeTab })}
                className="tab-detail"
              >
                <TabPane tab="Score" key="1">
                  <Score
                    totalscore={totalscore}
                    report={report}
                    savescore={savescore}
                    percent={percent}
                    activeRecommendation={activeRecommendation}
                    reportName={reportName}
                    recommend={recommend}
                    toc={toc}
                    openRecommendation={this._openRecommendation}
                    closeRecommendation={this._closeRecommendation}
                  />
                </TabPane>
                <TabPane tab="Heatmap" key="2" forceRender={true}>
                  <Heatmap report={report} toc={toc} />
                </TabPane>
                <TabPane tab="Radar" key="3" forceRender={true}>
                  <Radar title={title} report={report} />
                </TabPane>
                {reportName && reportName === ReportAssessment.Decision ? (
                  <TabPane tab="Recommendation" key="4" forceRender={true}>
                    <Decision recommend={recommend} toc={toc} />
                  </TabPane>
                ) : null}
              </Tabs>
            ) : null}
            {reportName && reportName === ReportAssessment.Strategy ? (
              <Tabs
                defaultActiveKey="1"
                activeKey={activeTab}
                onChange={(activeTab) => this.setState({ activeTab })}
                className="tab-detail"
              >
                <TabPane tab="Overall" key="1">
                  <Strategy recommend={recommend} report={report} />
                </TabPane>
              </Tabs>
            ) : null}
          </Col>
        </Row>
        {showReportForm && (
          <DownloadForm
            reportName={reportName}
            show={showReportForm}
            onCancel={() => this.onShowReportForm(false)}
            onSubmit={this.onClickPrint}
            defaultCountry={DEFAULT_COUNTRY}
          />
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  navStyle: state.settings.navStyle,
});
const mapDispatchToProps = (dispatch) => ({
  changeNavStyle: () => dispatch(onNavStyleChange(NAV_STYLE_DARK_HORIZONTAL)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AssessmentReport);
