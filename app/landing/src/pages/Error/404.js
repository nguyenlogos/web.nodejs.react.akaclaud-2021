import React from "react";

const Error404 = ({ }) => {

    return <div className="gx-page-error-container">
        <div className="gx-page-error-content">
            <div className="gx-error-code">404</div>
            <h2 className="gx-text-center">permission denied</h2>

            <p className="gx-text-center">
                {/* <Link className="gx-btn gx-btn-primary" to="/">
                    Go to Home
        </Link> */}
            </p>

            
        </div>
    </div>;
};

export default Error404;