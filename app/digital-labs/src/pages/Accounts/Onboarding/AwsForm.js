import { AWS_GUIDELINE_URL } from '@common';
import { Button, Form, Input, Spin } from 'antd';
import classnames from 'classnames';
import React from 'react';

const FormItem = Form.Item;

const AwsOnBoardingForm = ({ className, onSubmit, onSubmitFailed, isLoading }) => {
  const formClassnames = classnames('onboarding-form', className);

  return (
    <Form name="aws-onboarding" className={formClassnames} onFinish={onSubmit} onFinishFailed={onSubmitFailed}>
      <p className="sm-notes">
        <b>
          Note:
        </b>
        &nbsp;You can use existing cloud account to study&nbsp;
        <a href={AWS_GUIDELINE_URL} target="_blank" className="guideline" rel="noopener noreferrer">
          (Guideline)
        </a>
      </p>

      <FormItem name="accessKey" rules={[{ required: true, message: 'Please input your Assess key!' }]}>
        <Input placeholder="Enter AWS Assess key" />
      </FormItem>

      <FormItem name="secretKey" rules={[{ required: true, message: 'Please confirm your Secret key!' }]}>
        <Input placeholder="Enter AWS Secret key" />
      </FormItem>

      {isLoading && (
        <div className="gx-text-center">
          <Spin tip="Account is creating. Please wait until process is complete" />
        </div>
      )}

      <FormItem>
        <Button type="primary" htmlType="submit" className="gx-full-width">
          Save
        </Button>
      </FormItem>
    </Form>
  );
};

export default AwsOnBoardingForm;
