import { switchLanguage } from '@appRedux/actions';
import { toggleCollapsedSideNav } from '@appRedux/actions/Setting';
import { SUPPORT_LANGUAGES } from '@common/languages';
import AppNotification from '@components/AppNotification';
import Auxiliary from '@components/Auxiliary';
import CustomScrollbars from '@components/CustomScrollbars';
import SearchBox from '@components/SearchBox';
import UserMenu from '@components/UserMenu';
import { NAV_STYLE_DRAWER, NAV_STYLE_FIXED, NAV_STYLE_MINI_SIDEBAR, TAB_SIZE } from '@constants';
import { Layout, Popover } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const { Header } = Layout;

const Topbar = () => {
  const { width, navStyle, locale } = useSelector(({ settings }) => settings);
  const { navCollapsed } = useSelector(({ common }) => common);
  const { searchText, setSearchText } = useState('');
  const dispatch = useDispatch();

  const updateSearchChatUser = (evt) => {
    setSearchText(evt.target.value);
  };

  const languageMenu = () => (
    <CustomScrollbars className="gx-popover-lang-scroll">
      <ul className="gx-sub-popover">
        {SUPPORT_LANGUAGES.map(language =>
          (<li className="gx-media gx-pointer" key={JSON.stringify(language)} onClick={(e) =>
            dispatch(switchLanguage(language))}
          >
            <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`} />
            <span className="gx-language-text">
              {language.name}
            </span>
          </li>)
        )}
      </ul>
    </CustomScrollbars>);

  return (
    <>
      <Header className="gx-sm-only">
        {navStyle === NAV_STYLE_DRAWER ||
          ((navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR) && width < TAB_SIZE) ? (
            <div className="gx-linebar gx-mr-3">
              <i className="gx-icon-btn icon icon-menu"
                onClick={() => {
                  dispatch(toggleCollapsedSideNav(!navCollapsed));
                }}
              />
            </div>
          ) : null}

        <ul className="gx-header-notifications gx-ml-auto">
          <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
            <Popover
              overlayClassName="gx-popover-horizantal"
              placement="bottomRight"
              content={
                <SearchBox
                  styleName="gx-popover-search-bar"
                  placeholder="Search in app..."
                  onChange={updateSearchChatUser}
                  value={searchText}
                />
              }
              trigger="click"
            >
              <span className="gx-pointer gx-d-block">
                <i className="icon icon-search-new" />
              </span>
            </Popover>
          </li>
          <Auxiliary>
            <li className="gx-notify">
              <Popover
                overlayClassName="gx-popover-horizantal"
                placement="bottomRight"
                content={<AppNotification />}
                trigger="click"
              >
                <span className="gx-pointer gx-d-block">
                  <i className="icon icon-notification" />
                </span>
              </Popover>
            </li>
          </Auxiliary>

          <li className="gx-language">
            <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={languageMenu()}
              trigger="click"
            >
              <span className="gx-pointer gx-flex-row gx-align-items-center">
                <i className={`flag flag-24 flag-${locale.icon}`} />
                <span className="gx-pl-2 gx-language-name">
                  {locale.name}
                </span>
                <i className="icon icon-chevron-down gx-pl-2" />
              </span>
            </Popover>
          </li>

          <Auxiliary>
            <li className="gx-user-nav">
              <UserMenu />
            </li>
          </Auxiliary>
        </ul>
      </Header>

      <div className="gx-no-header-horizontal gx-xl-only">
        <div className="gx-d-block gx-d-lg-none gx-linebar gx-mr-xs-3">
          <i
            className="gx-icon-btn icon icon-menu"
            onClick={() => {
              dispatch(toggleCollapsedSideNav(!navCollapsed));
            }}
          />
        </div>
      </div>
    </>
  );
};

export default Topbar;
