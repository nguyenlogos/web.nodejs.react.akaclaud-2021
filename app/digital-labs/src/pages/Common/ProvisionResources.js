import { CheckCircleTwoTone, CloseCircleTwoTone, SyncOutlined } from '@ant-design/icons';
import { ResourceState } from '@constants';
import { Table } from 'antd';
import React from 'react';

const ResourceTableColumns = [
  {
    title: '#',
    dataIndex: 'resourceId',
    key: 'resourceId',
    width: 40,
    align: 'center',
    render: (text, record, index) => {
      return (<span>
        {index + 1}
      </span>);
    },
  },
  {
    title: 'Service',
    dataIndex: 'resourceId',
    key: 'resourceId',
  },
  {
    title: 'Name',
    dataIndex: 'resourceType',
    key: 'resourceType',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    align: 'center',
    render: (text) => {
      return (
        <div className="status">
          {text === ResourceState.Running && <CheckCircleTwoTone twoToneColor="#52c41a" />}
          {text === ResourceState.Creating && <SyncOutlined spin={true} />}
          {text === ResourceState.Failed && <CloseCircleTwoTone />}
        </div>
      );
    },
  },
];

const ProvisionResources = ({ provisonResources }) => {
  const { resources } = provisonResources;

  const showResources = resources
    ? resources.map((element, index) => {
      element['key'] = index;

      return element;
    })
    : [];

  return (
    <Table pagination={false} bordered={true} columns={ResourceTableColumns} dataSource={showResources} size="small" />
  );
};

export default ProvisionResources;
