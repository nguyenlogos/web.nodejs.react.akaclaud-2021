import { Col, Row } from 'antd';
import * as React from 'react';

import StrategyRecommendation from './StrategyRecommendation';

const Strategy = (props) => {
  const { recommend, report } = props;
  return (
    <Row className="strategy-tab report-tab">
      <Col className="strategy-col" md={24} xl={20}>
        <div className="recommendation">
          <div className="overall-recommendation">
            <div className="recommendation_title overall-recommendation_title">Overall Assessment Score</div>
            <p className="recommendation_content">
              This Decision Point presents cloud migration strategies as a choice between fives alternatives acion.
            </p>
            <p className="recommendation_content">
              However it does not go into the specifics of migrating the application to a specific cloud platform on a
              specific cloud provider. This Decision Point leads readers to an appropriate migration stategy rather than
              toward a particular service provider.
            </p>
            <p className="recommendation_content">
              The alternative migration strategies in this Decision Point are explained and differentiated in this
              section. No matter which alternatives you choose, make sure you pair your choice with a clear strategy to
              decommission the existing version or instances of the application. Many cloud migration projects have been
              derailed by an inability to move the last few stragglers off of the legacy solution.
            </p>
          </div>
          <div className="recommendation-report">
            {recommend &&
              recommend.map((recommendation, index) => (
                <StrategyRecommendation
                  key={index}
                  recommendationIndex={index}
                  recommendation={recommendation}
                  report={report}
                />
              ))}
          </div>
        </div>
      </Col>
    </Row>
  );
};
export default Strategy;
