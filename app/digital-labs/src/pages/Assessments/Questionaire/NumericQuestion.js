import { saveAnswer } from '@api';
import { InputNumber } from 'antd';
import * as React from 'react';

export default class NumericQuestion extends React.Component {
  subscriptions = [];
  constructor(props) {
    super(props);

    this.state = {
      answer: props.question.answer,
      show: true,
    };
  }
  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onChange = (value) => {
    const { question, assessmentId } = this.props;

    const questionObject = {
      id: question.id,
      question: question.question,
      domain: question.domain,
      subDomain: question.subDomain,
      type: question.type,
      answer: value,
    };

    this.subscriptions.push(
      saveAnswer(assessmentId, questionObject).subscribe(
        () => {
          this.props.callbackFromParent(question);
          this.setState({ answer: value });
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      )
    );
  };

  render() {
    const { show, answer } = this.state;
    const { question, unanswered } = this.props;

    return (
      <div className={unanswered === question.key ? 'unanswered' : null}>
        {show && (
          <div ref={this._questionRef} className="title-question">
            <div className="content-question">
              <b>
                Question
                {question.key}
              </b>{' '}
              :{question.question}
            </div>
            <div className="radio">
              <InputNumber min={1} onChange={this.onChange} defaultValue={answer} />
            </div>
          </div>
        )}
      </div>
    );
  }
}
