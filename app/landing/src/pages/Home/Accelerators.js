import React from "react";
import { Row, Col, Carousel } from "antd";
import DownloadPdf from './DownloadPdf';
import { ACCELERATORS } from "@constants/Landing"

const Accelerators = () => (<>
    <Row className="accelerators" justify="center">
        <Col xs={24} lg={20}>
            <div className="accelerators-title">
                <h2>Our Accelerators</h2>
                <p>akaClaud – FPT Digital Kaizen&trade; Suite for Cloudification</p>
            </div>
            <div className="accelerators-diagram web-layout">
                <img src="/landing/OurAcc.png" alt="" />
            </div>
            <div className="accelerators-diagram mobile-layout">

                <Row className="mobile-layout" justify="center">
                    <Col xs={24}>
                        <Carousel autoplasy>
                            {ACCELERATORS.map(({ title, description, imageUrl, checklist }, i) => (
                                <div className="values-item" key={i}>
                                    <div className="accelerators-image">
                                        <img src={imageUrl} alt="" />
                                    </div>
                                    <h3>{title}</h3>
                                    <p>{description}</p>
                                    <ul className="checklist">
                                        {checklist.map((item, j) => {
                                            if (item.children) {
                                                return (
                                                    <ul className="checklist-children">
                                                        {item.children.map((e, k) => (
                                                            <li key={k}>{e}</li>
                                                        ))}
                                                    </ul>
                                                )
                                            }
                                            return (
                                                <li key={j}>{item.value}</li>
                                            )
                                        }
                                        )}
                                    </ul>
                                </div>
                            ))}
                        </Carousel>
                    </Col>
                </Row>
            </div>
        </Col>
    </Row>

    <DownloadPdf
        title="akaClaud – FPT Digital Kaizen&trade;"
        subTitle="Our full cloud services & solutions for you"
        fileName="akaClaud-Introduction-v1.1.1.pdf"
        btnText="Introduction"
    />
</>);

export default Accelerators;