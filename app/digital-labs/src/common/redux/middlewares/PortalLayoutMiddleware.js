import { NAV_STYLE_DARK_HORIZONTAL, NAV_STYLE_FIXED } from '@constants';
import { onNavStyleChange } from '../actions';

const portalLayoutHandler = (store) => (next) => (action) => {
  if (action.type === '@@router/LOCATION_CHANGE') {
    const { pathname } = action.payload.location;
    const { settings } = store.getState();
    const { navStyle } = settings;

    if (pathname != null && /\/portals\//i.test(pathname)) {
      if (navStyle !== NAV_STYLE_FIXED) {
        store.dispatch(onNavStyleChange(NAV_STYLE_FIXED));
      }
    } else {
      if (navStyle !== NAV_STYLE_DARK_HORIZONTAL) {
        store.dispatch(onNavStyleChange(NAV_STYLE_DARK_HORIZONTAL));
      }
    }
  }

  next(action);
};

export default portalLayoutHandler;
