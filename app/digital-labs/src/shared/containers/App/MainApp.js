import { Layout } from 'antd';
import React, { useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import App from '@pages';
import Sidebar from '../Sidebar';
import NavStyle from './NavStyle';
import { updateToken } from '@common/authorization';
import { useSelector } from 'react-redux';
import OpenNotificationLogin from '@components/OpenNotificationLogin';

const { Content, Footer } = Layout;

const MainApp = () => {
  const match = useRouteMatch();
  const tokenExpired = useSelector((state) => state.auth.tokenExpired);
  const authUser = useSelector((state) => state.auth.authUser);
  const [wapperClass, setWapperClass] = useState(false);
  
  if (Boolean(tokenExpired) === true && authUser) {
    updateToken()
      .then((accesstoken) => {
        window.location.reload();
      })
      .catch((err) => {
        localStorage.clear();
        OpenNotificationLogin();
      });
  }
  console.log('wapperClass', wapperClass);
  return (
    <Layout className={`gx-app-layout ${wapperClass? 'gx-app-layout-without-sidebar' : ''}`}>
      <Sidebar setWapperClass={(val) => setWapperClass(val)} />
      <Layout>
        <NavStyle />
        <Content
          className={'gx-layout-content gx-container-wrap'}
          id="body-content"
        >
          <App match={match} />
          <Footer>
            <div className="gx-layout-footer-content">
              © akaClaud, FPT Software. All Rights Reserved.
            </div>
          </Footer>
        </Content>
      </Layout>
    </Layout>
  );
};
export default MainApp;
