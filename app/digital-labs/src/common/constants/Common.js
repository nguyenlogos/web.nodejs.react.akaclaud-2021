export const QuestionType = {
  Rating: 'Rating',
  YesNo: 'YesNo',
  Text: 'Text',
  Numeric: 'Numeric',
};

export const LabDetailTab = {
  BusinessStory: '1',
  BeforeYouStart: '2',
  Solution: '3',
  Guidance: '4',
};

export const TrainingLabIndex = {
  BusinessStory: '1',
  TrainingLab: '2',
};

export const AccountBilling = {
  FPTBilling: 0,
  CustomerBilling: 1,
};

export const LabState = {
  pending: 'pending',
  rejected: 'rejected',
  ready: 'ready',
};

export const LabDetailState = {
  Ready: 0,
  Creating: 1,
  Running: 2,
  Failed: 5,
};

export const ResourceState = {
  Init: 0,
  Creating: 1,
  Running: 2,
  Stopping: 3,
  Stopped: 4,
  Failed: 5,
};

export const RenderInput = {
  RESOURCE_TEMPLATE: 0,
  RESOURCE_PARAMETER: 1,
  LAUNCH_POLICY: 2,
};

export const HOOK_AUTHORIZATION_TYPE = 'HOOK_AUTHORIZATION_TYPE';

export const AUTHORIZATION_CODE = {
  TokenMissing: 0,
  TokenExpired: 1,
  PermissionMissing: 2,
};

export const CloudProvider = {
  Aws: 'aws',
  Azure: 'azure',
  Gcp: 'gcp',
};

export const TARGET_OFFSET = 20;

export const HTTP_CODES = {
  Unauthorized: 401,
};

export const AccountType = {
  UserAccount: 1,
  FPTAccount: 2,
};

export const MessageAfterValidate = {
  Success: 'SUCCESS',
  False: 'FALSE',
};

export const DigitalLabType = ['Training', 'References Business Case'];

export const DigitalLabTypeValue = {
  Training: 'Training',
  ReferencesSolutions: 'References Business Case',
};

export const FilterType = {
  category: 'Category',
  vendor: 'Vendor',
  level: 'Level',
  keyword: 'keyword',
};
