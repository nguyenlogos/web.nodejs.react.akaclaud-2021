import { Col, Row } from 'antd';
import React, { Component } from 'react';

import HtmlAttachment from '../HtmlAttachment';

class BeforeStart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      beforeStart: props.defaultValue ? props.defaultValue.beforeStart : null,
    };
  }

  validateInput = () => {
    return this.state.beforeStart ? true : false;
  };

  onBeforeStartChange = (context) => {
    this.setState(
      {
        beforeStart: context,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { beforeStart } = this.state;
    return (
      <div className="lab-step-prerequisite">
        <Row>
          <Col span={24}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="guidance">
                  Before You Start
                </label>
                <HtmlAttachment source={beforeStart} onBlur={this.onBeforeStartChange} />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default BeforeStart;
