import { Col, Row } from 'antd';
import React, { Component } from 'react';

import HtmlAttachment from '../HtmlAttachment';

class GuideLine extends Component {
  constructor(props) {
    super(props);

    this.state = {
      guidline: props.defaultValue ? props.defaultValue.guidline : null,
    };
  }

  validateInput = () => {
    return this.state.guidline ? true : false;
  };

  onGuidelineChange = (context) => {
    this.setState(
      {
        guidline: context,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { guidline } = this.state;
    return (
      <div className="lab-step-guideLine">
        <Row>
          <Col span={24}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="guidance">
                  Digital Lab
                </label>
                <HtmlAttachment source={guidline} onBlur={this.onGuidelineChange} />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default GuideLine;
