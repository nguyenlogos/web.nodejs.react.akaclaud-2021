import { getAccountsByUser, registerAccount } from '@api';
import awsLogo from '@assets/images/aws_logo.svg';
import azureLogo from '@assets/images/azure_logo.svg';
import googleLogo from '@assets/images/gcp_logo.svg';
import { openNotification } from '@common/helpers';
import { AccountBilling, AccountType, CloudProvider } from '@constants';
import { Alert, Button, Card, Col, Radio, Row, Spin } from 'antd';
import classnames from 'classnames';
import React from 'react';

import AwsOnBoardingForm from './AwsForm';
import AzureOnBoardingForm from './AzureForm';
import GcpOnBoardingForm from './GcpForm';

class AccountOnBoarding extends React.Component {
  _subscriptions = [];
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      accounts: [],
      accountType: AccountType.UserAccount,
      provider: props.location.state != null &&
        props.location.state.provider != null ? props.location.state.provider : CloudProvider.Aws,
    };
  }

  componentDidMount() {
    this.$loadAccountStatus();
  }

  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  $loadAccountStatus = () => {
    this._subscriptions.push(
      getAccountsByUser().subscribe((res) => {
        const accounts = res.dataSources;

        this.setState({
          accounts,
        });
      })
    );
  };

  isExistingAccount = () => {
    const { accounts, provider } = this.state;

    return accounts.find((acc) => acc.vendor === provider);
  };

  onProviderChange = (provider) => {
    this.setState({ provider });
  };

  handleChangeAccount = (e) => {
    const accountType = e.target.value;

    this.setState({
      accountType,
    });
  };

  handleSubmit = (values) => {
    this.setState({
      isLoading: true,
    });

    const { history } = this.props;
    const { accountType, provider } = this.state;
    const { accessKey, secretKey, tenantId, subscriptionId } = values;
    const accountPayload = {
      vendor: provider,
      accessKey: accountType === AccountType.UserAccount ? accessKey : null,
      secretKey: accountType === AccountType.UserAccount ? secretKey : null,
      tenantId: accountType === AccountType.UserAccount ? tenantId : null,
      subscriptionId: accountType === AccountType.UserAccount ? subscriptionId : null,
      billingOwner:
        accountType === AccountType.UserAccount ? AccountBilling.CustomerBilling : AccountBilling.FPTBilling,
    };

    this._subscriptions.push(
      registerAccount(accountPayload).subscribe(
        () => {
          this.setState({
            isLoading: false,
          });
          openNotification('success', 'Setup cloud account onboarding successfully');

          history.goBack();
        },
        (e) => {
          this.setState({
            isLoading: false,
          });
          openNotification('warning', e.response.data ? e.response.data : 'Setup cloud account onboarding failed!');
        }
      )
    );
  };

  handleSubmitFailed = () => {
    openNotification('error', 'Setup cloud account onboarding failed!');
  };

  render() {
    const { accountType, provider, isLoading } = this.state;

    const awsClassnames = classnames('gx-icon-views provider', {
      'active-provider': provider === CloudProvider.Aws,
    });
    const azureClassnames = classnames('gx-icon-views provider', {
      'active-provider': provider === CloudProvider.Azure,
    });
    const gcpClassnames = classnames('gx-icon-views provider', {
      'active-provider': provider === CloudProvider.Gcp,
    });

    return (
      <div className="gx-login-container on-boarding">
        <div className="gx-login-content">
          <div className="gx-text-center">
            <h1 className="gx-login-title">
              Choose Your Provider
            </h1>
          </div>
          <Row justify="center">
            <Col span={8}>
              <div className={awsClassnames} onClick={() => this.onProviderChange(CloudProvider.Aws)}>
                <img src={awsLogo} alt="Amazon Web Service" />
              </div>
            </Col>
            <Col span={8}>
              <div className={azureClassnames} onClick={() => this.onProviderChange(CloudProvider.Azure)}>
                <img src={azureLogo} alt="Microsoft Azure" />
              </div>
            </Col>
            <Col span={8}>
              <div className={gcpClassnames} onClick={() => this.onProviderChange(CloudProvider.Gcp)}>
                <img src={googleLogo} alt="Google Cloud Platform" />
              </div>
            </Col>
          </Row>

          <div className="gx-text-center gx-top-header">
            <h1 className="gx-login-title">
              Fill Cloud Account Information
            </h1>
            {this.isExistingAccount() && (
              <Alert
                message={'Your account already onboarding in ' + provider.toUpperCase()}
                type="success"
                showIcon={true}
                className="gx-text-left"
              />
            )}
          </div>
          <Card className="gx-px-3">
            <Radio.Group value={accountType} onChange={this.handleChangeAccount} className="gx-full-width">
              <Row>
                <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Radio value={AccountType.FPTAccount} className="gx-pb-2 vertical-radio">
                    <label>
                      Use FPT cloud account
                    </label>
                  </Radio>
                </Col>
                <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Radio value={AccountType.UserAccount} className="gx-pb-2 vertical-radio">
                    <label>
                      Use existing cloud account
                    </label>
                  </Radio>
                </Col>
              </Row>
              {accountType === AccountType.UserAccount && (
                <>
                  {provider === CloudProvider.Azure && (
                    <AzureOnBoardingForm
                      isLoading={isLoading}
                      onSubmit={this.handleSubmit}
                      onSubmitFailed={this.handleSubmitFailed}
                    />
                  )}
                  {provider === CloudProvider.Aws && (
                    <AwsOnBoardingForm
                      isLoading={isLoading}
                      onSubmit={this.handleSubmit}
                      onSubmitFailed={this.handleSubmitFailed}
                    />
                  )}
                  {provider === CloudProvider.Gcp && (
                    <GcpOnBoardingForm
                      isLoading={isLoading}
                      onSubmit={this.handleSubmit}
                      onSubmitFailed={this.handleSubmitFailed}
                    />
                  )}
                </>
              )}
              <hr />

              {accountType === AccountType.FPTAccount && (
                <div className="gx-pt-2">
                  {isLoading && (
                    <div className="gx-text-center">
                      <Spin tip="Account is creating. Please wait until process is complete" />
                    </div>
                  )}
                  <Button className="gx-full-width" type="primary" onClick={this.handleSubmit}>
                    Save
                  </Button>
                </div>
              )}
            </Radio.Group>
          </Card>
        </div>
      </div>
    );
  }
}

export default AccountOnBoarding;
