import { approveLab, getDocumentText, getLab, rejectLab } from '@api';
import { getFilteredMenu, navigateTo, showSuccessToast, openNotification } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import MarkdownViewer from '@components/MdViewer';
import { Button, Card, Input, Modal, Tabs, Empty } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forkJoin } from 'rxjs';

const { TextArea } = Input;
const { confirm } = Modal;
const TabPane = Tabs.TabPane;
class ReviewLab extends Component {
  constructor(props) {
    super(props);
    this.preLabSubTabURL = React.createRef();
    this.guidanceSubTabURL = React.createRef();
    this.cloudformationURL = React.createRef();
    this.state = {
      id: props.match.params.id,
      showFileReviewModal: false,
      showRejectModal: false,
      validLab: true,

      contentHtml: '',
      content: null,

      commentReject: '',
      messageAfterValidate: '',
      lab: {
        code: '',
        labName: '',
        description: '',
        preLabSubTabURL: '',
        guidanceSubTabURL: '',
        cloudformationURL: '',
        status: '',
        launchPolicy: '',
        timeInMinutes: 0,
        estimateCost: 0,
        cost: 0,
        credit: 0,
        active: true,
      },

      activeTab: '1',
    };
    this._subscriptions = [];
  }

  componentDidMount() {
    this._getLab();
    // get url from first item of Contribution
    this._initMenu();
  }

  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onLinkReviewLabList = () => {
    navigateTo('/portals/contributes/labs/all');
  };

  _initMenu = () => {
    const roles = this.props.user.roles;
    const menuContribution = getFilteredMenu(roles).filter((item) => item.id === 'Contribute');
    if (menuContribution.length !== 0) {
      if (menuContribution[0].children.length !== 0) {
        this.setState({ rootUrl: menuContribution[0].children[0].url });
      }
    }
  };

  _getLab = () => {
    const { id } = this.state;
    this._subscriptions.push(
      getLab(id).subscribe((res) => {
        if (typeof res.launchPolicy === 'object') {
          res.launchPolicy = JSON.stringify(res.launchPolicy);
        }

        forkJoin(
          [res.preLabSubTabURL, res.guidanceSubTabURL, res.launchPolicy, res.cloudformationURL].map((file) => {
            return getDocumentText(res.code, file);
          })
        ).subscribe(
          ([_preLabSubTabURL, _guidanceSubTabURL, _launchPolicy, _cloudformationURL]) => {
            const lab = {
              code: res.code,
              labName: res.labName,
              description: res.description,
              preLabSubTabURL: typeof _preLabSubTabURL == 'number' ? _preLabSubTabURL.toString() : _preLabSubTabURL,
              guidanceSubTabURL:
                typeof _guidanceSubTabURL == 'number' ? _guidanceSubTabURL.toString() : _guidanceSubTabURL,
              cloudformationURL:
                typeof _cloudformationURL == 'number' ? _cloudformationURL.toString() : _cloudformationURL,
              status: res.status,
              launchPolicy: typeof _launchPolicy == 'number' ? _launchPolicy.toString() : _launchPolicy,
              timeInMinutes: res.timeInMinutes,
              estimateCost: res.estimateCost,
              cost: res.cost,
              credit: res.credit,
              active: res.active,
            };

            this.setState({ lab });
          },
          (err) => {
            console.log(err);
            this.setState({ validLab: false });
            openNotification('error', 'Can not get content lab');
          }
        );
      })
    );
  };

  onCostChange = (e) => {
    const { value } = e.target;
    this.setState((prevState) => {
      const lab = Object.assign({}, prevState.lab);
      lab.cost = value;
      return { lab };
    });
  };

  onCreditChange = (e) => {
    const { value } = e.target;
    this.setState((prevState) => {
      const lab = Object.assign({}, prevState.lab);
      lab.credit = value;
      return { lab };
    });
  };

  prettyJson = (content) => {
    if (!content) {
      return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
    }
    if (typeof content === 'object') {
      return JSON.stringify(content, undefined, 4);
    } else {
      try {
        const obj = JSON.parse(content);
        const pretty = JSON.stringify(obj, undefined, 4);

        return pretty;
      } catch (err) {
        return content;
      }
    }
  };

  handleCommentReject = (e) => {
    this.setState({ commentReject: e.target.value });
  };

  visibleRejectModal = () => {
    this.setState({ showRejectModal: true });
  };

  approveLab = () => {
    confirm({
      title: 'Verify lab',
      content: 'Do you want to approve this lab ?',
      onOk: () => {
        const { id, lab } = this.state;
        this._subscriptions.push(
          approveLab(id, '', lab.cost, lab.credit).subscribe(
            (res) => {
              showSuccessToast('Approve cloudformation success');
              this.onLinkReviewLabList();
            },
            (error) => {
              openNotification('error', 'Approve cloudformation False');
            }
          )
        );
      },
    });
  };

  rejectLab = () => {
    const { id, commentReject } = this.state;
    this._subscriptions.push(
      rejectLab(id, commentReject).subscribe(
        (res) => {
          this.onLinkReviewLabList();
          showSuccessToast('Reject cloudformation Success');
        },
        (error) => {
          this.onLinkReviewLabList();
          openNotification('error', 'Reject cloudformation False');
        }
      )
    );
  };

  handleCancel = () => {
    this.setState({
      showFileReviewModal: false,
      contentHtml: '',
      content: '',
      showRejectModal: false,
    });
  };

  renderLaunchPolicy = () => {
    const { lab } = this.state;
    return (
      <div>
        <pre>
          {this.prettyJson(lab.launchPolicy)}
        </pre>
      </div>
    );
  };

  renderCloudformationTemplate = () => {
    const { lab } = this.state;
    return (
      <div>
        <pre>
          {this.prettyJson(lab.cloudformationURL)}
        </pre>
      </div>
    );
  };

  renderGuidance = () => {
    const { lab } = this.state;
    return (
      <div>
        {lab.guidanceSubTabURL ? (
          <MarkdownViewer src={lab.guidanceSubTabURL} />
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    );
  };

  renderBusinessCase = () => {
    const { lab } = this.state;
    return (
      <div>
        {lab.preLabSubTabURL ? (
          <MarkdownViewer src={lab.guidanceSubTabURL} />
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    );
  };

  renderLabInformation = () => {
    const { lab } = this.state;
    lab.description = lab.description.replace(/<p>|<\/p>/gi, '');
    return (
      <div>
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Lab Name
          </p>
        </div>
        <p>
          {lab.labName}
        </p>
        <hr />
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Lab Description
          </p>
        </div>
        <p>
          {lab.description}
        </p>
        <hr />
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Number of minutes in launch lab
          </p>
        </div>
        <p>
          {lab.timeInMinutes}
          {' '}
          minutes
        </p>
        <hr />
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Estimate Cost
          </p>
        </div>
        <p>
          {lab.estimateCost}
          {' '}
          USD
        </p>
        <hr />
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Set Cost (USD)
          </p>
        </div>
        <Input
          style={{ width: '20%' }}
          type="number"
          min={1}
          name="cost"
          value={lab.cost}
          onChange={this.onCostChange}
        />
        <hr />
        <div style={{ fontWeight: 'bold' }}>
          <p>
            Set Credit (Akaclaud credit)
          </p>
        </div>
        <Input
          style={{ width: '20%' }}
          type="number"
          min={1}
          name="credit"
          value={lab.credit}
          onChange={this.onCreditChange}
        />
      </div>
    );
  };

  renderVerifyLab = () => {
    const {
      showFileReviewModal,
      showRejectModal,
      commentReject,
      contentHtml,
      content,
      validLab,
    } = this.state;

    return (
      <div className="reviewlab-verify">
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Button disabled={!validLab} onClick={this.approveLab} type="primary" style={{ width: '100px' }}>
            Approve
          </Button>
          <Button
            onClick={this.visibleRejectModal}
            type="danger"
            style={{
              width: '100px',
            }}
          >
            Reject
          </Button>
          <Button onClick={this.onLinkReviewLabList} style={{ width: '100px' }}>
            Cancel
          </Button>
        </div>

        <Modal
          title="File preview"
          visible={showFileReviewModal}
          footer={null}
          width={800}
          onCancel={this.handleCancel}
        >
          {contentHtml && <MarkdownViewer src={contentHtml} />}
          {content && (
            <div>
              <pre>
                {this.prettyJson(content)}
              </pre>
            </div>
          )}
        </Modal>

        <Modal title="Reject confirm" footer={null} visible={showRejectModal} onCancel={this.handleCancel}>
          <p>
            "Do you want to reject this lab ? Please input reason."
          </p>
          <TextArea rows={4} onChange={this.handleCommentReject} />
          <div style={{ marginTop: '1.875rem' }}>
            <Button disabled={commentReject.trim().length === 0} type="danger" onClick={this.rejectLab}>
              Reject
            </Button>
            <Button onClick={this.handleCancel}>
              Cancel
            </Button>
          </div>
        </Modal>
      </div>
    );
  };

  render() {
    const { lab } = this.state;
    return (
      <div className="reviewlab">
        <div style={{ marginBottom: '1.875rem' }}>
          <Breadcrumbs
            list={[
              { title: 'My Contribution', to: this.state.rootUrl },
              { title: 'All lab', to: '/portals/contributes/labs/all' },
              { title: 'Review Labs' },
            ]}
          />
          <div style={{ fontSize: '1.5rem', fontWeight: 'bold', lineHeight: 1.75, color: '#121619' }}>
            <span>
              {lab.labName}
            </span>
          </div>
        </div>
        {this.renderVerifyLab()}
        <Card className="gx-card">
          <Tabs defaultActiveKey="1">
            <TabPane tab="Lab Information" key="1">
              {this.renderLabInformation()}
            </TabPane>
            <TabPane tab="Business Case" key="2">
              {this.renderBusinessCase()}
            </TabPane>
            <TabPane tab="Guidance" key="3">
              {this.renderGuidance()}
            </TabPane>
            <TabPane tab="Cloudformation template" key="4">
              {this.renderCloudformationTemplate()}
            </TabPane>
            <TabPane tab="Launch policy" key="5">
              {this.renderLaunchPolicy()}
            </TabPane>
          </Tabs>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, {})(ReviewLab);
