import { RedoOutlined, SearchOutlined } from '@ant-design/icons';
import { getMyCredit, getMyCreditHistory } from '@api';
import { formatDateLocaleString, openNotification, truncateText } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Button, Card, Col, Divider, Input, Row, Table, Tag, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE } from '@constants';

const UserCredit = () => {
  const subscriptions = [];
  const [userCredit, setUserCredit] = useState({
    dataSource: [],
    totalCount: 0,
    page: DEFAULT_ADMIN_PAGE_INDEX,
    pageSize: DEFAULT_ADMIN_PAGE_SIZE,
  });
  const [loading, setLoading] = useState(false);
  const [keyword, setKeyword] = useState('');

  const [userName, setUserName] = useState('');
  const [credit, setCredit] = useState(0);
  const [debit, setDebit] = useState(0);
  const [balance, setBalance] = useState(0);

  const fnEffectHook = () => {
    loadUserCredit(keyword, DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE);
    loadUserAccounting();

    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const loadUserAccounting = () => {
    subscriptions.push(
      getMyCredit().subscribe((accounting) => {
        const { userName, credit, debit, balance } = accounting;
        setUserName(userName);
        setCredit(credit);
        setDebit(debit);
        setBalance(balance);
      })
    );
  };

  const loadUserCredit = (keyword, page, pageSize) => {
    setLoading(true);
    subscriptions.push(
      getMyCreditHistory(keyword, page, pageSize).subscribe(
        (data) => {
          if (data) {
            const { dataSources: dataSource, count: totalCount, page, size: pageSize } = data;
            const mapDataSources = dataSource
              ? dataSource.map((data, key) => {
                data.key = key;
                return data;
              })
              : [];
            setUserCredit({
              dataSource: mapDataSources,
              totalCount,
              page,
              pageSize,
            });
          }
        },
        () => {
          setLoading(false);
          openNotification('error', 'Loading user credit list failed');
        },
        () => {
          setLoading(false);
        }
      )
    );
  };
  const onPageChange = (page) => {
    const { pageSize } = userCredit;
    const newPage = page > 0 ? page - 1 : 0;
    loadUserCredit(null, newPage, pageSize);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadUserCredit(null, 0, pageSize);
  };

  const onClickSearch = () => {
    const { pageSize } = userCredit;
    loadUserCredit(keyword, 0, pageSize);
  };

  const onReload = () => {
    loadUserCredit(null, 0, 10);
  };

  const keypressSearchAssessments = (e) => {
    const { pageSize } = userCredit;
    if (e.key === 'Enter') {
      loadUserCredit(keyword, 0, pageSize);
    }
  };

  const searchAssessments = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    setKeyword(keyword);
  };


  const buildTableuserCredit = () => {
    const { dataSource, totalCount, pageSize, page } = userCredit;
    const from = page * pageSize + 1;
    const pagination = {
      total: totalCount,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        title: '#',
        key: 'index',
        width: '5%',
        render: (text, record, index) => {
          return from + index;
        },
      },
      {
        title: 'Lab Name',
        key: 'labName',
        dataIndex: 'labName',
        width: '35%',
        render: (text, record, index) => {
          const url = '/labs/view/' + record['labId'];
          return (
            <a href={url} key={index}>
              {text}
            </a>
          );
        },
      },

      {
        title: 'Description',
        key: 'description',
        dataIndex: 'description',
        width: '35%',
        render: (description) => {
          return truncateText(description, 100);
        },
      },
      {
        title: 'Credit',
        key: 'credit',
        dataIndex: 'credit',
        width: '10%',
        render: (text, record, index) => {
          const credit = record['debit'] - record['credit'];
          if (parseInt(credit) === 0) {
            return (
              <Tag color="orange" style={{ width: 50, textAlign: 'center' }}>
                -0
              </Tag>
            );
          } else if (credit > 0) {
            return (
              <Tag color="green" style={{ width: 50, textAlign: 'center' }}>
                {'+' + credit}
              </Tag>
            );
          } else {
            return (
              <Tag color="orange" style={{ width: 50, textAlign: 'center' }}>
                {credit}
              </Tag>
            );
          }
        },
      },
      {
        title: 'Created Time',
        key: 'createdTime',
        dataIndex: 'createdTime',
        width: '15%',
        render: (text) => {
          const timeStr = formatDateLocaleString(text);
          return timeStr;
        },
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={dataSource}
        columns={columns}
        pagination={pagination}
        loading={loading}
      />
    );
  };

  return (
    <>
      <div className="gx-page-header">
        <Breadcrumbs list={[{ title: 'My Account', to: '/portals/accounts' }, { title: 'My Credits' }]} />
        <div style={{ fontSize: '1.5rem', fontWeight: 'bold', lineHeight: 1.75, color: '#121619' }}>
          <span>
            Credit History
          </span>
        </div>
      </div>
      <Card className="gx-card" title="Credit Summary">
        <div style={{ justifyContent: 'space-between', display: 'flex' }}>
          <p>
            User name:
          </p>
          <Tag style={{ marginLeft: '-5%' }} color="blue">
            {userName}
          </Tag>
          <p>
            Added credit:
          </p>
          <Tag style={{ marginLeft: '-5%' }} color="green">
            {'+' + debit}
          </Tag>
          <p>
            Used credit:
          </p>
          <Tag style={{ marginLeft: '-5%' }} color="orange">
            {'-' + credit}
          </Tag>
          <p>
            Ready credit:
          </p>
          <Tag style={{ marginLeft: '-5%' }} color="green">
            {'+' + balance}
          </Tag>
        </div>
      </Card>
      <Card className="gx-card">
        <Row justify="end" align="top">
          <Col span={12}>
            <Space direction="horizontal" align="start" style={{float: 'right'}}>
              <Input
                type="text"
                onKeyPress={keypressSearchAssessments}
                onChange={searchAssessments}
                placeholder="Type keyword to type"
              />
              <Button shape="circle" icon={<SearchOutlined />} onClick={onClickSearch} />
              <Button shape="circle" icon={<RedoOutlined />} onClick={onReload} />
            </Space>
          </Col>
        </Row>
        <Divider />
        {buildTableuserCredit()}
      </Card>
    </>
  );
};

export default UserCredit;
