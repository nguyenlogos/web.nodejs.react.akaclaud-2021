import { from } from 'rxjs';

import { AuthHttp } from './client';

// POST: /api/v1/questions/import
export function createImportQuestion(question) {
  let url = 'questions/import';
  return from(AuthHttp.post(url, question));
}

// PUT: /api/v1/questions/import
export function updateImportQuestion(question) {
  let url = 'questions/import';
  return from(AuthHttp.put(url, question));
}

// GET: /api/v1/questions
export function getAllActiveQuestion() {
  let url = 'questions';
  return from(AuthHttp.get(url));
}

// GET: /api/v1/questions/all
export function getAll() {
  let url = 'questions/all';
  return from(AuthHttp.get(url));
}

// GET: /api/v1/questions/domain
export function getDomain() {
  let url = 'questions/domain';
  return from(AuthHttp.get(url));
}

// GET: /api/v1/questions/subdomain
export function getSubDomain() {
  let url = 'questions/subdomain';
  return from(AuthHttp.get(url));
}

export function deleteQuestion(objectId) {
  let url = `questions/${objectId}`;
  return from(AuthHttp.delete(url));
}

export function createQuestion(question) {
  let url = 'questions';
  return from(AuthHttp.post(url, question));
}

export function updateQuestion(id, question) {
  let url = `questions/${id}`;
  return from(AuthHttp.put(url, question));
}
