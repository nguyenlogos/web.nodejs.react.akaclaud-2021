import asyncComponent from '@components/AsyncComponent';
import SafeRoute from '@components/SafeRoute';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const PortalApp = ({ prefixRoute }) => {
  return (
    <div className="gx-page-layout portals">
      <Switch>
        <Route
          path={`${prefixRoute}/assessments`}
          exact={true}
          component={asyncComponent(() => import('./Assessments/AssessmentHistory'))}
        />

        <Route path={`${prefixRoute}/labs/stacks`} component={asyncComponent(() => import('./Labs/StacksHistory'))} />

        <Route
          path={`${prefixRoute}/accounts`}
          exact={true}
          component={asyncComponent(() => import('./Accounts/UserInfo'))}
        />
        <Route
          path={`${prefixRoute}/accounts/my-credits`}
          component={asyncComponent(() => import('./Accounts/UserCredit'))}
        />

        <SafeRoute
          roles={'admin'}
          path={`${prefixRoute}/admin/credit-management`}
          component={asyncComponent(() => import('./Admin/CreditManagement'))}
        />

        <Route
          path={`${prefixRoute}/contributes/labs`}
          exact={true}
          component={asyncComponent(() => import('./Contributes/MyContributesLab'))}
        />
        <SafeRoute
          roles={'leader'}
          path={`${prefixRoute}/contributes/labs/all`}
          component={asyncComponent(() => import('./Contributes/AllLabs'))}
        />
        <SafeRoute
          roles={'leader'}
          path={`${prefixRoute}/contributes/labs/review/:id`}
          component={asyncComponent(() => import('./Contributes/ReviewLab'))}
        />

        <Route
          path={`${prefixRoute}/contributes/labs/all`}
          exact={true}
          component={asyncComponent(() => import('./Contributes/ReviewContribute'))}
        />
        <Route
          path={`${prefixRoute}/contributes/labs/create`}
          exact={true}
          component={asyncComponent(() => import('./Contributes/LabCreate'))}
        />
        <Route
          path={`${prefixRoute}/contributes/labs/edit/:id`}
          exact={true}
          component={asyncComponent(() => import('./Contributes/LabEdit'))}
        />

        <Route
          path={`${prefixRoute}/contributes/scenario/create`}
          exact={true}
          component={asyncComponent(() => import('./Contributes/ScenarioCreate'))}
        />
      </Switch>
    </div>
  );
};

export default PortalApp;
