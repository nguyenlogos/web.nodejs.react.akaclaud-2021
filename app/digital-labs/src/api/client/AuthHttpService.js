import axios from 'axios';
import { V1_API_URL } from '@common';
import { getAccessToken } from '@common/helpers';
import { HTTP_CODES } from '@constants';
import { debounceRootDispatch } from 'octopus-immutable-store';
import { SESSION_TIMEOUT } from '@constants';

const authHttp = axios.create({
  baseURL: V1_API_URL,
});

authHttp.interceptors.request.use(
  (config) => {
    config.headers['Authorization'] = getAccessToken();
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

authHttp.interceptors.response.use(
  (response) => {
    const res = response.data;

    return res;
  },
  (error) => {
    if (error.response && error.response.status === HTTP_CODES.Unauthorized) {
      debounceRootDispatch(SESSION_TIMEOUT, true);
      return false;
    }

    return Promise.reject(error);
  }
);

export default authHttp;
