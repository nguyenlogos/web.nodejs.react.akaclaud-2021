import { ArrowRightOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';
import * as React from 'react';

import RadarChart from './RadarChart';

const Radar = (props) => (
  <Row>
    <Col xs={24}>
      <div className="radar-report report-tab">
        <div className="radar-header">
          <div className="radar-title tab-title">Cloud Readiness Radar Chart</div>
          <div className="radar-report-content">
            <span>Assessment scores shown in a radar, across the six AWS CAF perspectives.</span>
            <div className="caf-link">
              <a href="#top">
                Learn more about CAF <ArrowRightOutlined />
              </a>
            </div>
            <div className="radar-text">
              <span>
                Visualize your cloud-readiness strengths and weaknesses with the radar below, so you can prioritize
                remediation activities.
              </span>
            </div>
          </div>
        </div>
        <div className="radar-chart">
          <RadarChart title={props.title} questions={props.report} />
        </div>
      </div>
    </Col>
    {/* <Col>s</Col> */}
  </Row>
);
export default Radar;
