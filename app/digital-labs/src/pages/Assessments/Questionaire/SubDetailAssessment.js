import { QuestionType } from '@constants';
import React from 'react';

import ConfirmQuestion from './ConfirmQuestion';
import NumericQuestion from './NumericQuestion';
import RateQuestion from './RateQuestion';
import TextQuestion from './TextQuestion';

export default class SubDetailAssessment extends React.Component {
  callbackFromGrand = (dataFromChild) => {
    this.props.callbackFromGrand(dataFromChild);
  };

  buildConfirmQuestion = (q) => {
    const { unanswered, assessmentId } = this.props;

    return (
      <div className="question" key={q.key}>
        <ConfirmQuestion
          assessmentId={assessmentId}
          question={q}
          callbackFromParent={this.callbackFromGrand}
          unanswered={unanswered}
        />
      </div>
    );
  };

  buildTextQuestion = (q) => {
    const { unanswered, assessmentId } = this.props;

    return (
      <div className="question" key={q.key}>
        <TextQuestion
          assessmentId={assessmentId}
          question={q}
          callbackFromParent={this.callbackFromGrand}
          unanswered={unanswered}
        />
      </div>
    );
  };

  buildRatingQuestion = (q) => {
    const { unanswered, assessmentId } = this.props;

    return (
      <div className="question" key={q.key}>
        <RateQuestion
          assessmentId={assessmentId}
          question={q}
          callbackFromParent={this.callbackFromGrand}
          unanswered={unanswered}
        />
      </div>
    );
  };

  buildNumericQuestion = (q) => {
    const { unanswered, assessmentId } = this.props;

    return (
      <div className="question" key={q.key}>
        <NumericQuestion
          assessmentId={assessmentId}
          question={q}
          callbackFromParent={this.callbackFromGrand}
          unanswered={unanswered}
        />
      </div>
    );
  };

  render() {
    const { questions } = this.props;
    return (
      <div className="subtab-detail">
        {questions.map((q) => {
          if (q.type === QuestionType.YesNo) {
            return this.buildConfirmQuestion(q);
          } else if (q.type === QuestionType.Text) {
            return this.buildTextQuestion(q);
          } else if (q.type === QuestionType.Rating) {
            return this.buildRatingQuestion(q);
          } else if (q.type === QuestionType.Numeric) {
            return this.buildNumericQuestion(q);
          }

          return null;
        })}
      </div>
    );
  }
}
