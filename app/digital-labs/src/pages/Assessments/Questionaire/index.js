import { getAllQuestionInAssessment, getAssessment, getAssessmentReport, reCalculate } from '@api';
import { navigateTo, showErrorToast, showSuccessToast } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import TableOfContent from '@components/TableOfContent';
import { Button, Col, Modal, Row, Tabs } from 'antd';
import * as React from 'react';

import SubDetailAssessment from './SubDetailAssessment';

const { TabPane } = Tabs;
const { confirm } = Modal;

export default class AssessmentDetail extends React.Component {
  subscriptions = [];
  constructor(props) {
    super(props);

    this.state = {
      assessmentId: props.match.params.id,
      //property in question: id,name,domain,subDomain,type
      questions: [],
      // array string main Activekey ( Tabs : ant design )
      domains: [],
      // array string sub Activekey ( Tabs : ant design )
      subDomains: [],
      // current question number
      currentQuesitonKey: null,
      // current string sub tab
      subActivekey: '',
      // current string main tab
      mainActivekey: '',
      // key number un-answer question
      unanswered: null,
      assessmentName: '',
      percent: 0,
      reportId: '',
    };
  }

  /**
   *  Handle answered question from grandChild text-question,rate-question,confirm-question.
   *  @param question object ( from child ) with value
   */
  handleValueFromGrandchildren = (question) => {
    const { questions } = this.state;
    let questionWithValue = [...questions];
    questionWithValue[question.key - 1].answer = question.answer;
    this.setState({ questions: questionWithValue, currentQuesitonKey: question.key });
  };

  componentDidMount() {
    this._getAllQuestion();
    this._getReport();
  }
  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  _getReport = () => {
    const { assessmentId } = this.state;
    this.subscriptions.push(
      getAssessmentReport(assessmentId).subscribe((report) => {
        this.setState({
          reportId: report._id,
        });
      })
    );
  };

  /**
   * Get question: id,name,domain,subDomain,type .
   * Add property key to every question.
   */

  _getAllQuestion() {
    const { assessmentId } = this.state;

    this.subscriptions.push(
      getAssessment(assessmentId).subscribe((assessment) => {
        this.setState({ assessmentName: assessment['assessmentName'] });
      })
    );
    this.subscriptions.push(
      getAllQuestionInAssessment(assessmentId).subscribe((questions) => {
        this._domainsAndSubDomains(questions);
        const _questions = this._addKey(questions);
        this.setState({ questions: _questions, currentQuesitonKey: 0 });
      })
    );
  }

  /**
   * get String array domains and subDomains
   * @param questions
   */
  _domainsAndSubDomains(questions) {
    let domains = [];
    let subDomains = [];

    // array domains without duplicate element
    questions.map((e) => domains.push(e.domain));
    domains = domains.filter((elem, index, self) => {
      return index === self.indexOf(elem);
    });

    // array subDomains: each element domain compare with array SubDomain
    subDomains = new Array(domains.length);

    //get all subDomain
    questions.map((question) => {
      domains.map((domain, index) => {
        const subdomainAnchor = { href: '#' + question.subDomain, title: question.subDomain };
        const found = subDomains[index].some((item) => item.title === subdomainAnchor.title);
        if (question.domain === domain && !found) {
          subDomains[index].push(subdomainAnchor);
        }
        return domain;
      });
      return question;
    });

    for (let i = 0; i < domains.length; i++) {
      subDomains[i] = Array.from(subDomains[i]);
    }
    this.setState({
      domains: domains,
      subDomains: subDomains,
      subActivekey: subDomains[0][0],
      mainActivekey: domains[0],
    });
  }

  /**
   * Add property "key" for questions and Sort question base on key.
   * Key is number of question and for navigate
   * @param questions
   */
  _addKey(questions) {
    for (let i = 0; i < questions.length; i++) {
      questions[i].key = i + 1;
    }
    return questions;
  }

  /**
   * Get questions for each SubDetailAssessment, where "domain and subDomain" compare Tab's indexMain and indexSub
   * @param indexMain
   * @param indexSub
   */
  _questionsForEachTab(indexMain, indexSub) {
    const { questions } = this.state;
    let arrQuestion = [];
    for (let i = 0; i < questions.length; i++) {
      if (questions[i].domain === indexMain && questions[i].subDomain === indexSub) {
        arrQuestion.push(questions[i]);
      }
    }
    return arrQuestion;
  }

  /**
   * handle navigate click in Tab left parents
   * @param activeKey is default param,string name ( ant-design )
   */
  _domainClick = (activeKey) => {
    const { subDomains, domains } = this.state;
    this.setState({
      mainActivekey: activeKey,
      subActivekey: subDomains[domains.findIndex((e) => e === activeKey)][0].title,
    });
  };

  /**
   * handle navigate click in Tab top children
   */
  _subDomainClick = (activeKey) => {
    this.setState({ subActivekey: activeKey });
  };
  navigateTo = (url) => {
    navigateTo(url);
  };

  _onRecalculateItem = (itemId) => {
    const { reportId } = this.state;
    const onOkClick = () => {
      this.subscriptions.push(
        reCalculate(itemId).subscribe(
          (res) => {
            if (res) {
              showSuccessToast('Update Report completed!');
              this.navigateTo('/assessments/reports/' + reportId);
            } else {
              showErrorToast('Update Report failed!');
            }
          },
          () => {
            showErrorToast('Update Report failed!');
          }
        )
      );
    };

    confirm({
      title: 'Are you sure update scores for this reports?',
      okText: 'View Report',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk: onOkClick,
    });
  };

  render() {
    const { mainActivekey, unanswered, domains, subDomains, assessmentId, assessmentName, reportId } = this.state;

    return (
      <div className="assessment-detail">
        <Row className="banner" align="middle" justify="space-between">
          <Col className="banner-title">
            {assessmentName && (
              <Breadcrumbs
                list={[
                  { title: 'Cloud Assessment', to: '/' },
                  { title: 'My Assessments', to: '/assessments' },
                  { title: assessmentName },
                ]}
              />
            )}
            <div className="title">{assessmentName}</div>
          </Col>
          <Col className="view-report">
            <Button className="view-report-button" onClick={() => this._onRecalculateItem(reportId)}>
              View reports
            </Button>
          </Col>
        </Row>

        <Row className="tabs" align="center" id="body-content">
          <Col className="tab-content">
            <Tabs activeKey={mainActivekey} tabPosition={'top'} onChange={this._domainClick} className="tab-detail">
              {domains.map((_domain, index) => {
                const tabName = index + 1 + '.' + _domain;
                return (
                  <TabPane tab={tabName} key={_domain}>
                    <Row className="tab-domain">
                      <Col xl={20} md={24} className="questions">
                        <div className="list-content" id="list-content">
                          {subDomains[index].map((_subDomain, i) => {
                            return (
                              <div id={_subDomain.title} key={i} className="sub-domain">
                                <div className="title-subdomain">{_subDomain.title}</div>
                                <SubDetailAssessment
                                  key={_subDomain.title}
                                  assessmentId={assessmentId}
                                  questions={this._questionsForEachTab(_domain, _subDomain.title)}
                                  callbackFromGrand={this.handleValueFromGrandchildren}
                                  unanswered={unanswered !== undefined ? unanswered : null}
                                />
                              </div>
                            );
                          })}
                        </div>
                      </Col>
                      <Col className="domain-anchor" md={0} xl={4}>
                        <div className="domain-anchor-detail">
                          <TableOfContent items={subDomains[index]} />
                        </div>
                      </Col>
                    </Row>
                  </TabPane>
                );
              })}
            </Tabs>
          </Col>
        </Row>
        <Row className="disclaimer">
          <div className="title">DISCLAIMER</div>
          <div className="content">
            This tool is designed to help organizations assess their progress with cloud adoption and identify gaps in
            organizational skills and processes. The recommendations provided are for informational purposes only, and
            based upon the information you provide and AWS’s experience in assisting organizations transition to the
            cloud. Your organization is responsible for making its own independent assessment of the recommendations and
            for any use of AWS’s products or services.
          </div>
        </Row>
      </div>
    );
  }
}
