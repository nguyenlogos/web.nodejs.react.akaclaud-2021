import emptyBox from '@assets/images/icons/empty-box-open.svg';
import { getCloudAccountByVendor, getRunningLab } from '@api';
import { LabDetailState } from '@constants';
import { Button, Select, Statistic, Image } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const { Option } = Select;
const { Countdown } = Statistic;

class LabAccount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accounts: [],
      selectedAccountId: null,
      labStarting: false,
      runningLab: null,
      status: 0,
      provisonResources: null,
    };

    this.subscriptions = [];
  }

  componentDidMount() {
    const { user } = this.props;
    const { roles } = user;
    if (roles) {
      this.getCloudAccount();
      this.getRunningLab();
    }
  }

  componentWillMount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      labStarting: nextProps.labStarting,
      status: nextProps.labStatus,
      provisonResources: nextProps.provisonResources,
    });

    if (nextProps.activeStack) {
      this.setState({ selectedAccountId: nextProps.activeStack.cloudUserId });
    }
  }

  getRunningLab = () => {
    this.subscriptions.push(
      getRunningLab().subscribe((stack) => {
        if (stack) {
          this.setState({ runningLab: stack });
        }
      })
    );
  };

  getCloudAccount() {
    const { user, labVendor, activeStack } = this.props;
    const { roles } = user;

    // has role 'trainee'
    if (roles && roles.indexOf('trainee') >= 0) {
      this.subscriptions.push(
        getCloudAccountByVendor(labVendor).subscribe((accounts) => {
          const activeAccount = activeStack ? accounts.find((a) => a.resourceId === activeStack.cloudUserId) : null;

          this.setState({
            accounts: accounts,
            selectedAccountId: activeAccount ? activeAccount._id : null,
          });
        })
      );
    }
  }

  onSelectedAccountChange = (value) => {
    this.setState({ selectedAccountId: value });
  };

  renderCloudAccounts = () => {
    const { accounts, selectedAccountId } = this.state;

    const options = accounts
      ? accounts.map((item, index) => {
        let title = '';
        for (const [key, value] of Object.entries(item)) {
          if (key !== '_id') {
            title += `${key}: ${value} \n`;
          }
        }

        return (
          <Option value={item._id} key={index} title={title}>
            {item.name ? item.name : item.resourceName}
          </Option>
        );
      })
      : [];

    const notFoundContent = (
      <div className="no-data gx-py-2">
        <Image className="no-data-image gx-my-2" src={emptyBox} />
        <p className="text-detail">
          No Cloud Account available.
        </p>
        <p>
          Click&nbsp;
          <Link to={{pathname: '/accounts/onboarding', state: {provider: this.props.labVendor}}} className="guideline">
            Account Onboarding
          </Link>
        </p>
        <p>
          before Starting Lab
        </p>
      </div>
    );

    return (
      <Select
        getPopupContainer={(trigger) => trigger.parentNode}
        notFoundContent={notFoundContent}
        placeholder="Select your account"
        className="select-cloud-account"
        onChange={this.onSelectedAccountChange}
        value={selectedAccountId}
      >
        {options}
      </Select>
    );
  };

  onStart = () => {
    const { selectedAccountId, accounts, runningLab } = this.state;
    const { onStartLabClick } = this.props;
    onStartLabClick(selectedAccountId, accounts, runningLab);
  };

  onFinish = () => {
    const { onFinishLabClick } = this.props;
    onFinishLabClick();
  };

  CountDown = () => {
    const { provisonResources } = this.state;
    const { startTime } = provisonResources;
    const { timeInMinutes } = this.props;
    const deadline = startTime.getTime() + 1000 * 60 * timeInMinutes;
    return (
      <div className="labdetail-end-lab">
        <Button type="danger" onClick={this.onFinish}>
          End lab
        </Button>
        <Countdown value={deadline} onFinish={this.onFinish} />
      </div>
    );
  };

  render() {
    const { labStarting, selectedAccountId, status, provisonResources } = this.state;
    const { isAlreadyLab } = this.props;

    const startLabDisable = labStarting || selectedAccountId == null || isAlreadyLab;

    return (
      <>
        <div className="labdetail-cloud-accounts">
          {this.renderCloudAccounts()}
        </div>
        {status === LabDetailState.Ready && (
          <div className="labdetail-start-lab">
            <Button
              type="primary"
              loading={labStarting}
              onClick={this.onStart}
              disabled={startLabDisable}
              title={isAlreadyLab ? 'This lab is comming soon' : null}
            >
              Start Lab
            </Button>
          </div>
        )}
        {status === LabDetailState.Creating && (
          <div className="labdetail-creating-lab">
            <Button type="primary" disabled={true} loading={true}>
              Show Resources
            </Button>
          </div>
        )}

        {status === LabDetailState.Failed && (
          <div className="labdetail-fail-lab">
            <Button type="primary" onClick={this.onFinish}>
              Deployment Failed
            </Button>
          </div>
        )}

        {status === LabDetailState.Running && provisonResources && this.CountDown()}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(LabAccount);
