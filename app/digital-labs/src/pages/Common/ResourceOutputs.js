import { CloudProvider } from '@constants';
import { Table, Typography } from 'antd';
import React from 'react';

const { Paragraph } = Typography;

const ResourceOutputcolumns = [
  {
    title: '#',
    dataIndex: 'index',
    key: 'index',
    width: 40,
    align: 'center',
    render: (text, record, index) => {
      return (
        <span>
          {index + 1}
        </span>);
    },
  },
  {
    title: 'Service',
    dataIndex: 'Description',
    key: 'Description',
  },
  {
    title: 'Name',
    dataIndex: 'OutputKey',
    key: 'OutputKey',
  },
  {
    title: 'Output',
    dataIndex: 'OutputValue',
    key: 'OutputValue',
    render: (text, record, index) => {
      return (
        <div>
          {/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/gi.test(text)
            ? (
              <a href={text} target="_blank" rel="noopener noreferrer" >
                Go to resource
              </a>
            ) : (
              <>
                {record.OutputValue}
              </>
            )}
        </div>
      );
    },
  },
];

const ResourceOutputs = ({ provisonResources, commonInfo, resourceOutputs }) => {
  const showresourceOutputs = resourceOutputs
    ? resourceOutputs.map((element, index) => {
      element['key'] = index;

      return element;
    })
    : [];

  const { portal } = provisonResources;
  let lauchPortalMsg;
  switch (commonInfo.vendor.toLowerCase()) {
    case 'azure':
      lauchPortalMsg = 'Azure portal';
      break;
    case 'gcp':
      lauchPortalMsg = 'GCP console';
      break;
    default:
      lauchPortalMsg = 'AWS console';
      break;
  }

  return (
    <div className="progess-resource">
      {portal && portal.signInUrl && (
        <div className="identity" id="identity">
          <h3 className="title">
            Identity information
          </h3>
          <p className="padding-p">
            Use following information to lauch to 
            {' '}
            {lauchPortalMsg}
            {' '}
            and practice the hand-on lab!
          </p>
          <a href={decodeURIComponent(portal.signInUrl)} target="_blank" rel="noopener noreferrer">
            {lauchPortalMsg}
          </a>
          {commonInfo.vendor === CloudProvider.Gcp && (
            <div className="secret">
              <Paragraph copyable={{ text: portal.loginId }}>
                Id login Gcp
              </Paragraph>
              <Paragraph copyable={{ text: portal.loginPw }}>
                Password login Gcp
              </Paragraph>
            </div>
          )}
        </div>
      )}
      <Table pagination={false} bordered={true} columns={ResourceOutputcolumns} dataSource={showresourceOutputs} />
    </div>
  );
};

export default ResourceOutputs;
