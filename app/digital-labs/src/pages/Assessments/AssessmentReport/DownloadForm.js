import { LIST_COUNTRIES } from '@constants';
import { Button, Checkbox, Col, Modal, Row, Select } from 'antd';
import classnames from 'classnames';
import React, { useState } from 'react';

const Option = Select.Option;
const initialValidation = {
  invalidFullName: false,
  invalidPhoneNumber: false,
  invalidEmail: false,
  invalidJobTitle: false,
  invalidCountry: false,
};

const DownloadForm = (props) => {
  const { show, onCancel, onSubmit, defaultCountry } = props;
  const initialForm = {
    fullName: '',
    phoneNumber: '',
    email: '',
    jobTitle: '',
    country: defaultCountry, // refs : countries.ts
    agreeTerm: false,
  };
  const [form, setForm] = useState({ ...initialForm });
  const [validation, setValidation] = useState({ ...initialValidation });
  const { agreeTerm, fullName, phoneNumber, email, jobTitle, country } = form;
  const validate = () => {
    if (agreeTerm) {
      setValidation({ ...initialValidation });
      if (!form.fullName) {
        setValidation({ invalidFullName: true });
      }
      if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g.test(form.phoneNumber)) {
        setValidation({ invalidPhoneNumber: true });
      }
      if (!form.email || !(form.email.includes('.') && form.email.includes('@'))) {
        setValidation({ invalidEmail: true });
      }
      if (!form.jobTitle) {
        setValidation({ invalidJobTitle: true });
      }
      if (!form.country) {
        setValidation({ invalidCountry: true });
      }

      if (
        !validation.invalidFullName &&
        !validation.invalidPhoneNumber &&
        !validation.invalidEmail &&
        !validation.invalidJobTitle &&
        !validation.invalidCountry
      ) {
        onSubmit(form);
      }
    } else {
      Modal.warning({
        title: 'Warning',
        content:
          "You must click an 'Agree to akaClaud Site Terms' checkbox to accept the Terms of Service and the Privacy Policy of akClaud before receiving the email.",
      });
    }
  };
  return (
    <Modal title="Get Your Full Report Now" onCancel={onCancel} visible={show} footer={null} width={400}>
      <Row className="input-send-email" justify="center">
        <Col span={20}>
          <Row className="email-form-input">
            <input
              className={classnames({ 'input-require': validation.invalidFullName })}
              value={fullName}
              placeholder="Full Name"
              onChange={(e) => setForm({ fullName: e.target.value })}
            />
          </Row>
          <Row className="email-form-input">
            <input
              className={classnames({ 'input-require': validation.invalidPhoneNumber })}
              value={phoneNumber}
              placeholder="Phone Number"
              onChange={(e) => setForm({ phoneNumber: e.target.value })}
            />
          </Row>
          <Row className="email-form-input">
            <input
              className={classnames({ 'input-require': validation.invalidEmail })}
              value={email}
              placeholder="Email"
              onChange={(e) => setForm({ email: e.target.value })}
            />
          </Row>
          <Row className="email-form-input">
            <input
              className={classnames({ 'input-require': validation.invalidJobTitle })}
              value={jobTitle}
              placeholder="Job Title"
              onChange={(e) => setForm((prevForm) => ({ ...prevForm, jobTitle: e.target.value }))}
            />
          </Row>
          <Row className="email-form-input">
            <Select
              className={classnames({ 'input-require': validation.invalidCountry })}
              placeholder="Country"
              value={country}
              onChange={(country) => setForm({ country })}
            >
              {LIST_COUNTRIES.map((country) => (
                <Option title={country.name} value={country.name} key={country.name}>
                  {country.name}
                </Option>
              ))}
            </Select>
          </Row>
          <Row className="email-form-input">
            <Button onClick={validate} type="primary" className="submit-btn">
              Get report
            </Button>
          </Row>
          <Row className="term email-form-input">
            <Checkbox checked={agreeTerm} onChange={(e) => setForm({ agreeTerm: e.target.checked })}>
              Agree to{' '}
              <a href="#download" target="_blank" rel="noopener noreferrer" >
                akaClauld Site Terms
              </a>
              <div className="logo-content-bottom">{/* <span>{sendMail}</span> */}</div>
            </Checkbox>
          </Row>
        </Col>
      </Row>
    </Modal>
  );
};
export default DownloadForm;
