import configureStore from '@appRedux/store';
import { getLoginUserInfo } from '@common/authorization';
import { initResumeFlow, enforceReload } from 'aka-keycloak-login';

import RenderApp from './NextApp';
import registerServiceWorker from './registerServiceWorker';

const main = () => {
  registerServiceWorker();

  const store = configureStore({
    user: getLoginUserInfo(),
  });

  RenderApp(store);
};

initResumeFlow()
  .then(() => {
    enforceReload();
    main();
  })
  .catch((err) => {
    console.error(err.response && err.response.status);
    if (err.response && parseInt(err.response.status) === 400) {
      localStorage.clear();
      window.location.reload();
    }
  });
