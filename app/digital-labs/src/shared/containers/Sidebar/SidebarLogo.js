import { AppstoreOutlined } from '@ant-design/icons';
import { onNavStyleChange, toggleCollapsedSideNav } from '@appRedux/actions/Setting';
import akaClaudLogo from '@assets/images/logo.svg';
import { LANDING_URL, REACT_APP_OPERATION_URL, REACT_APP_SECURITY_URL } from '@common';
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  TAB_SIZE,
  THEME_TYPE_LITE,
} from '@constants';
import { Dropdown, Menu } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

const SidebarLogo = () => {
  const dispatch = useDispatch();
  const { width, themeType } = useSelector(({ settings }) => settings);
  const { navCollapsed } = useSelector(({ common }) => common);
  let navStyle = useSelector(({ settings }) => settings.navStyle);
  if (width < TAB_SIZE && navStyle === NAV_STYLE_FIXED) {
    navStyle = NAV_STYLE_DRAWER;
  }

  const menu = (
    <Menu className="menu-navigation">
      <Menu.Item key="0">
        <a href={REACT_APP_OPERATION_URL} className="gx-site-logo app-nav-link">
          Cloud Operation
        </a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href={REACT_APP_SECURITY_URL} className="gx-site-logo app-nav-link">
          Security Assurance
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="gx-layout-sider-header">
      {navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR ? (
        <div className="gx-linebar">
          <i
            className={`gx-icon-btn icon icon-${navStyle === NAV_STYLE_MINI_SIDEBAR ? 'menu-unfold' : 'menu-fold'}`}
            onClick={() => {
              if (navStyle === NAV_STYLE_DRAWER) {
                dispatch(toggleCollapsedSideNav(!navCollapsed));
              } else if (navStyle === NAV_STYLE_FIXED) {
                dispatch(onNavStyleChange(NAV_STYLE_MINI_SIDEBAR));
              } else if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
                dispatch(toggleCollapsedSideNav(!navCollapsed));
              } else {
                dispatch(onNavStyleChange(NAV_STYLE_FIXED));
              }
            }}
          />
        </div>
      ) : null}

      <a href={LANDING_URL} target="_blank" className="gx-site-logo" rel="noopener noreferrer">
        {navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR && width >= TAB_SIZE ? (
          <img alt="" src={require('@assets/images/w-logo.png')} />
        ) : themeType === THEME_TYPE_LITE ? (
          <img alt="" src={require('@assets/images/logo-white.png')} />
        ) : (
          <img alt="" src={akaClaudLogo} />
        )}
      </a>

      <Dropdown overlay={menu} trigger={['click']}>
        <a href="#top" className="ant-dropdown-link apps-navigation" onClick={(e) => e.preventDefault()}>
          <AppstoreOutlined style={{ fontSize: '1rem' }} />
        </a>
      </Dropdown>
    </div>
  );
};

export default SidebarLogo;
