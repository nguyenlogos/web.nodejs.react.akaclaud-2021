export class Vendor {
  constructor() {
    this.id = '';
    this.vendorId = '';
    this.name = '';
    this.active = false;
    this.createdBy = '';
    this.updatedBy = '';
    this.createdTime = new Date();
    this.updatedTime = new Date();
  }

  fromJson(json) {
    if (!json) {
      return this;
    }

    this.id = json._id;
    this.vendorId = json.vendorId;
    this.name = json.name;
    this.active = json.active;
    this.createdBy = json.createdBy;
    this.updatedBy = json.updatedBy;
    this.updatedBy = json.updatedBy;
    this.createdTime = json.createdTime ? new Date(json.createdTime) : null;
    this.updatedTime = json.updatedTime ? new Date(json.updatedTime) : null;

    return this;
  }
}
