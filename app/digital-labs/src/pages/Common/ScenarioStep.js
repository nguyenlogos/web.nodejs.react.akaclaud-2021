import { getResourceUrl } from '@api';
import { Rate } from 'antd';
import React from 'react';

const ScenarioStep = ({ step, categories }) => (
  <div className="step">
    <img src={getResourceUrl(step.code, step.image)} alt="" className="step-image" />
    <div>
      <div className="step-name">
        <h3>
          {step.labName}
        </h3>
        {step.status === 6 && (
          <span className="completed">
            Completed
          </span>)}
      </div>
      <p className="step-description">
        {step.description}
      </p>
      <div className="step-meta">
        <div className="meta-rating">
          <Rate disabled={true} allowHalf={true} defaultValue={2.5} />
        </div>
        <div className="meta-text">
          {step.timeInMinutes}
          {' '}
          minutes
        </div>
        {step.childCategoryId &&
          step.childCategoryId.map((cat) => {
            const category = categories.find((x) => x.categoryId === cat);
            return (
              category && (
                <div className="meta-text" key={cat}>
                  {category.name}
                </div>
              )
            );
          })}
        <div className="meta-text">
          {step.vendor}
        </div>
        <div className="meta-text">
          {step.credit > 0 ? `${step.credit} credits` : 'Free'}
        </div>
      </div>
    </div>
  </div>
);

export default ScenarioStep;
