import { getFilteredMenu } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Col, Row } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';

const BreadcumbLab = ({ title }) => {
  const { roles } = useSelector(({ user }) => user);

  // get url from first item of Contribution
  var rootUrl = '/';
  const menuContribution = getFilteredMenu(roles).filter((item) => item.id === 'Contribute');
  if (menuContribution.length !== 0) {
    if (menuContribution[0].children.length !== 0) {
      rootUrl = menuContribution[0].children[0].url;
    }
  }

  return (
    <Row className="contribute-lab_banner" align="middle" justify="space-between">
      <Col className="contribute-lab_banner-title">
        <Breadcrumbs list={[{ title: 'My Contribution', to: rootUrl }, { title: `${title}` }]} />
        <label className="title">
          {title}
        </label>
      </Col>
    </Row>
  );
};

export default BreadcumbLab;
