import { AUTHORIZATION_CODE, HOOK_AUTHORIZATION_TYPE } from '@constants';
import { debounceRootDispatch, getRootState as getImmutableRootStore } from 'octopus-immutable-store';

export function dispatchAuthorization(status, code = AUTHORIZATION_CODE) {
  // debounce dispatch with 2000ms
  // if want customize debounce time -> add 3rd parameters
  debounceRootDispatch(HOOK_AUTHORIZATION_TYPE, { status, code });
}

export function getRootState() {
  return getImmutableRootStore();
}
