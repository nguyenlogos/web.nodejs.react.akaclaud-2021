import { activeLab, checkStackRunning, deactiveLab, getAllLabs } from '@api';
import { PUBLIC_URL } from '@common';
import { getFilteredMenu, openNotification, truncateText, cleanHTMLTag } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { LabState } from '@constants';
import  {Tooltip} from 'antd';
import { Button, Card, Checkbox, Col, Divider, Input, Modal, Row, Table, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { map } from 'rxjs/operators';

const { confirm } = Modal;

const MyContributesLab = () => {
  const initPage = 0;
  const initPageSize = 20;
  const subscriptions = [];
  const { roles } = useSelector(({ user }) => user);

  // get url from first item of Contribution
  var rootUrl = '/';
  const menuContribution = getFilteredMenu(roles).filter((item) => item.id === 'Contribute');
  if (menuContribution.length !== 0) {
    if (menuContribution[0].children.length !== 0) {
      rootUrl = menuContribution[0].children[0].url;
    }
  }

  const [myLabInfo, setMyLabInfo] = useState({
    labs: [],
    count: 0,
    page: 0,
    pageSize: 10,
  });
  const [keyword, setKeyword] = useState('');
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const [filters, setFilters] = useState({});

  const fnEffectHook = () => {
    loadAllLabs(initPage, initPageSize);
    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const loadAllLabs = (page, pageSize, filters) => {
    setLoading(true);

    subscriptions.push(
      getAllLabs(page, pageSize, filters, keyword)
        .pipe(
          map((docs) => {
            const { count, page, size: pageSize } = docs;

            const labs = docs.dataSources
              ? docs.dataSources.map((lab, index) => {
                lab['key'] = index;

                return lab;
              })
              : [];
            return {
              count,
              labs,
              page,
              pageSize,
            };
          })
        )
        .subscribe(
          ({ count, labs, page, pageSize }) => {
            setMyLabInfo({
              labs: labs,
              count: count,
              page: page,
              pageSize: pageSize,
            });
          },
          (err) => {
            openNotification('error', 'Occurred error when load list lab from server!');
          },
          () => {
            setLoading(false);
          }
        )
    );
  };

  const onClickSearch = () => {
    const { pageSize } = myLabInfo;
    loadAllLabs(0, pageSize);
  };

  const keypressSearchAssessments = (e) => {
    const { pageSize } = myLabInfo;
    if (e.key === 'Enter') {
      loadAllLabs(0, pageSize);
    }
  };

  const searchAssessments = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    setKeyword(keyword);
  };

  const onClickCreate = () => {
    history.replace(`${PUBLIC_URL}/portals/contributes/labs/create`);
  };

  const onPageChange = (page, pageSize) => {
    const newPage = page > 0 ? page - 1 : 0;
    loadAllLabs(newPage, pageSize, filters);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadAllLabs(0, pageSize, filters);
  };

  const buildMyContributeLabTable = () => {
    const { labs, count, pageSize, page } = myLabInfo;
    const from = page * pageSize + 1;
    const pagination = {
      total: count,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        key: 'index',
        width: '5%',
        render: (text, record, index) => {
          return from + index;
        },
      },
      {
        title: 'Lab name',
        key: 'labName',
        dataIndex: 'labName',
        width: '20%',
        render: (labName) => {
          return truncateText(labName, 100, true);
        },
      },
      {
        title: 'Short Description',
        key: 'description',
        dataIndex: 'description',
        width: '20%',
        render: (description) => {
          return (
            <Tooltip placement='topLeft' overlayClassName='small-tooltip' title={cleanHTMLTag(description)} >
              {truncateText(description, 20, true)}
            </Tooltip>
          );
        },
      },
      {
        title: 'Launch Time',
        key: 'timeInMinutes',
        dataIndex: 'timeInMinutes',
        width: '10%',
      },
      {
        title: 'Created By',
        key: 'createdBy',
        dataIndex: 'createdBy',
        width: '15%',
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
        width: '10%',
        render: (status) => {
          if (status === 'ready') {
            return (
              <Tag color="green" style={{ width: 70, textAlign: 'center' }}>
                Ready
              </Tag>
            );
          }
          return (
            <Tag color="orange" style={{ width: 70, textAlign: 'center' }}>
              Pending
            </Tag>
          );
        },
      },
      {
        title: 'Order',
        dataIndex: 'rating',
        key: 'rating',
        width: '7%',
      },
      {
        title: 'Action',
        key: 'active',
        width: '10%',
        render: (text, record) => (
          <span>
            {record.status === LabState.ready && (
              <span className="gx-link" onClick={() => changeActive(record)}>
                {record.active ? 'Deactive' : 'Active'}
              </span>
            )}
            {record.status === LabState.pending && (
              <span className="gx-link" onClick={() => lauchEditLab(record._id)}>
                Edit
              </span>
            )}
            {record.status === LabState.ready && record.active === false && (
              <>
                <Divider type="vertical" />
                <span className="gx-link" onClick={() => lauchEditLab(record._id)}>
                  Edit
                </span>
              </>
            )}
          </span>
        ),
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={labs}
        columns={columns}
        pagination={pagination}
        loading={loading}
        scroll={{ y: '100%' }}
      />
    );
  };

  const changeActive = (record) => {
    const { page, pageSize } = myLabInfo;
    subscriptions.push(
      checkStackRunning(record._id).subscribe((stack) => {
        if (Boolean(stack.active) === true) {
          return openNotification('error', 'This lab is running');
        } else {
          confirm({
            title: record.active ? 'Deactive' : 'Active',
            content: `Are you sure ${record.active ? 'deactive' : 'active'} this lab?`,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
              if (record.active) {
                subscriptions.push(
                  deactiveLab(record._id).subscribe(
                    () => {
                      openNotification('success', 'Change status success');
                      loadAllLabs(page, pageSize);
                    },
                    () => {
                      openNotification('error', 'Error occured');
                    }
                  )
                );
              } else {
                subscriptions.push(
                  activeLab(record._id).subscribe(
                    () => {
                      openNotification('success', 'Change status success');
                      loadAllLabs(page, pageSize);
                    },
                    () => {
                      openNotification('error', 'Error occured');
                    }
                  )
                );
              }
            },
          });
        }
      })
    );
  };

  const lauchEditLab = (recordId) => {
    history.replace(`${PUBLIC_URL}/portals/contributes/labs/edit/` + recordId, 'Lab');
  };

  const onChange = (e) => {
    const { pageSize } = myLabInfo;
    if (e.length === 0 || e.length === 2) {
      setFilters({});
      loadAllLabs(0, pageSize);
    }
    if (e.length === 1 && e[0] === 'pending') {
      setFilters({ status: 'pending' });
      loadAllLabs(0, pageSize, { status: 'pending' });
    }
    if (e.length === 1 && e[0] === 'ready') {
      setFilters({ status: 'ready' });
      loadAllLabs(0, pageSize, { status: 'ready' });
    }
  };

  const options = [
    { label: 'Pending Labs', value: 'pending' },
    { label: 'Ready Labs', value: 'ready' },
  ];

  return (
    <div className="my-contribute-labs">
      <Row align="middle" className="my-contribute-labs-header">
        <Col span={24} className="header-title-container">
          <Breadcrumbs list={[{ title: 'My Contribution', to: rootUrl }, { title: 'My Cloud Digital Lab' }]} />
          <div className="title">
            <span>
              My Cloud Digital Lab
            </span>
          </div>
        </Col>
      </Row>

      <Card className="gx-card" title="Labs Status">
        <div style={{ justifyContent: 'flex-start', display: 'flex' }}>
          <div style={{ marginRight: 60 }}>
            There are
            {' '}
            <Tag color="blue" style={{ width: 50, textAlign: 'center' }}>
              {myLabInfo.count}
            </Tag>
            labs in list
          </div>
          <Checkbox.Group options={options} onChange={onChange} />
        </div>
      </Card>

      <Card className="gx-card">
        <Row style={{ justifyContent: 'space-between', display: 'flex', margin: 0 - 0.3 }}>
          <Col>
            <Button type="primary" onClick={onClickCreate} className="my-contribute-labs-create">
              Add new lab
            </Button>
          </Col>
          <Col>
            <div style={{ position: 'relative' }}>
              <Input
                type="text"
                onKeyPress={keypressSearchAssessments}
                onChange={searchAssessments}
                placeholder="Type keyword to type"
                style={{ paddingLeft: 30 }}
              />
              <div style={{ position: 'absolute', cursor: 'pointer', top: 12, left: 12 }} onClick={onClickSearch}>
                <i className="icon icon-search-new" />
              </div>
            </div>
          </Col>
        </Row>
        <Divider />
        {buildMyContributeLabTable()}
      </Card>
    </div>
  );
};

export default MyContributesLab;
