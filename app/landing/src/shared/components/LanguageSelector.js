import React from "react";
import { Menu, Dropdown } from "antd";
import { DownOutlined } from '@ant-design/icons';
import { switchLanguage } from "@appRedux/actions";
import "@styles/language-selector.less";

const LanguageMenu = ({ languages }) => (
    <Menu className="language-selector-menu">
        {languages.map(language => (
            <Menu.Item key={language.id} onClick={() => {
              // TODO: check switch language action
              switchLanguage(language.id)
            }}>
                {language.text}
            </Menu.Item>))}
    </Menu>
);

const LanguageSelector = ({ languages, currentLanguage }) => (
  <Dropdown className="language-selector" overlay={<LanguageMenu languages={languages} />} trigger={['click']}>
    <div className="ant-dropdown-link">
      {currentLanguage.text} <DownOutlined />
    </div>
  </Dropdown>
);
export default LanguageSelector;