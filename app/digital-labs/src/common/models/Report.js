export class Report {
  constructor() {
    this.id = '';
    this.assessmentId = '';
    this.assessmentName = '';
    this.report = [];
    this.createdBy = '';
    this.updatedTime = new Date();
  }

  fromJson(json) {
    if (!json) {
      return this;
    }

    this.id = json._id;
    this.assessmentId = json.assessmentId;
    this.assessmentName = json.assessmentName;
    this.report = json.report;
    this.createdBy = json.createdBy;
    this.updatedTime = json.updatedTime ? new Date(json.updatedTime) : null;

    return this;
  }
}
