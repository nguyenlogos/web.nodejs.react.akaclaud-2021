import asyncComponent from '@components/AsyncComponent';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const TraningLabsApp = ({ prefixRoute }) => {
  return (
    <div className="gx-page-layout training-labs">
      <Switch>
        <Route path={`${prefixRoute}/`} exact={true} component={asyncComponent(() => import('./Catalog'))} />
        <Route path={`${prefixRoute}/:id`} exact={true} component={asyncComponent(() => import('./Detail'))} />
        <Route path={`${prefixRoute}/scenario/:id`} component={asyncComponent(() => import('./Scenario'))} />
      </Switch>
    </div>
  );
};

export default TraningLabsApp;
