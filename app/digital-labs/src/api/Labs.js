import { Category, CategoryStats, Level, Vendor } from '@common/models';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthHttp, Http } from './client';

/**
 * GET: /api/v1/labs?_page&_limit
 * @param {number} page page index
 * @param {number} pageSize page size
 * @param {object} filter filter object
 */
export function getAllLabs(page, pageSize, filter, keyword) {
  let url = `labs?_page=${page}&_limit=${pageSize}`;

  if (keyword && keyword.length > 0) {
    url = `labs?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${pageSize}`;
  } else {
    url = `labs?_page=${page}&_limit=${pageSize}`;
  }
  if (filter) {
    const { childCategoryId, vendor, complexityId, status } = filter;

    if (childCategoryId) {
      url += '&childCategoryId=' + childCategoryId;
    }

    if (vendor) {
      url += '&vendor=' + vendor;
    }

    if (complexityId) {
      url += '&complexityId=' + complexityId;
    }

    if (status) {
      url += '&status=' + status;
    }
  }

  return from(AuthHttp.get(url));
}

export function getAllReadyTrainingLab(keyword, page, pageSize, filter) {
  let url = '';
  if (keyword && keyword.length > 0) {
    url = `lab-portal/labs/training/ready/all?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${pageSize}`;
  } else {
    url = `lab-portal/labs/training/ready/all?_page=${page}&_limit=${pageSize}`;
  }

  if (filter) {
    const { childCategoryId, vendor, complexityId } = filter;

    if (childCategoryId) {
      url += '&childCategoryId=' + childCategoryId;
    }

    if (vendor) {
      url += '&vendor=' + vendor;
    }

    if (complexityId) {
      url += '&complexityId=' + complexityId;
    }
  }
  return from(Http.get(url));
}

export function getAllReadyBusinessCaseLab(keyword, page, pageSize, filter) {
  let url = '';
  if (keyword && keyword.length > 0) {
    url = `lab-portal/labs/business-case/ready/all?_keyword=${encodeURIComponent(
      keyword
    )}&_page=${page}&_limit=${pageSize}`;
  } else {
    url = `lab-portal/labs/business-case/ready/all?_page=${page}&_limit=${pageSize}`;
  }

  if (filter) {
    const { childCategoryId, vendor, complexityId } = filter;

    if (childCategoryId) {
      url += '&childCategoryId=' + childCategoryId;
    }

    if (vendor) {
      url += '&vendor=' + vendor;
    }

    if (complexityId) {
      url += '&complexityId=' + complexityId;
    }
  }
  return from(Http.get(url));
}

// GET: /api/v1/labs/scenarios/
export function getAllScenarios(page, pageSize) {
  let url = `scenario/all?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

// GET : /api/v1/labs/scenarios/:id
export function getScenarioContent(id) {
  let url = `scenario/${id}`;
  return from(AuthHttp.get(url));
}

// POST : /api/v1/labs/scenarios/
export function insertScenario(data) {
  const url = 'scenario';
  return from(AuthHttp.post(url, data));
}

// PUT : /api/v1/labs/scenarios/:id
export function updateScenario(id, data) {
  const url = `scenario/${id}`;
  return from(AuthHttp.put(url, data));
}

// PUT /api/v1/labs/validate/:id
export function validateCloudformation(id) {
  let url = `labs/validate/${id}`;
  return from(AuthHttp.put(url, ''));
}

// POST: /api/v1/labs/
export function insertLab(data) {
  let url = 'labs';
  return from(AuthHttp.post(url, data));
}

// DELETE: /api/v1/labs/:id
export function deleteLab(id) {
  let url = `labs/${id}`;
  return from(AuthHttp.delete(url));
}

// PUT: /api/v1/labs/:id
export function updateLab(id, data) {
  let url = `labs/${id}`;
  return from(AuthHttp.put(url, data));
}

// PUT: /api/v1/labs/active/:id
export function activeLab(id) {
  let url = `labs/active/${id}`;
  return from(AuthHttp.put(url, null));
}

// PUT: /api/v1/labs/deactive/:id
export function deactiveLab(id) {
  let url = `labs/deactive/${id}`;
  return from(AuthHttp.put(url, null));
}

// PUT : /api/v1/labs/approve/:id
export function approveLab(id, notes, cost) {
  let url = `labs/approve/${id}`;
  return from(AuthHttp.put(url, { notes, cost }));
}

// PUT : /api/v1/labs/reject/:id
export function rejectLab(id, notes) {
  let url = `labs/reject/${id}`;
  return from(AuthHttp.put(url, { notes }));
}

// GET lab/:labId/stacks
export function checkStacksUser(labId) {
  let url = `labs/${labId}/stacks`;
  return from(AuthHttp.get(url));
}

export function createStack(labId, selectedAccount) {
  let url = `labs/${labId}/stacks`;
  return from(AuthHttp.put(url, { selectedAccount }));
}

export function describeStack(stackName) {
  let url = `labs/stacks/${stackName}`;
  return from(AuthHttp.get(url));
}

export function describeStackResources(stackName) {
  let url = `labs/stacks/${stackName}/resources`;
  return from(AuthHttp.get(url));
}

export function generateSignURL(stackName) {
  let url = `labs/stacks/${stackName}/url`;
  return from(AuthHttp.get(url));
}

export function generateURL(stackInfo) {
  let url = 'aws/user';
  return from(AuthHttp.post(url, stackInfo));
}

export function cleanResource(stackName, stack) {
  let url = `labs/stacks/${stackName}`;
  return from(AuthHttp.delete(url, stack));
}

export function storeDocuments(formData, prefix) {
  let url = `resources/documents/${prefix}`;
  return from(AuthHttp.post(url, formData));
}

export function storeFileToS3(formData) {
  let url = 'resources/media';
  return from(AuthHttp.post(url, formData));
}

export function updateFileS3(formData) {
  let url = 'resources/media';
  return from(AuthHttp.put(url, formData));
}

export function getContentObjectURLS3(key) {
  let url = `resources/stream?key=${key}`;
  return from(AuthHttp.get(url));
}

export function deleteObject(key) {
  let url = `aws/${key}`;
  return from(AuthHttp.delete(url));
}

export function getlabProvision(stackName) {
  let url = `aws/stack/${stackName}`;
  return from(AuthHttp.get(url));
}

// GET: /api/v1/assets?_page&_limit
export function getAllLabsCostContributor(page, pageSize) {
  let url = `labs/cost/contributor?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

// GET: /api/v1/assets?_page&_limit
export function getAllLabsCostTrainee(page, pageSize) {
  let url = `labs/cost/trainee?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

export function getCategories() {
  let url = 'lab-portal/categories';
  return from(Http.get(url)).pipe(
    map((res) => {
      return res ? res.map((d) => new Category().fromJson(d)) : [];
    })
  );
}

export function getVendors() {
  let url = 'lab-portal/vendors';
  return from(Http.get(url)).pipe(
    map((res) => {
      return res ? res.map((d) => new Vendor().fromJson(d)) : [];
    })
  );
}

export function getLevels() {
  let url = 'lab-portal/levels';
  return from(Http.get(url)).pipe(
    map((res) => {
      return res ? res.map((d) => new Level().fromJson(d)) : [];
    })
  );
}

export function getCategoryStats(categoryId) {
  let url = 'lab-portal/categories/stats';
  if (categoryId) {
    url += `?categoryId=${categoryId}`;
  }

  return from(Http.get(url)).pipe(
    map((res) => {
      return res ? res.map((d) => new CategoryStats().fromJson(d)) : [];
    })
  );
}

export function getListLevel() {
  let url = 'lab-portal/complexities';
  return from(Http.get(url));
}

export function getLab(id) {
  let url = `lab-portal/labs/${id}`;
  return from(Http.get(url));
}

export function getRunningLab() {
  let url = 'labs/stack';
  return from(AuthHttp.get(url));
}

export function getAllHistorystacks(keyword, page, pageSize) {
  let url = `labs/stacks?_page=${page}&_limit=${pageSize}`;
  if (keyword && keyword.length > 0) {
    url = `labs/stacks?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${pageSize}`;
    return from(AuthHttp.get(url));
  }
  return from(AuthHttp.get(url));
}

export function checkStackRunning(id) {
  let url = `labs/stack/${id}`;
  return from(AuthHttp.get(url));
}
