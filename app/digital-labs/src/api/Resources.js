import { PUBLIC_API_URL, PUBLIC_DOCUMENT_URL } from '@common';
import axios from 'axios';
import { from, of } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';

import { AuthHttp, Http } from './client';

export function getDocumentText(prefix, fileName) {
  if (fileName == null) {
    return of(null);
  }

  let url = `resources/text/${prefix}/${fileName}`;
  return from(Http.get(url));
}

export function getContentGit(prefix, fileName) {
  if (fileName && fileName.length > 0) {
    const gitLabUrl = getResourceUrl(prefix, fileName);
    return from(axios.get(gitLabUrl)).pipe(map((result) => result.data));
  }
  return of(null);
}

export function getContentS3(prefix, fileName) {
  let s3Url = `labs/getContentFromS3?prefix=${prefix}&fileName=${fileName}`;
  if (fileName && fileName.length > 0) {
    return from(AuthHttp.get(s3Url)).pipe(
      concatMap(({ url }) => {
        return axios.get(url);
      }),
      map((result) => result.data)
    );
  }
  return of(null);
}

export function getPublicDoc(prefix, fileName) {
  if (fileName && fileName.length > 0) {
    return from(axios.get(`${PUBLIC_API_URL}/resources/docs/${prefix}/${fileName}`)).pipe(map((result) => result.data));
  }

  return of(null);
}

export function getPublicResourceUrl(prefix, fileName) {
  return `${PUBLIC_API_URL}/resources/docs/${prefix}/${fileName}`;
}

export function getResourceUrl(prefix, fileName) {
  return `${PUBLIC_DOCUMENT_URL}/-/raw/${prefix}/${prefix}/${fileName}`;
}

export function uploadImage(file, upoadProgressFunc) {
  let url = 'resources/media';
  const data = new FormData();
  data.append('file', file);
  const config = {
    onUploadProgress: upoadProgressFunc,
  };

  return from(AuthHttp.post(url, data, config));
}
