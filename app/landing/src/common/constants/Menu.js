// import React from 'react';
import {
  BellFilled,
  CloudFilled,
  DollarCircleFilled,
  MessageFilled,
} from '@ant-design/icons';

export const ICON_NAVIGATION = [];
// export const ICON_NAVIGATION = [
//   {
//     id: "notifications",
//     text: "Notifications",
//     url: process.env.REACT_APP_DIGITAL_LABS+"/accounts/my-notification",
//     icon: <BellFilled style={{ fontSize: '0.9em' }} />,
//     acceptRoles: "trainee",
//   },
//   {
//     id: "chat",
//     text: "Messages",
//     url: process.env.REACT_APP_DIGITAL_LABS+"/accounts/my-chat",
//     icon: <MessageFilled style={{ fontSize: '0.9em' }} />,
//     acceptRoles: "trainee",
//   },
//   {
//     id: "credits",
//     text: "My Credits",
//     url: process.env.REACT_APP_DIGITAL_LABS+"/accounts/my-credit",
//     icon: <DollarCircleFilled style={{ fontSize: '0.9em' }} />,
//     acceptRoles: "trainee",
//   },
//   {
//     id: "accounts",
//     text: "My Cloud Accounts",
//     url: process.env.REACT_APP_DIGITAL_LABS+"/accounts",
//     icon: <CloudFilled style={{ fontSize: '0.9em' }} />,
//     acceptRoles: "trainee",
//   },
// ];