import aws from "@assets/images/icons/AWS.svg";
import azure from "@assets/images/icons/Azure.svg";
import cis from "@assets/images/icons/CIS.png";
import GCP from "@assets/images/icons/GCP.svg";
import HIPAA from "@assets/images/icons/HIPAA.png";
import iso from "@assets/images/icons/iso.png";
import PCI from "@assets/images/icons/PCI.png";
import React, { useState } from "react";
import { Menu, Row, Col, Carousel, Tabs, Image } from "antd";
import { LANDING_TABS, IMAGE_ERROR } from "../../common/constants/Landing";
import { CloudDownloadOutlined } from "@ant-design/icons";
import DownloadPdf from './DownloadPdf';

const { TabPane } = Tabs;

const Services = () => {
    const [tab, setTab] = useState(LANDING_TABS[0]);
    return (
        <Row className="services gx-px-md-3" justify="center">
            <Col xs={24} lg={20}>
                <div className="services-title">
                    <h2>Our Services</h2>
                    <p>Cloud Strategy &amp; Roadmap (following FPT Digital Kaizen&trade;)</p>
                </div>
                <Row>
                    <Col xs={24} xl={0}>
                        <Tabs defaultActiveKey={[tab.id]} activeKey={tab.id} onChange={(key) => {
                            const currentTab = LANDING_TABS.find(item => item.id === key);
                            if (currentTab) {
                                setTab(currentTab);
                            }
                        }}>
                            {LANDING_TABS.map(item => (
                                <TabPane
                                    key={item.id}
                                    tab={item.text}
                                    onClick={() => setTab(item)}
                                />
                            ))}
                        </Tabs>
                    </Col>
                </Row>
                <Row style={{ minHeight: "400px" }}>
                    <Col xs={0} xl={4}>
                        <Menu mode="inline" defaultSelectedKeys={[tab.id]} selectedKeys={[tab.id]}>
                            {LANDING_TABS.map(item => (
                                <Menu.Item
                                    key={item.id}
                                    onClick={() => setTab(item)}
                                    className="services-tab"
                                >
                                    <p>{item.text}</p>
                                </Menu.Item>
                            ))}
                        </Menu>
                    </Col>
                    <Col xs={24} md={10} xl={8} className="carousel-container">
                        <Carousel autsoplay>
                            {tab.slides.map(slide => (
                                <div key={slide.id} className="services-slide">
                                    <img className="service-slide-image" src={slide.image} alt={slide.title} />
                                    <div className="service-slide-text">
                                        <h6>{slide.title}</h6>
                                        <p>{slide.description}</p>
                                    </div>
                                </div>
                            ))}
                        </Carousel>
                    </Col>
                    <Col xs={24} sm={24} md={14} xl={12}>
                        <Row gutter={[20, 16]} className="services-container">
                            {tab.items.map(item => (
                                <Col key={item.id} xs={12} sm={12} md={8}>
                                    <div className="services-item">
                                        <img className="services-item-icon" src={item.icon} alt={item.title} />
                                        <h3>{item.title}</h3>
                                        <p>{item.description}</p>
                                    </div>
                                </Col>
                            ))}
                        </Row>
                    </Col>
                </Row>

                <DownloadPdf
                    title="OUR SERVICES OFFERING"
                    subTitle="Full our cloud services & solutions for you"
                    fileName="FPT-CloudProfessionalService-101-v1.1.1.pdf"
                    btnText="Offering"
                />
            </Col>
            <Col md={22} lg={18} className="partners web-layout">
                <div>
                    <img src="/landing/Partner.png " alt="Our Partners" />
                    <Row justify="space-between">
                        <Col span="11">
                            <div className="partners-section">
                                <hr />
                                <span>Cloud Platform</span>
                            </div>
                        </Col>
                        <Col span="12">
                            <div className="partners-section">
                                <hr />
                                <span>Security Regulation</span>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Col>
            <Row className="partners mobile-layout gx-w-100">
                <Col span={24} className="partners-icon">
                    <Image className="aws-logo"
                        src={aws}
                        fallback={IMAGE_ERROR}
                    />
                    <Image className="azure-logo"
                        src={azure}
                        fallback={IMAGE_ERROR}
                    />
                    <Image className="gcp-logo"
                        src={GCP}
                        fallback={IMAGE_ERROR}
                    />
                </Col>
                <Col span={24}>
                    <div className="partners-section">
                        <hr />
                        <span>Cloud Platform</span>
                    </div>
                </Col>
                <Col span={24} className="partners-icon">
                    <Image className="cis-logo"
                        src={cis}
                        fallback={IMAGE_ERROR}
                    />
                    <Image className="hipaa-logo"
                        src={HIPAA}
                        fallback={IMAGE_ERROR}
                    />
                    <Image className="iso-logo"
                        src={iso}
                        fallback={IMAGE_ERROR}
                    />
                    <Image className="pci-logo"
                        src={PCI}
                        fallback={IMAGE_ERROR}
                    />
                </Col>
                <Col span={24}>
                    <div className="partners-section">
                        <hr />
                        <span>Security Regulation</span>
                    </div>
                </Col>
            </Row>
        </Row>
    );
};
export default Services;