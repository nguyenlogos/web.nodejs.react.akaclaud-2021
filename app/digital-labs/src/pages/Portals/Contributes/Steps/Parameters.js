import { RenderInput } from '@common/constants';
import { Col, Row } from 'antd';
import React, { Component } from 'react';

import CloudResource from './CloudResources';

class Parameters extends Component {
  constructor(props) {
    super(props);

    this.state = props.defaultValue
      ? { ...props.defaultValue }
      : {
        resourceParameter: '',
      };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.validationInput) {
      const { updateOutputValidation } = this.props;
      const outputValidation = this.validateInput();
      updateOutputValidation(outputValidation);
    }

    if (nextProps.defaultValue) {
      this.setState({
        resourceParameter: nextProps.defaultValue.resourceParameter,
      });
    }
  }

  validateInput = () => {
    return this.state.resourceParameter ? true : false;
  };

  clearInput = () => {
    this.setState({
      resourceParameter: '',
    });
  };

  onParameters = (text) => {
    this.setState(
      {
        resourceParameter: text,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { resourceParameter } = this.state;
    return (
      <div className="lab-step-parameters">
        <Row>
          <Col span={24}>
            <CloudResource
              renderInput={RenderInput.RESOURCE_PARAMETER}
              resourceParameter={resourceParameter}
              onParameterChange={this.onParameters}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Parameters;
