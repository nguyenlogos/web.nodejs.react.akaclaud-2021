import { QuestionType } from '@common/constants';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthHttp, Http } from './client';

// GET: /api/v1/assessments?_page&_limit
export function getAllAssessment(page, pageSize) {
  let url = `assessments?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

// GET: /api/v1/assessments/:id
export function getAssessment(id) {
  let url = `assessments/${id}`;
  return from(AuthHttp.get(url));
}

// GET: /api/v1/assessments/:id/report
export function getAssessmentReport(id) {
  let url = `assessments/${id}/report`;
  return from(AuthHttp.get(url));
}

// DELETE: /api/v1/assessments/:id
export function deleteAssessment(id) {
  let url = `assessments/${id}`;
  return from(AuthHttp.delete(url));
}

// GET: /api/v1/assessments/question/:id
export function getAllQuestionInAssessment(id) {
  let url = `assessments/${id}/questions`;
  return from(AuthHttp.get(url)).pipe(
    map((questions) => {
      return questions
        ? questions.map((q) => {
          const question = {
            id: q.id,
            domain: q.domain,
            subDomain: q.subDomain,
            question: q.question,
            type: q.questionType
              ? q.questionType.toLowerCase() === QuestionType.Rating.toLowerCase()
                ? QuestionType.Rating
                : q.questionType.toLowerCase() === QuestionType.YesNo.toLowerCase()
                  ? QuestionType.YesNo
                  : q.questionType.toLowerCase() === QuestionType.Numeric.toLowerCase()
                    ? QuestionType.Numeric
                    : QuestionType.Text
              : QuestionType.Text,
            answer: q.answer,
            guide: q.guide,
          };

          return question;
        })
        : [];
    })
  );
}

// PUT: /api/v1/assessments/question/:id
export function saveAnswer(id, question) {
  let url = `assessments/${id}/questions`;
  return from(AuthHttp.put(url, question));
}

// POST: /api/v1/assessments/
export function createAssessment(assessment) {
  let url = 'assessments/';
  return from(AuthHttp.post(url, assessment));
}

// GET: /api/v1/assessments/cost
export function getAllAssessmentCost(page, pageSize) {
  let url = `assessments/cost?_page=${page}&_limit=${pageSize}`;
  return from(AuthHttp.get(url));
}

// PUT: /api/v1/assessments/
export function updateAssessment(id, assessment) {
  let url = `assessments/${id}`;
  return from(AuthHttp.put(url, assessment));
}

// POST: /api/v1/assessments/start/:id
export function startAssessment(id, assessment) {
  let url = `assessments/start/${id}`;
  return from(AuthHttp.post(url, assessment));
}

// GET: /api/v1/assessments/recommend/:id
export function getRecommend(id) {
  let url = `assessments/recommend/${id}`;
  return from(AuthHttp.get(url));
}

/**
 * GET: /assessments/user
 * Get all current user working assessments
 * @param keyword assessment name
 * @param page page index
 * @param limit page size
 */

export function getAllAssessmentByUser(keyword, page, limit) {
  let url = `assessments/user?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `assessments/user?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(AuthHttp.get(url));
  }
  return from(AuthHttp.get(url));
}

// GET: /api/v1/assessments/templates/:id
export function getAssessmentTemplateByID(id) {
  let url = `assessments/templates/${id}`;
  return from(AuthHttp.get(url));
}

// DELETE: /api/v1/assessments/templates/:id
export function deleteAssessmentTemplate(id) {
  let url = `assessments/templates/${id}`;
  return from(AuthHttp.delete(url));
}

/**
 * GET: /assessments/templates/all
 * Get assessment templates
 * @param keyword assessment name
 * @param page page index
 * @param limit page size
 */
export function getAssessmentTemplate(keyword, page, limit) {
  let url = `assessments/templates/all?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `assessments/templates/all?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(Http.get(url));
  }
  return from(Http.get(url));
}
