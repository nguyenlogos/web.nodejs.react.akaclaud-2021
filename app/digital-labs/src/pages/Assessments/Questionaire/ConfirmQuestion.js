import { saveAnswer } from '@api';
import { Radio } from 'antd';
import * as React from 'react';

export default class ConfirmQuestion extends React.Component {
  subscriptions = [];
  constructor(props) {
    super(props);
    this.state = {
      answer: props.question.answer,
    };
  }
  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  onChange = (e) => {
    const { question, assessmentId } = this.props;

    const questionObject = {
      id: question.id,
      question: question.question,
      domain: question.domain,
      subDomain: question.subDomain,
      type: question.type,
      answer: e.target.value,
    };
    this.subscriptions.push(
      saveAnswer(assessmentId, questionObject).subscribe(
        () => {
          this.props.callbackFromParent(question);
          this.setState({ answer: e.target.value });
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      )
    );
  };

  render() {
    const { answer } = this.state;
    const { question, unanswered } = this.props;

    return (
      <div className={unanswered === question.key ? 'unanswered' : null}>
        <div ref={this.divFocus} className="title-question">
          <div className="content-question">{question.question}</div>
          <Radio.Group className="radio" onChange={this.onChange} value={answer}>
            <div className="answer">
              <Radio className="radio-button" value={'Yes'}>
                Yes
              </Radio>
            </div>
            <div className="answer">
              <Radio className="radio-button" value={'No'}>
                No
              </Radio>
            </div>
          </Radio.Group>
        </div>
      </div>
    );
  }
}
