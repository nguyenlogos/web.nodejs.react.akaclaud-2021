import Auxiliary from '@components/Auxiliary';
import React from 'react';

const AppNotification = () => {
  return (
    <Auxiliary>
      <div className="gx-popover-header">
        <h3 className="gx-mb-0">
          Notifications
        </h3>
        <i className="gx-icon-btn icon icon-charvlet-down" />
      </div>
    </Auxiliary>
  );
};

export default AppNotification;
