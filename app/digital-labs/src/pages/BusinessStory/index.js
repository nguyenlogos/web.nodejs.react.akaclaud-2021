import asyncComponent from '@components/AsyncComponent';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const AccountApp = ({ prefixRoute, history }) => {
  return (
    <div className="gx-page-layout business-story">
      <Switch>
        <Route
          path={`${prefixRoute}/`}
          exact={true}
          component={asyncComponent(() => import('./Catalog'), { history })}
        />
        <Route path={`${prefixRoute}/:id`} component={asyncComponent(() => import('./Detail'), { history })} />
      </Switch>
    </div>
  );
};

export default AccountApp;
