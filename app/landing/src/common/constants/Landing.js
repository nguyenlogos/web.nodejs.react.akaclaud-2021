
export const LANDING_TABS = [
    {
        id: "labs",
        text: "Digital Lab",
        slides: [
            {
                id: "slide1",
                title: "Reference Migration Solution",
                description: "Process of moving on-prems to Cloud. Combination of tooling, services, processes and manual steps as appropriate.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide2",
                title: "Minimum Business Infrastructure",
                description: "Provide a pre-configured standard & security environment - provisioned through Infrastructure as a code on Clouds.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide3",
                title: "Solution Development",
                description: "Quickly provide environment to build Proof of Concept that closest matching with customer demand.",
                image: "/landing/casestudy.png"
            },
        ],
        items: [
            {
                id: "item1",
                title: "MBI Provisioning",
                description: "Minimum business infrastructure to help you quickly get to know the standardized environments",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item2",
                title: "Solution Mapping",
                description: "Proven Business cases & Well-Architecture as your reference",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item3",
                title: "Solution Development",
                description: "Provide enviroment to build Proof of Concept that closest matching with customer demand",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item5",
                title: "Solution Migration",
                description: "Moving to the Cloud with highly reliable & cost saving methodology",
                icon: "/landing/Aglity.svg",
            },
        ],

    },
    {
        id: "assessment",
        text: "Cloud Assessment",
        slides: [
            {
                id: "slide1",
                title: "Cloud IT Strategy Assessment",
                description: "Map your organization’s optimal path for leveraging cost efficiencies along with opportunities for measurable improvements.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide2",
                title: "Cloud Workload Assessment",
                description: "Use cloud workload assessment to explore your organization's current environment and prioritize focus areas to enable and accelerate cloud adoption.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide3",
                title: "Cloud Migration Strategy",
                description: "Identify the best candidate applications for cloud migration, coupled with a proof-of-concept technical training",
                image: "/landing/casestudy.png"
            },
        ],
        items: [
            {
                id: "item1",
                title: "Cloud Readiness Assessment",
                description: "Assess your own cloud readiness: Create a detailed roadmap for cloud migration. ",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item2",
                title: "Cloud Provider Assessment",
                description: "Enables IT practitioners and technical teams to make smarter and faster cloud vendor decisions.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item3",
                title: "Cloud Migration Readiness Assessment",
                description: "Decision Point for Migrating Your Data Center to Public Cloud Services.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item4",
                title: "Application Migration Assessment",
                description: "Assess to offer the best migration strategy for the migration.",
                icon: "/landing/Aglity.svg",
            },
        ],
    },
    {
        id: "transformation",
        text: "Innovate & Transform",
        slides: [
            {
                id: "slide1",
                title: "Containerization/ APIfication",
                description: "Bundling an application together with all of its related configuration files, libraries and dependencies.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide2",
                title: "Application Modernization",
                description: "Create new business value from existing applications by refactoring, re-purposing or consolidation of legacy system.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide3",
                title: "BI & Data Analytics",
                description: "Analyzing the historical data, current data and predicting future trends to make the right changes in the proposed business mode.",
                image: "/landing/casestudy.png"
            },
        ],
        items: [
            {
                id: "item1",
                title: "Lift & Shift Migration",
                description: "Moving an application to cloud without redesigning the app.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item2",
                title: "App & Data Modernization",
                description: "Revise/ Refactor/ Rearchitect: A complete progress involves modification of the application.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item3",
                title: "Cloud Native Development",
                description: "Build cloud-native applications on Multi-Cloud without compromising on security or quality.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item4",
                title: "Containerization/ APIfication",
                description: "Bundling an application together with all of its related configuration files, libraries and dependencies.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item5",
                title: "BI & Data Analytics",
                description: "Data analytics, data visualization and data modeling techniques and technologies.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item6",
                title: "Devops/ DevSecOps",
                description: "Set of practices that combines software development (Dev) and IT operations (Ops) and Security (Sec).",
                icon: "/landing/Aglity.svg",
            },
        ],
    },
    {
        id: "optimise",
        text: "Manage & Optimize",
        slides: [
            {
                id: "slide1",
                title: "Cost Optimization",
                description: "Reducing your overall cloud spend by identifying mismanaged resources, and Right Sizing computing services to scale.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide2",
                title: "Security Compliance",
                description: "Right breadth and depth of continuity protection. Backup data center that mirrors the core functions they have here.",
                image: "/landing/casestudy.png"
            },
            {
                id: "slide3",
                title: "Backup and Disaster recovery",
                description: "Migrate workload of the whole system to AWS Cloud: SAP system, Oracle 12 data, SAP Solution.",
                image: "/landing/casestudy.png"
            },
        ],
        items: [
            {
                id: "item1",
                title: "Multi-Cloud Managed Services",
                description: "Offer IT management services to make it cloud-based.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item2",
                title: "Cloud Security Assurance",
                description: "Varying degrees of control over security, privacy and assurance, as well as different levels of risk.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item3",
                title: "Cloud Optimization Services",
                description: "Evaluate your cloud environment, report on critical gaps, and re-architect.",
                icon: "/landing/Aglity.svg",
            },
            {
                id: "item4",
                title: "Cloud Automation Services",
                description: "Enhance your Cloud capabilities with automation consulting services.",
                icon: "/landing/Aglity.svg",
            },
        ],
    },
];

export const LANDING_NAVIGATION = [
    {
        id: "home",
        text: "Home",
        url: "/"
    },
    {
        id: "labs",
        text: "Digital Lab",
        url: process.env.REACT_APP_DIGITAL_LABS
    },
    {
        id: "operation",
        text: "Cloud Operation",
        url: process.env.REACT_APP_CLOUD_OPERATION
    },
    {
        id: "security",
        text: "Security Assurance",
        url: process.env.REACT_APP_SECURITY_ASSURANCE
    },
    {
        id: "blog",
        text: "Blog",
        url: process.env.REACT_APP_BLOG
    },
];

export const LANDING_FOOTER_1 = [
    {
        text: "Home",
        url: "/"
    },
    {
        text: "About Us",
        url: "/about",
        children: [
            {
                text: "Company",
                url: "/"
            },
            {
                text: "Offerings",
                url: "/"
            },
            {
                text: "Case Studies",
                url: "/"
            },
            {
                text: "Awards",
                url: "/"
            },
            {
                text: "Partnerships",
                url: "/"
            },
            {
                text: "News",
                url: "/"
            },
            {
                text: "Experts",
                url: "/"
            },
            {
                text: "Contact Us",
                url: "/"
            },

        ]
    },
];

export const LANDING_FOOTER_2 = [
    {
        text: "Digital Lab",
        url: process.env.REACT_APP_DIGITAL_LABS,
        children: [
            {
                text: "Minimum Business Infrastructure",
                url: process.env.REACT_APP_DIGITAL_LABS + "/"
            },
            {
                text: "Reference Business Cases",
                url: process.env.REACT_APP_DIGITAL_LABS + "/"
            },
            {
                text: "Training Labs",
                url: process.env.REACT_APP_DIGITAL_LABS + "/"
            },
        ],
    },
    {
        text: "Assessment",
        url: "/"
    },
    {
        text: "Transformation & Migration",
        url: "/"
    },
];

export const LANDING_FOOTER_3 = [
    {
        text: "Operation",
        url: process.env.REACT_APP_CLOUD_OPERATION + "/cloud",
        children: [
            {
                text: "Account Management",
                url: "/"
            },
            {
                text: "Identity Management",
                url: "/"
            },
            {
                text: "Asset Management",
                url: "/"
            },
            {
                text: "Monitoring",
                url: "/"
            },
            {
                text: "Cost Management",
                url: "/"
            },
            {
                text: "Security Management",
                url: "/"
            },
            {
                text: "Backup & DR",
                url: "/"
            },
        ],
    }
];

export const LANDING_FOOTER_ICONS = [
    {
        title: "Facebook",
        image: "/landing/logo-facebook.svg",
        url: "https://www.facebook.com/"
    },
    {
        title: "RSS",
        image: "/landing/logo-rss.svg",
        url: "https://www.facebook.com/"
    },
    {
        title: "Slack",
        image: "/landing/logo-slack.svg",
        url: "https://www.facebook.com/"
    },
    {
        title: "Twitter",
        image: "/landing/logo-twitter.svg",
        url: "https://www.facebook.com/"
    },
    {
        title: "Youtube",
        image: "/landing/logo-youtube.svg",
        url: "https://www.facebook.com/"
    },
];

export const CUSTOMERS = [
    {
        title: "Adopt AI for advanced translations on AWS",
        description: "Lower translation costs by adopting AI advances into translation jobs. Improve the overall execution of their business in global markets just by streamlining the translation process.",
        image: "/landing/cs 3.png",
        //   companyName: "Royal Dutch Shell",
        //   companyImage: "/landing/Shell logo.png",
        //   hashtags: "#AWS",
    },
    {
        title: "Business continuity on Azure",
        description: "Right breadth and depth of continuity protection. In the case of a lights-out event, they want to be able to control how the systems failover.",
        image: "/landing/cs 2.png",
        //   companyName: "Royal Dutch Shell",
        //   companyImage: "/landing/Shell logo.png",
        //   hashtags: "#Azure",
    },
    {
        title: "Migrating SAP HANA to Azure",
        description: "Reduce server and storage hardware in their own datacenters to minimize IT related costs.",
        image: "/landing/cs 1.png",
        //   companyName: "Royal Dutch Shell",
        //   companyImage: "/landing/Shell logo.png",
        //   hashtags: "#Azure",
    },
];

export const VALUES = [
    {
        title: "BUSSINESS AGILITY",
        description: "With our best practice we have you to shorten time to market",
        image: "/landing/Aglity.svg",
        checklist: [
            "3x times faster to find relevant biz cases",
            "5x times faster on prepare environment",
            "25-40% faster on development",
            "15-30% cheaper of implementation cost"
        ]
    },
    {
        title: "SERVICE QUALITY",
        description: "With our well-architecture framework we have you to have better service quality",
        image: "/landing/quality.svg",
        checklist: [
            "Improve 10-30% of Service Quality",
            "Reduce security risk 20%-30%",
            "Improve operation efficiency 20-30%"
        ]
    },
    {
        title: "COST BENEFITS",
        description: "With our optimization framework and experts we have you to achieve cost benefits",
        image: "/landing/savecost.svg",
        checklist: [
            "10-25% Cloud TCO optimization",
            "Reduce 30-60% Common Pitfalls",
            "Low-cost data storage for archiving",
        ]
    },
    {
        title: "AVOID PITFALLS",
        description: "With our experience we help you to avoid common pitfalls like",
        image: "/landing/pitfall.svg",
        checklist: [
            "Hybrid cloud integration complexity",
            "Costly and time-consuming migration",
            // "Anticipated cost savings don’t materialize",
            "Vendor lock-in",
            "Data security and privacy",
            "Compliance and regulatory risks",
        ]
    },
];

export const ACCELERATORS = [
    {
        title: "DIGITAL LABS",
        imageUrl: "/landing/acc-labs.png",
        checklist: [
            { value: "Minimum Business Infrastructure" },
            { value: "Reference Solutions" },
            {
                children: [
                    "Migration/ Modernization",
                    "Managed Service",
                    "Innovation (Big Data, AI, IoT, Microservices)",
                    "Security",
                ]
            },
            { value: "Training Labs" }
        ]
    },
    {
        title: "CLOUD ASSESSMENT",
        imageUrl: "/landing/acc-assessment.png",
        checklist: [
            { value: "Web-based Questionaire"},
            {
                children: [
                    "Cloud Readiness",
                    "Migration Readiness",
                    "Application Migration",
                    "Cloud Provider Selection",
                    "Security Assessment",
                ]
            },
            { value: "IT Infra Discovery"},
            { value: "Application Inventory"},
            { value: "Dependency Map"}
        ]
    },
    {
        title: "CLOUD TRANSFORMATION & INNOVATION",
        imageUrl: "/landing/acc-transformation.png",
        checklist: [
            { value: "Migration Orchestration & Project Management"},
            { value: "Migration Utilities (Pre-built Migration Step)"},
            {
                children: [
                    "Server Migration",
                    "Application Migration",
                    "Database Migration"
                ]
            }
        ]
    },
    {
        title: "CLOUD OPERATION & OPTIMIZATION",
        imageUrl: "/landing/acc-operation.png",
        checklist: [
            { value: "MBI Provisioning/ Account Onboarding" },
            { value: "Account Management" },
            { value: "Asset Management" },
            { value: "Monitoring" },
            { value: "Cost Management" },
            { value: "Security Management" },
            { value: "Backup & DR"},
            { value: "Identity Management"}
        ]
    }
];

export const IMAGE_ERROR = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==";