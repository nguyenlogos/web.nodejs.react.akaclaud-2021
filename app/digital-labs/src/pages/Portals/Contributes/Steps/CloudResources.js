import { JsonEditor, YamlEditor } from 'octopus-editor';
import React, { Component } from 'react';

class CloudResource extends Component {
  constructor(props) {
    super(props);

    this.state = {
      resourceTemplate: props.defaultValue ? props.defaultValue.resourceTemplate : '',
      resourceParameter: props.defaultValue ? props.defaultValue.resourceParameter : '{}',
      resourcePolicy: props.defaultValue ? props.defaultValue.resourcePolicy : '{}',
    };
  }

  onChange = (value, name) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  validateInput = () => {
    const { resourceTemplate, resourceParameter, resourcePolicy } = this.state;

    return (
      resourceTemplate.trim().length > 0 && resourceParameter.trim().length > 0 && resourcePolicy.trim().length > 0
    );
  };

  render() {
    const { resourceTemplate, resourcePolicy, resourceParameter } = this.state;

    return (
      <div className="lab-overview-form_group">
        <div className="lab-overview-form_item">
          <label className="required" title="Resource infra management template">
            Resource template (YAML)
          </label>
          <YamlEditor
            className="code-editor"
            tabSize={4}
            theme="bright"
            content={resourceTemplate}
            onBlur={(value) => this.onChange(value, 'resourceTemplate')}
          />
        </div>

        <div className="lab-overview-form_item">
          <label className="required" title="Resource infra management template parameter">
            Resource Parameters (JSON)
          </label>
          <JsonEditor
            className="code-editor"
            tabSize={4}
            theme="bright"
            content={JSON.parse(resourceParameter)}
            onBlur={(value) => this.onChange(value, 'resourceParameter')}
          />
        </div>
        <div className="lab-overview-form_item">
          <label className="required" title="Resource infra management template launch policy">
            Launch policy (JSON)
          </label>
          <JsonEditor
            className="code-editor"
            tabSize={4}
            content={JSON.parse(resourcePolicy)}
            theme="bright"
            onBlur={(value) => this.onChange(value, 'resourcePolicy')}
          />
        </div>
      </div>
    );
  }
}

export default CloudResource;
