import { getPublicResourceUrl } from '@api';
import awsThumbnail from '@assets/images/thumbnails/aws-thumbnail.png';
import azureThumbnail from '@assets/images/thumbnails/azure-thumbnail.png';
import googleThumbnail from '@assets/images/thumbnails/gcp-thumbnail.png';
import { CloudProvider } from '@constants';
import { Image } from 'antd';
import React from 'react';

const getDefaultThumbnailUrl = (vendor) => {
  if (vendor === CloudProvider.Aws) {
    return awsThumbnail;
  }

  if (vendor === CloudProvider.Azure) {
    return azureThumbnail;
  }

  return googleThumbnail;
};

const FlexLayout = (props) => {
  const { onMouseEnter, onMouseLeave, labItem, onClick } = props;
  const { code, image, vendor, labName, complexityId, credit, timeInMinutes } = labItem;
  return (
    <div
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      className="gx-product-item  gx-product-vertical lab-card-flex"
      onClick={() => onClick(labItem)}
    >
      <div className="gx-product-image">
        <div className="gx-grid-thumb-equal">
          <span className="gx-link gx-grid-thumb-cover">
            <Image src={getPublicResourceUrl(code, image)} fallback={getDefaultThumbnailUrl(vendor)} />
          </span>
        </div>
      </div>
      <div className="gx-product-body lab-card-flex-content">
        <h3 className="gx-product-title lab-card-flex-content-title line-clamp">
          {labName}
        </h3>
        <div className="ant-row-flex">
          <h5 className="gx-text-muted ">
            {complexityId}
          </h5>
          <h5 className="gx-text-muted ">
            {credit}
          </h5>
        </div>
        <div className="ant-row-flex">
          <h5 className="gx-text-success">
            {vendor}
          </h5>
          <h5 className="gx-text-muted">
            {timeInMinutes}
            {' '}
            Minutes
          </h5>
        </div>
      </div>
    </div>
  );
};

export default FlexLayout;
