import React from "react";
import { Avatar, Popover } from "antd";
import { logOut } from "@common/auth";
import { connect } from "react-redux";

const UserMenu = ({ user }) => {
  const { name: userName } = user;

  const userMenuOptions = (
    <ul className="gx-user-popover">
      <li onClick={logOut} className="akac-header-dropdown-menu ">
        Logout
      </li>
    </ul>
  );

  return (
    <div className="gx-flex-row gx-align-items-center gx-avatar-row user-info">
      <Popover placement="bottomRight" content={userMenuOptions} trigger="click">
        <Avatar src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSYQNrIA4_2sAZ-3KikBTb2iNEalyMqQgr_5A&usqp=CAU" className="gx-pointer gx-mr-2 avatar-icon" alt="" />
        <span className="gx-avatar-name">
          {userName} <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
        </span>
      </Popover>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, {})(UserMenu);
