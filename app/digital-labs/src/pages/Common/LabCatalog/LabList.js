import { getLevels } from '@api';
import { Row } from 'antd';
import React, { Component } from 'react';

import LabItem from './LabItem';

export default class LabList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      level: [],
    };
    this.subscriptions = [];
  }

  componentDidMount() {
    this._getLevel();
  }

  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  _getLevel() {
    this.subscriptions.push(
      getLevels().subscribe((data) => {
        this.setState({ level: data });
      })
    );
  }

  _complexityIdToName(complexityId) {
    const { level } = this.state;
    let index = level.findIndex((e) => e.complexityId === complexityId);
    if (index !== -1) {
      return level[index].name;
    }
    return '';
  }

  setLabItem = (lab) => {
    const labItem = {};
    labItem._id = lab._id;
    labItem.labName = lab.labName;
    labItem.credit = lab.credit ? lab.credit + ' credit' : 'Free';
    labItem.vendor = lab.vendor;
    labItem.businessStory = lab.businessStory ? lab.businessStory : '';
    labItem.timeInMinutes = lab.timeInMinutes;
    labItem.type = lab.type;
    labItem.image = lab.image;
    labItem.code = lab.code;
    labItem.complexityId = this._complexityIdToName(lab.complexityId);
    return labItem;
  };

  render() {
    const { onClick, isFlexLayout, labList } = this.props;
    return (
      <Row className="lab-item-container">
        {labList &&
          labList.map((lab, i) => {
            const labItem = this.setLabItem(lab);
            return <LabItem labItem={labItem} key={i} isFlexLayout={isFlexLayout} onClick={onClick} />;
          })}
      </Row>
    );
  }
}
