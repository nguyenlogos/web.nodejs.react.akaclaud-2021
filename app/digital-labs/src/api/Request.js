import { from } from 'rxjs';

import { AuthHttp } from './client';

// POST /api/v1/requests/roles/contributor
export function addContributorRoleRequest(userName) {
  let url = 'requests/roles/contributor';
  return from(AuthHttp.post(url, userName));
}
