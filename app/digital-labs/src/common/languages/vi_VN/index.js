import antdVi from 'antd/lib/locale-provider/vi_VN';

import translates from './Messages.json';

const viLang = {
  messages: {
    ...translates
  },
  antd: antdVi,
  locale: 'vi-VN'
};
export default viLang;
