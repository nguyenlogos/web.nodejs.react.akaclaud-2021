import { insertScenario, storeDocuments } from '@api';
import { showAuthLoader } from '@appRedux/actions';
import { createTextFile, openNotification } from '@common/helpers';
import { Button, Col, Modal, Row, Tabs } from 'antd';
import React, { useState } from 'react';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

import BeforeStart from '../Steps/BeforeStart';
import BusinessCase from '../Steps/BusinessCase';
import GuideLine from '../Steps/GuideLine';
import LabOverview from '../Steps/LabOverview';
import Solution from '../Steps/Solution';
import LabSelector from './LabSelector';

const TabPane = Tabs.TabPane;
const { confirm } = Modal;

const ScenarioForm = ({ scenarioId }) => {
  const [tabActive, setTabActive] = useState('1');

  const [scenario, setScenario] = useState();
  const [preRequisite, setPreRequisite] = useState();
  const [guideline, setGuideline] = useState();
  const [beforeStart, setBeforeStart] = useState();
  const [solution, setSolution] = useState();
  const [steps, setSteps] = useState([]);

  const isScenarioValid = () => {
    return (
      scenario &&
      scenario.labName.trim().length > 0 &&
      // scenario.description.trim().length > 0 &&
      scenario.businessStory.trim().length > 0 &&
      scenario.vendor.trim().length > 0 &&
      parseInt(scenario.timeInMinutes) > 0 &&
      parseFloat(scenario.estimateCost) >= 0 &&
      scenario.digitalLabType != null &&
      scenario.childCategoryId.length > 0 &&
      scenario.complexityId != null &&
      parseInt(scenario.rating) >= 0
    );
  };

  const tabValidation = [
    isScenarioValid,
    () => !!preRequisite,
    () => !!guideline,
    () => !!beforeStart,
    () => !!solution,
  ];

  // const isFormValid = () => !tabValidation.map((validate) => validate()).includes(false);

  const onChangeTab = (tab) => {
    console.log('validate', parseInt(tabActive) - 1, tabValidation[parseInt(tabActive) - 1]());
    if (!tabValidation[parseInt(tabActive) - 1]() && parseInt(tab) > parseInt(tabActive)) {
      openNotification('error', 'Some required field need completed before');
    } else {
      setTabActive(tab);
    }
  };

  const validateSubmit = () => {
    confirm({
      title: 'Did you review this lab before submitting?',
      okText: 'Submit',
      okType: 'primary',
      cancelText: 'Review again',
      onOk: () => {
        showAuthLoader();
        if (!scenarioId) {
          createScenario();
        } else {
          // update
        }
      },
    });
  };

  const uploadLabResource = (labCode) => {
    const { image } = scenario;

    const data = new FormData();
    let needUpload = false;

    if (image) {
      data.append('intro-image.png', image);
      needUpload = true;
    }

    if (guideline) {
      if (typeof guideline != 'object') {
        let file = createTextFile('guidance-sub-tab.md', guideline + '', 'text/plain');
        data.append('guidance-sub-tab.md', file);
      } else {
        data.append('guidance-sub-tab.md', guideline);
      }
      needUpload = true;
    }

    if (preRequisite) {
      if (typeof preRequisite != 'object') {
        let file = createTextFile('pre-lab-sub-tab.md', preRequisite + '', 'text/plain');
        data.append('pre-lab-sub-tab.md', file);
      } else {
        data.append('pre-lab-sub-tab.md', preRequisite);
      }
      needUpload = true;
    }

    if (solution) {
      if (typeof solution != 'object') {
        let file = createTextFile('solution-sub-tab.md', solution + '', 'text/plain');
        data.append('solution-sub-tab.md', file);
      } else {
        data.append('solution-sub-tab.md', solution);
      }
      needUpload = true;
    }

    if (beforeStart) {
      if (typeof beforeStart != 'object') {
        let file = createTextFile('before-start-sub-tab.md', beforeStart + '', 'text/plain');
        data.append('before-start-sub-tab.md', file);
      } else {
        data.append('before-start-sub-tab.md', beforeStart);
      }
      needUpload = true;
    }

    if (needUpload) {
      console.log('we need to upload', data, labCode);
      return storeDocuments(data, labCode);
    }

    return of([]);
  };

  const createScenario = () => {
    if (scenario) {
      const code = uuidv4().replace(/-/g, '').toUpperCase();
      const {
        labName,
        // description,
        businessStory,
        timeInMinutes,
        childCategoryId,
        complexityId,
        vendor,
        estimateCost,
        digitalLabType,
        rating,
      } = scenario;

      return insertScenario({
        code,
        labName,
        timeInMinutes,
        childCategoryId,
        estimateCost,
        vendor,
        digitalLabType,
        complexityId,
        steps,
        rating,
        // description,
        // notes,
        businessStory,
        image: 'intro-image.png',
        guidanceSubTabURL: 'guidance-sub-tab.md', //guidelines
        preLabSubTabURL: 'pre-lab-sub-tab.md', //prerequisite
        solutionURL: 'solution-sub-tab.md',
        beforeStartURL: 'before-start-sub-tab.md',

        parameters: 'disable-server-validation',
        cloudformationURL: 'disable-server-validation',
        resourceParameterURL: 'disable-server-validation',
        launchPolicy: 'disable-server-validation',
      })
        .pipe(
          concatMap(() => {
            return uploadLabResource(code);
          })
        )
        .subscribe(
          (code) => {
            openNotification('success', 'Scenario is created');
          },
          (error) => {
            openNotification('error', 'Create scenario error');
          }
        );
    } else {
    }
  };

  return (
    <div className="contribute-scenario contribute-lab">
      <Row>
        <Col span={24}>
          <div className="contribute-lab_tab-content">
            <Tabs activeKey={tabActive} onChange={onChangeTab} tabPosition="top" className="contribute-lab_tab-detail">
              <TabPane tab="1. Overview" key="1">
                <div className="tab-content">
                  <LabOverview defaultValue={scenario} onChange={setScenario} isScenario={true} />
                  <LabSelector steps={steps} setSteps={setSteps} />
                </div>
              </TabPane>
              <TabPane tab="2. Business Story" key="2">
                <div className="tab-content">
                  <BusinessCase defaultValue={preRequisite} onChange={setPreRequisite} />
                </div>
              </TabPane>
              <TabPane tab="3. Digital Lab" key="3">
                <div className="tab-content">
                  <GuideLine defaultValue={guideline} onChange={setGuideline} />
                  <LabSelector steps={steps} readOnly={true} />
                </div>
              </TabPane>
              <TabPane tab="4. Before You Start" key="4">
                <div className="tab-content">
                  <BeforeStart defaultValue={beforeStart} onChange={setBeforeStart} />
                </div>
              </TabPane>
              <TabPane tab="5. Solution" key="5">
                <div className="tab-content">
                  <Solution defaultValue={solution} onChange={setSolution} />
                </div>
              </TabPane>
            </Tabs>
          </div>
        </Col>
        <Col span={24} className="contribute-lab_footer-button">
          <div className="footer">
            <div className="back-button">
              <Button disabled={tabActive === '1'} onClick={() => onChangeTab(`${parseInt(tabActive) - 1}`)}>
                Back
              </Button>
            </div>
            <div className="save-next-button">
              <Button className="save-button" onClick={validateSubmit}>
                Save
              </Button>
              <Button
                type="primary"
                disabled={tabActive === '5'}
                className="next-button"
                onClick={() => onChangeTab(`${parseInt(tabActive) + 1}`)}
              >
                Next
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ScenarioForm;
