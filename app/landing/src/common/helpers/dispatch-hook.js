import { AUTHORIZATION_CODE, HOOK_AUTHORIZATION_TYPE } from "../constants/index";

export function dispatchAuthorization(status, code = AUTHORIZATION_CODE) {
  const rootStore = window["rootStore"];

  if (rootStore) {
    const lastDispatch = window[HOOK_AUTHORIZATION_TYPE];
    if (lastDispatch) {
      const time = Date.now() - +lastDispatch;

      if (time < 5000) {
        // 5 seconds delay
        return;
      }
    }

    window[HOOK_AUTHORIZATION_TYPE] = Date.now();
    rootStore.dispatch((dispatch) => {
      dispatch({
        type: HOOK_AUTHORIZATION_TYPE,
        payload: { status, code },
      });
    });
  }
}

export function getRootStoreState() {
  const rootStore = window["rootStore"];

  if (rootStore == null) {
    return {};
  }

  return rootStore.getState();
}
