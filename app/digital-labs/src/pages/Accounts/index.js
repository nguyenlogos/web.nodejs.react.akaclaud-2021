import asyncComponent from '@components/AsyncComponent';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const AccountApp = ({ prefixRoute, history }) => {
  return (
    <div className="gx-main-content-wrapper">
      <Switch>
        <Route
          path={`${prefixRoute}/onboarding`}
          component={asyncComponent(() => import('./Onboarding'), { history })}
        />

        <Route path={`${prefixRoute}/mbi`} component={asyncComponent(() => import('./CloudMBI'))} />
      </Switch>
    </div>
  );
};

export default AccountApp;
