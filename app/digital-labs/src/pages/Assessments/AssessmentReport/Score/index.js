import TableOfContent from '@components/TableOfContent';
import { Col, Progress, Row } from 'antd';
import React from 'react';

import CategoryReport from './CategoryReport';

const titleToId = (title, tab) => title.split(' ').join('-') + tab;

const Score = (props) => {
  const {
    totalscore,
    report,
    savescore,
    activeRecommendation,
    reportName,
    recommend,
    toc,
    openRecommendation,
    closeRecommendation,
  } = props;

  return (
    <Row>
      <Col md={24} xl={20}>
        <div className="score report-tab">
          <Row className="overall-score">
            <Col className="overall" xl={16} sm={16}>
              <div className="overall-title tab-title" id="top">
                Overall Assessment Score
              </div>
              <span className="overall-content">
                We’ve finished analysing your survey responses to find out where you can create a detailed roadmap for
                cloud migration. Fill the rest your information to get Your Full Report Now
              </span>
            </Col>
            <Col className="overall-progress" xl={{ span: 8, offset: 0 }} sm={{ span: 6, offset: 1 }}>
              <Progress
                width={180}
                type="circle"
                percent={totalscore}
                strokeColor={totalscore < 60 ? '#FFCC01' : '#34C759'}
              />
            </Col>
          </Row>
          <div className="domains">
            {report &&
              report.map((r, index) => (
                <div id={titleToId(r.category, '1')} key={index} className="domain">
                  <CategoryReport
                    categoryIndex={index}
                    category={r.category}
                    readiness={r.readiness}
                    percent={savescore[index]}
                    activeRecommendation={activeRecommendation}
                    reportName={reportName}
                    recommend={recommend}
                    openRecommendation={() => openRecommendation(index)}
                    closeRecommendation={closeRecommendation}
                  />
                </div>
              ))}
          </div>
        </div>
      </Col>
      <Col className="anchor" xs={0} lg={4}>
        <div className="anchor-detail">
          <TableOfContent items={toc} />
        </div>
      </Col>
    </Row>
  );
};
export default Score;
