export class Level {
  constructor() {
    this.id = '';
    this.levelId = '';
    this.name = '';
    this.point = 0;
    this.active = false;
    this.complexityId = null;
    this.createdBy = '';
    this.updatedBy = '';
    this.createdTime = new Date();
    this.updatedTime = new Date();
  }

  fromJson(json) {
    if (!json) {
      return this;
    }

    this.id = json._id;
    this.levelId = json.levelId;
    this.name = json.name;
    this.point = +json.point;
    this.active = json.active;
    this.complexityId = +json.levelId;
    this.createdBy = json.createdBy;
    this.updatedBy = json.updatedBy;
    this.updatedBy = json.updatedBy;
    this.createdTime = json.createdTime ? new Date(json.createdTime) : null;
    this.updatedTime = json.updatedTime ? new Date(json.updatedTime) : null;

    return this;
  }
}
