import MarkdownViewer from '@components/MdViewer';
import React, { useEffect } from 'react';

const Guidance = ({ guideline }) => {
  useEffect(() => {}, []);

  return (
    <>
      <h1 className="guideline">
        Guideline
      </h1>
      <MarkdownViewer src={guideline} />
    </>
  );
};

export default Guidance;
