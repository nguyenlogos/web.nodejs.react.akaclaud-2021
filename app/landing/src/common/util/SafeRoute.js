import * as React from "react";
import { connect } from "react-redux";
import { Route } from "react-router-dom";
import { dispatchAuthorizationAction } from '../appRedux/actions';
import { authorValid, AUTHORIZATION_CODE, login } from "../auth";

// interface SafeRouteProps {
//   render?: (props) => any;
//   component?: JSX.Element;
//   exact?: boolean;
//   path?: string;
//   protected?: string;
//   history?: History;
//   roles?: History;
//   updateRouting?: ({ path, title }) => void;
// }

class SafeRoute extends React.Component {
  componentDidMount() {
    this.checkAuthentication()
        .then(() => {
            console.log('Logged in');
            this.checkValidAuthor(this.props);
        })
        .catch((e) => {
            console.log("Not authenticated.", this._currentLoginRoles());
        });
  }

  _currentLoginRoles = () => {
    return this.props.user != null ? this.props.user.roles : null;
  };

  checkAuthentication() {
    return new Promise((resolve, reject) => {
      const roles = this._currentLoginRoles();

      if (roles == null) {
        login().subscribe(() => {
          console.log("Redirect login...");
        });

        reject(false);
      } else {
        resolve(true);
      }
    });
  }

  checkValidAuthor(props) {
    const { history, dispatchAuthorization, roles: expectedRoles } = props;
    const roles = this._currentLoginRoles();

    if (!authorValid(expectedRoles, roles || "")) {
      // TODO: handle permission denied
      if (history) {
        // history.push("/permission-denied");
      }
      dispatchAuthorization("401", AUTHORIZATION_CODE.PermissionMissing);
    }
  }

  render() {
    const { exact, path, render: childRender, component: Component } = this.props;
    const childRenderFunction = (matchProps) => childRender
        ? childRender(matchProps)
        : <Component {...matchProps} />;

    return exact ? (
      <Route
        exact
        path={path}
        render={childRenderFunction}
      />
    ) : (
      <Route
        path={path}
        render={childRenderFunction}
      />
    );
  }
}

const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps, {
    dispatchAuthorization: dispatchAuthorizationAction,
})(SafeRoute);