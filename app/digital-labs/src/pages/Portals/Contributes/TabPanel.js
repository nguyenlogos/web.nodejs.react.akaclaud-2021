import { openNotification } from '@common/helpers';
import { Button, Col, Row, Tabs, Space } from 'antd';
import React, { useEffect, useState } from 'react';

const TabPane = Tabs.TabPane;

/** tab interface
 * - index: number
 * - title: string
 * - validate: () => boolean
 * - template: React Element
 **/
const InputTabPanel = ({ onCompleted, tabs, loading }) => {
  const [workingTabs, setWorkingTabs] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const [activeTab, setActiveTab] = useState(null);

  // handle default active first tab
  useEffect(() => {
    if (tabs && tabs.length > 0) {
      //  sort tabs with indexing increase
      const sortedTabs = tabs.sort((a, b) => {
        return a.index > b.index ? 1 : a.index === b.index ? 0 : -1;
      });

      const firstTab = sortedTabs[0];

      setActiveIndex(firstTab.index);
      setActiveTab(firstTab);
      setWorkingTabs(sortedTabs);
    }
  }, [tabs]);

  const nextTab = () => {
    const nexTab = workingTabs[activeIndex + 1];

    return goTab(nexTab);
  };

  const previousTab = () => {
    const nexTab = workingTabs[activeIndex - 1];

    return goTab(nexTab);
  };

  const goTab = (tab) => {
    // validate active tab
    const currentValid = validateTab(activeTab);
    if (!currentValid) {
      return openNotification('error', 'Some required field need completed before');
    }

    setActiveIndex(tab.index);
    setActiveTab(tab);
  };

  const validateTab = (tab) => {
    const tabValid = tab.validate();

    return tabValid === true;
  };

  const validateAllTabs = () => {
    for (var i = 0; i < workingTabs.length; i++) {
      const tab = workingTabs[i];
      const tabValid = validateTab(tab);

      if (!tabValid) {
        goTab(tab);
        return false;
      }
    }

    return true;
  };

  const onSubmitClick = () => {
    const inputValid = validateAllTabs();
    if (!inputValid) {
      return openNotification('error', 'Some required field need completed before');
    }

    if (onCompleted != null) {
      onCompleted();
    }
  };

  const onChangeTab = (tabIndex) => {
    const targetTab = workingTabs[tabIndex];

    return goTab(targetTab);
  };

  const renderTab = (tab) => {
    const { template: Template, index, title } = tab;

    return (
      <TabPane key={`${index}`} tab={title} className="tab-content">
        <Template />
      </TabPane>
    );
  };

  return (
    <div className="contribute-lab_tabs">
      <Tabs
        activeKey={`${activeIndex}`}
        onChange={onChangeTab}
        tabPosition="top"
        className="contribute-lab_tab-content"
      >
        {workingTabs &&
          workingTabs.map((tab) => {
            return renderTab(tab);
          })}
      </Tabs>

      <Row className="contribute-lab_footer-button" align="center">
        <Col span={12} >
          <div className="back-button">
            <Button disabled={activeIndex < 1} onClick={previousTab}>
              Back
            </Button>
          </div>
        </Col>
        <Col span={12} >
          <Space direction="horizontal" align="start" style={{float: 'right'}}>
            <div className="save-next-button">
              <Button className="save-button" onClick={onSubmitClick}>
                Save
              </Button>

              <Button
                type="primary"
                disabled={activeIndex >= workingTabs.length - 1}
                className="next-button"
                onClick={nextTab}
              >
                Next
              </Button>
            </div>
          </Space>

        </Col>
      </Row>
    </div>
  );
};

export default InputTabPanel;
