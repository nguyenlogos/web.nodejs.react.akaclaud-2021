import { getAllReadyTrainingLab } from '@api';
import Breadcrumbs from '@components/Breadcrumbs';
import { DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE } from '@constants';
import Catalog from '@pages/Common/LabCatalog';
import { Col, Row } from 'antd';
import queryString from 'query-string';
import React, { Component } from 'react';
import { map } from 'rxjs/operators';

export default class extends Component {
  _subscriptions = [];

  constructor(props) {
    super(props);

    const parsed = queryString.parse(props.location.search);
    const categoryId = parsed.categoryId ? parsed.categoryId.toString() : null;
    const defaultFilters =
      categoryId != null
        ? [
          {
            key: 'childCategoryId',
            value: categoryId,
          },
        ]
        : [];

    this.state = {
      labs: [],
      count: 0,
      selectedItems: defaultFilters,
      keyword: null,
    };

    this.subscriptions = [];
  }

  componentDidMount() {
    const { selectedItems, keyword } = this.state;
    this._$loadAllLabs(keyword, DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, selectedItems);
  }

  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  _$loadAllLabs = (keyword, page, pageSize, selectedItems) => {
    const filter = {};
    selectedItems.forEach((i) => {
      filter[i.key] = i.value;
    });

    this._subscriptions.push(
      getAllReadyTrainingLab(keyword, page, pageSize, filter)
        .pipe(
          map((docs) => {
            const count = docs.count;
            const labs = docs.dataSources
              ? docs.dataSources.map((lab, index) => {
                lab['key'] = index;
                return lab;
              })
              : [];
            return {
              count,
              labs,
            };
          })
        )
        .subscribe(({ count, labs }) => {
          this.setState({ count, labs });
        })
    );
  };

  render() {
    const { count, labs, selectedItems } = this.state;
    const { history } = this.props;
    const { pathname } = history.location;
    const mapSelectedItems = selectedItems
      ? selectedItems.map((i) => {
        return {
          type: 'category',
          key: i.value,
          objectId: i.value,
        };
      })
      : [];

    return (
      <div className="lab-catalog">
        <Row align="middle" className="page-breadcrumbs">
          <Col span={24}>
            <Breadcrumbs list={[{ title: 'Digital Lab', to: '/' }, { title: 'Training Lab' }]} />
            <div className="nav-bar">
              <div className="nav-bar_title">
                <span>
                  Training Lab
                </span>
              </div>
              <div className="nav-bar-list">
                <span>
                  {`Total ${count} business stories`}
                </span>
              </div>
            </div>
          </Col>
        </Row>
        <Catalog
          labs={labs}
          count={count}
          getLabs={this._$loadAllLabs}
          selectedItems={mapSelectedItems}
          onClick={({ _id }) => history.push(`${pathname}/${_id}`)}
        />
      </div>
    );
  }
}
