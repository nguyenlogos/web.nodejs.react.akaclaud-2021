import { insertLab, storeDocuments } from '@api';
import { createTextFile, openNotification } from '@common/helpers';
import { DigitalLabTypeValue } from '@constants';
import { Modal } from 'antd';
import React, { useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

import BreadcumbLab from './BreadcumbLab';
import BusinessCase from './Steps/BusinessCase';
import CloudResource from './Steps/CloudResources';
import GuideLine from './Steps/GuideLine';
import LabOverview from './Steps/LabOverview';
import Solution from './Steps/Solution';
import InputTabPanel from './TabPanel';

const { confirm } = Modal;

const LabCreate = () => {
  let labCommon, labCloudTemplate, labBusinessCase, labSolution, labGuideLine;
  labCommon = labCloudTemplate = labBusinessCase = labSolution = labGuideLine = null;

  const $labCommonRef = useRef(null);
  const $labCloudTemplateRef = useRef(null);
  const $labBusinessCaseRef = useRef(null);
  const $labGuideLineRef = useRef(null);
  const $labSolutionRef = useRef(null);

  const history = useHistory();
  const subscriptions = [];

  const fnEffectHook = () => {
    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const tabs = [
    {
      index: 0,
      title: '1. Lab Overview',
      validate: () => {
        if ($labCommonRef.current == null) {
          return true;
        }

        return $labCommonRef.current.validateInput();
      },
      template: () => <LabOverview ref={$labCommonRef} onChange={(common) => (labCommon = common)} />,
    },
    {
      index: 1,
      title: '2. Resource Template',
      validate: () => {
        if ($labCloudTemplateRef.current == null) {
          return true;
        }

        return $labCloudTemplateRef.current.validateInput();
      },
      template: () => <CloudResource ref={$labCloudTemplateRef} onChange={(value) => (labCloudTemplate = value)} />,
    },
    {
      index: 2,
      title: '3. Business Story',
      validate: () => {
        if ($labBusinessCaseRef.current == null) {
          return true;
        }

        return $labBusinessCaseRef.current.validateInput();
      },
      template: () => <BusinessCase ref={$labBusinessCaseRef} onChange={(value) => (labBusinessCase = value)} />,
    },
    {
      index: 3,
      title: '4. GuideLine',
      validate: () => {
        if ($labGuideLineRef.current == null) {
          return true;
        }

        if (labCommon !== null && labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions) {
          return true;
        }

        return $labGuideLineRef.current.validateInput();
      },
      template: () => <GuideLine ref={$labGuideLineRef} onChange={(value) => (labGuideLine = value)} />,
    },
    {
      index: 4,
      title: '5. Solution',
      validate: () => {
        if ($labSolutionRef.current == null) {
          return true;
        }

        if (
          labCommon !== null &&
          (labCommon.digitalLabType === DigitalLabTypeValue.Training ||
            labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions)
        ) {
          return true;
        }

        return $labSolutionRef.current.validateInput();
      },
      template: () => <Solution ref={$labSolutionRef} onChange={(value) => (labSolution = value)} />,
    },
  ];

  const uploadLabResource = (labCode) => {
    const { resourceParameter, resourceTemplate, resourcePolicy } = labCloudTemplate;
    const { image, attachments } = labCommon;

    let guidline,
        preRequisite,
        solution = null;
    if (labGuideLine) {
      guidline = labGuideLine.guidline;
    }
    if (labBusinessCase) {
      preRequisite = labBusinessCase.preRequisite;
    }
    if (labSolution) {
      solution = labSolution.solution;
    }

    const data = new FormData();
    let needUpload = false;

    if (image) {
      data.append('intro-image.png', image);
      needUpload = true;
    }

    if (guidline) {
      if (typeof guidline != 'object') {
        let file = createTextFile('guidance-sub-tab.md', guidline + '', 'text/plain');
        data.append('guidance-sub-tab.md', file);
      } else {
        data.append('guidance-sub-tab.md', guidline);
      }
      needUpload = true;
    }

    if (preRequisite) {
      if (typeof preRequisite != 'object') {
        let file = createTextFile('pre-lab-sub-tab.md', preRequisite + '', 'text/plain');
        data.append('pre-lab-sub-tab.md', file);
      } else {
        data.append('pre-lab-sub-tab.md', preRequisite);
      }
      needUpload = true;
    }

    if (solution) {
      if (typeof solution != 'object') {
        let file = createTextFile('solution-sub-tab.md', solution + '', 'text/plain');
        data.append('solution-sub-tab.md', file);
      } else {
        data.append('solution-sub-tab.md', solution);
      }
      needUpload = true;
    }

    if (resourceTemplate) {
      let content = typeof resourceTemplate == 'object' ? JSON.stringify(resourceTemplate) : resourceTemplate;
      let type = '';
      try {
        JSON.parse(resourceTemplate);
        type = 'json';
      } catch {
        type = 'yaml';
      }
      const fileName = 'resource-template.' + type;
      let file = createTextFile(fileName, content, type);
      data.append(fileName, file);
      needUpload = true;
    }

    if (resourceParameter) {
      let content = typeof resourceParameter == 'object' ? JSON.stringify(resourceParameter) : resourceParameter;
      let type = '';
      try {
        JSON.parse(resourceParameter);
        type = 'json';
      } catch {
        type = 'yaml';
      }
      const fileName = 'param-template.' + type;
      let file = createTextFile(fileName, content, type);
      data.append(fileName, file);
      needUpload = true;
    }

    if (resourcePolicy) {
      let file = createTextFile('launch-policy.json', resourcePolicy, 'json');
      data.append('launch-policy.json', file);
      needUpload = true;
    }

    if (attachments && attachments.length > 0) {
      attachments.forEach((f) => {
        data.append(f.name, f.originFileObj);
      });

      needUpload = true;
    }

    if (needUpload) {
      return storeDocuments(data, labCode);
    }

    return of([]);
  };

  const insertNewLab = () => {
    const {
      labName,
      description,
      businessStory,
      timeInMinutes,
      childCategoryId,
      complexityId,
      vendor,
      estimateCost,
      notes,
      attachments,
      digitalLabType,
      rating,
    } = labCommon;
    const { resourceParameter, resourceTemplate } = labCloudTemplate;

    let labCode = uuidv4();
    labCode = labCode.replace(/-/g, '');
    labCode = labCode.toUpperCase();

    let type = 'json';
    try {
      JSON.parse(resourceTemplate);
      type = 'json';
    } catch {
      type = 'yaml';
    }

    const attachmentNames = attachments
      ? attachments.map((f) => {
        return f.name;
      })
      : [];

    const createLab = {
      labName,
      code: labCode,
      description,
      businessStory,
      digitalLabType,
      parameters: resourceParameter,
      resourceParameterURL: 'param-template.json',
      image: 'intro-image.png',
      guidanceSubTabURL: 'guidance-sub-tab.md',
      preLabSubTabURL: 'pre-lab-sub-tab.md',
      solutionURL: 'solution-sub-tab.md',
      cloudformationURL: 'resource-template.' + type,
      launchPolicy: 'launch-policy.json',
      timeInMinutes,
      childCategoryId,
      complexityId,
      vendor: vendor.toLowerCase(),
      estimateCost,
      notes,
      attachments: attachmentNames,
      rating,
    };

    if (labGuideLine == null) {
      createLab.guidanceSubTabURL = null;
    }

    if (labSolution == null) {
      createLab.solutionURL = null;
    }

    return insertLab(createLab);
  };

  const onSubmit = () => {
    confirm({
      title: 'Are you review this lab before submit?',
      okText: 'Submit',
      okType: 'primary',
      cancelText: 'Review again',
      onOk: () => {
        subscriptions.push(
          insertNewLab()
            .pipe(
              concatMap((lab) => {
                return uploadLabResource(lab.code);
              })
            )
            .subscribe(
              (code) => {
                openNotification('success', 'Lab is created');
                history.push('/portals/contributes/labs');
              },
              (error) => {
                openNotification('error', 'Create lab error');
              }
            )
        );
      },
    });
  };

  return (
    <div className="contribute-lab">
      <BreadcumbLab title="Add new lab" />

      <div className="create-lab">
        <InputTabPanel tabs={tabs} onCompleted={onSubmit} />
      </div>
    </div>
  );
};

export default LabCreate;
