import React from "react";
import { Breadcrumb } from "antd";
import { Link } from 'react-router-dom';

const Breadcrumbs = ({ list }) => (
    <Breadcrumb className="bread-crumb">
        {list.map((item) => item && (
            <Breadcrumb.Item key={item.title.split(" ").join("-")}>
                {item.to && <Link to={item.to}><span className="gx-link">{item.title}</span></Link>}
                {!item.to && <span>{item.title}</span>}
            </Breadcrumb.Item>
        ))}                  
    </Breadcrumb>
)
export default Breadcrumbs;