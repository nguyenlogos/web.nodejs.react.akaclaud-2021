import { getAllHistorystacks } from '@api';
import { formatDateLocaleString, openNotification } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Card, Table, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE } from '@constants';

const StacksHistory = () => {
  const subscriptions = [];
  const history = useHistory();
  const [stacksHistory, setStacksHistory] = useState({
    dataSource: [],
    totalCount: 0,
    page: DEFAULT_ADMIN_PAGE_INDEX,
    pageSize: DEFAULT_ADMIN_PAGE_SIZE,
  });
  const [loading, setLoading] = useState(false);
  const [keyword] = useState('');

  const fnEffectHook = () => {
    loadStacks(keyword, DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE);

    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const loadStacks = (keyword, page, pageSize) => {
    setLoading(true);

    subscriptions.push(
      getAllHistorystacks(keyword, page, pageSize).subscribe(
        (data) => {
          if (data) {
            const { dataSources: dataSource, count: totalCount, page, size: pageSize } = data;
            const mapDataSources = dataSource
              ? dataSource.map((data, key) => {
                data.key = key;
                return data;
              })
              : [];
            setStacksHistory({
              dataSource: mapDataSources,
              totalCount,
              page,
              pageSize,
            });
          }
        },
        () => {
          setLoading(false);
          openNotification('error', 'Loading stacks history list failed');
        },
        () => {
          setLoading(false);
        }
      )
    );
  };
  const onPageChange = (page) => {
    const { pageSize } = stacksHistory;
    const newPage = page > 0 ? page - 1 : 0;
    loadStacks(null, newPage, pageSize);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadStacks(null, 0, pageSize);
  };

  const navigateRunningLab = (text, record, index) => {
    return record.active ? (
      <span className="padding-left-0 ant-btn-link" onClick={() => history.push('/labs/' + record.labId)}>
        {text}
      </span>
    ) : (
      <span>
        {text}
      </span>
    );
  };

  const buildTableStacksHistory = () => {
    const { dataSource, totalCount, pageSize, page } = stacksHistory;
    const from = page * pageSize + 1;
    const pagination = {
      total: totalCount,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        title: '#',
        key: 'index',
        width: '5%',
        render: (text, record, index) => {
          return from + index;
        },
      },
      {
        title: 'Lab name',
        key: 'labName',
        dataIndex: 'labName',
        width: '35%',
        // render: (labName) => {
        //   return shortDescription(labName, 100);
        // },
        render: (text, record, index) => (
          text ?
            navigateRunningLab(text, record, index)
            : (
              <i>
                Lab was deleted
              </i>)
        ),
      },
      {
        title: 'Vendor',
        key: 'vendor',
        dataIndex: 'vendor',
        width: '10%',
      },
      {
        title: 'Credit',
        key: 'costLaunchLab',
        dataIndex: 'costLaunchLab',
        width: '15%',
        sorter: (a, b) => a.costLaunchLab - b.costLaunchLab,
        render: (text, record, index) => {
          if (record?.costLaunchLab) {
            return record.costLaunchLab;
          }
          return 0;
        },
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
        width: '15%',
        sorter: (a, b) => {
          return a['active'] - b['active'];
        },
        render: (text, record, index) => {
          if (record.active) {
            return (
              <Tag color="green" style={{ width: 70, textAlign: 'center' }}>
                Active
              </Tag>
            );
          }
          return (
            <Tag color="orange" style={{ width: 70, textAlign: 'center' }}>
              Inactive
            </Tag>
          );
        },
      },
      {
        title: 'Started Time',
        key: 'createdTime',
        dataIndex: 'createdTime',
        width: '20%',
        sorter: (a, b) => {
          return new Date(a['createdTime']).getTime() - new Date(b['createdTime']).getTime();
        },
        render: (text) => {
          const timeStr = formatDateLocaleString(text);
          return timeStr;
        },
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={dataSource}
        columns={columns}
        pagination={pagination}
        loading={loading}
        scroll={{ y: '100%' }}
      />
    );
  };

  return (
    <>
      <div style={{ marginBottom: '1.875rem' }}>
        <Breadcrumbs list={[{ title: 'Digital lab', to: '/portals/labs/stacks' }, { title: 'My Learning' }]} />
        <div style={{ fontSize: '1.5rem', fontWeight: 'bold', lineHeight: 1.75, color: '#121619' }}>
          <span>
            My Learning
          </span>
        </div>
      </div>
      <Card className="gx-card">
        {buildTableStacksHistory()}
      </Card>
    </>
  );
};

export default StacksHistory;
