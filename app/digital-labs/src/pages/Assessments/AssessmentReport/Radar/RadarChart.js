import React from 'react';
import {
  Legend,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart as Chart,
  ResponsiveContainer,
} from 'recharts';

const RadarChart = (props) => {
  const { questions } = props;
  const avgScore = (readiness) => {
    if (readiness && Object.values(readiness).length > 0) {
      const total = Object.values(readiness).reduce((total, current) => total + current);
      const amount = Object.values(readiness).length;
      return Math.ceil(total / amount);
    } else {
      return 0;
    }
  };
  const data = questions
    ? questions.map(({ category, readiness }) => ({
        category,
        cloudAdoption: avgScore(readiness),
        fullMark: 100,
      }))
    : [];

  return (
    <ResponsiveContainer outerRadius={160} width="100%" height={400}>
      <Chart data={data}>
        <Radar
          name="Level of cloud adoption"
          dataKey="cloudAdoption"
          stroke="#ffcc00"
          fill="#ffcc00"
          fillOpacity={0.5}
        />
        <PolarGrid />
        <PolarAngleAxis dataKey="category" />
        <PolarRadiusAxis />
        <Legend />
      </Chart>
    </ResponsiveContainer>
  );
};

export default RadarChart;
