import TableOfContent from '@components/TableOfContent';
import { Col, Row } from 'antd';
import * as React from 'react';

import DecisionRecommendation from './DecisionRecommendation';

const Decision = (props) => {
  const { recommend, toc } = props;
  return (
    <Row>
      <Col xs={24} md={18} lg={20}>
        <div className="recommendation report-tab">
          <div className="overall-recommendation">
            <div className="text-title">Overall Assessment Score</div>
            <span className="content-font-size">
              We’ve finished analysing your survey responses to find out where you can create a detailed roadmap for
              cloud migration. Fill the rest your information to get Your Full Report Now
            </span>
          </div>
          <div className="recommendation-report">
            <div className="recommendation-report_migration">
              <h3>MIGRATION DECISION RECOMMENDATION</h3>
              {recommend && recommend.map((recommendation, index) => {
                const recommendDetail = recommendation.recommendDetail.split('*|');
                if (!recommendation.domain) {
                  return (
                    <div>
                      <div key={index} className="recommendation-report_migration_content">
                        {recommendation.recommendOverview}
                      </div>
                      <div className="recommendation-report_migration_detail">
                        {recommendDetail && recommendDetail.map((r, index) => (
                          <div key={index}>{r}</div>
                        ))}
                      </div>
                    </div>
                  );
                }
                return '';
              })}
            </div>

            {recommend && recommend.map(
              (recommendation, index) =>
                recommendation.domain && (
                  <DecisionRecommendation key={index} recommendation={recommendation} recommendationIndex={index} />
                )
            )}
          </div>
        </div>
      </Col>
      <Col className="anchor" xs={0} md={6} lg={4}>
        <div className="anchor-detail">
          <TableOfContent items={toc} />
        </div>
      </Col>
    </Row>
  );
};

export default Decision;
