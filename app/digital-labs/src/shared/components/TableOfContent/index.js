import { TARGET_OFFSET } from '@constants';
import { Anchor } from 'antd';
import classnames from 'classnames';
import React from 'react';

const { Link } = Anchor;

export default class TableOfContent extends React.Component {
  getContainer = () => {
    return document.getElementById('body-content');
  };

  renderItem = (item, index) => {
    return (
      <Link href={item.href} title={item.title} key={index}>
        {item.children &&
          item.children.map((child, i) => {
            return <Link href={child.href} title={child.title} key={index + '.' + i} />;
          })}
      </Link>
    );
  };

  render() {
    const { classNames, items } = this.props;

    const destClassNames = classnames(classNames, 'toc', 'hidden-anchor');

    return (
      <div className={destClassNames}>
        <Anchor affix={false} targetOffset={TARGET_OFFSET} getContainer={this.getContainer}>
          {items &&
            items.map((item, i) => {
              return this.renderItem(item, i);
            })}
        </Anchor>
      </div>
    );
  }
}
