import { NAV_STYLE_FIXED, NAV_STYLE_MINI_SIDEBAR } from '@constants';
import React from 'react';
import { useSelector } from 'react-redux';

import Topbar from '../Topbar';
import HorizontalDark from '../Topbar/HorizontalDark';

const NavStyle = () => {
  const { navStyle } = useSelector(({ settings }) => settings);

  return navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR ? <Topbar /> : <HorizontalDark />;
};

export default NavStyle;
