import { RedoOutlined, SearchOutlined } from '@ant-design/icons';
import { activeLab, checkStackRunning, deactiveLab, getAllLabs } from '@api';
import { LabState } from '@common/constants';
import { getFilteredMenu, navigateTo, showSuccessToast, openNotification, truncateText } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Badge, Button, Card, Checkbox, Col, Divider, Input, Modal, Row, Table, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE } from '@constants';

const { confirm } = Modal;

const LabContributor = () => {
  const subscriptions = [];
  const [labContributor, setLabContributor] = useState({
    dataSource: [],
    totalCount: 0,
    page: DEFAULT_ADMIN_PAGE_INDEX,
    pageSize: DEFAULT_ADMIN_PAGE_SIZE,
  });
  const [loading, setLoading] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState({});

  // get url from first item of Contribution
  const { roles } = useSelector(({ user }) => user);
  var rootUrl = '/';
  const menuContribution = getFilteredMenu(roles).filter((item) => item.id === 'Contribute');
  if (menuContribution.length !== 0) {
    if (menuContribution[0].children.length !== 0) {
      rootUrl = menuContribution[0].children[0].url;
    }
  }

  const fnEffectHook = () => {
    loadAllLabs(DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE);
    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const loadAllLabs = async (page, pageSize, filters) => {
    setLoading(true);

    subscriptions.push(
      getAllLabs(page, pageSize, filters, keyword).subscribe(
        (data) => {
          if (data) {
            const { dataSources: dataSource, count: totalCount, page, size: pageSize } = data;
            const mapDataSources = dataSource
              ? dataSource.map((data, key) => {
                data.key = key;
                return data;
              })
              : [];

            setLabContributor({
              dataSource: mapDataSources,
              totalCount,
              page,
              pageSize,
            });
          }
          return data;
        },
        () => {
          setLoading(false);
          openNotification('error', 'Occurred error when load list lab from server!');
        },
        () => {
          setLoading(false);
        }
      )
    );
  };

  const onPageChange = (page) => {
    const { pageSize } = labContributor;
    const newPage = page > 0 ? page - 1 : 0;
    loadAllLabs(newPage, pageSize, filters);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadAllLabs(0, pageSize);
  };

  const onClickSearch = () => {
    const { pageSize } = labContributor;
    loadAllLabs(0, pageSize);
  };

  const onReload = () => {
    loadAllLabs(0, 10);
  };

  const keypressSearchAssessments = (e) => {
    const { pageSize } = labContributor;
    if (e.key === 'Enter') {
      loadAllLabs(0, pageSize);
    }
  };

  const searchAssessments = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    setKeyword(keyword);
  };


  const buildTableuserInfo = () => {
    const { dataSource, totalCount, pageSize, page } = labContributor;
    const from = page * pageSize + 1;
    const pagination = {
      total: totalCount,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        key: 'index',
        width: '5%',
        render: (text, record, index) => {
          return from + index;
        },
      },
      {
        title: 'Lab name',
        key: 'labName',
        dataIndex: 'labName',
        width: '20%',
        render: (labName) => {
          return truncateText(labName, 100, true);
        },
      },
      {
        title: 'Short Description',
        key: 'description',
        dataIndex: 'description',
        width: '20%',
        render: (description) => {
          return truncateText(description, 100, true);
        },
      },
      {
        title: 'Launch Time',
        key: 'timeInMinutes',
        dataIndex: 'timeInMinutes',
        width: '10%',
      },
      {
        title: 'Created By',
        key: 'createdBy',
        dataIndex: 'createdBy',
        width: '20%',
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
        width: '15%',
        render: (status) => {
          if (status === 'ready') {
            return (
              <Tag color="green" style={{ width: 70, textAlign: 'center' }}>
                Ready
              </Tag>
            );
          }
          return (
            <Tag color="orange" style={{ width: 70, textAlign: 'center' }}>
              Pending
            </Tag>
          );
        },
      },

      {
        title: 'Action',
        key: 'active',
        width: '10%',
        render: (text, record) => (
          <span>
            {record.status === LabState.ready && (
              <span className="gx-link" onClick={() => changeActive(record)}>
                {record.active ? 'Deactive' : 'Active'}
              </span>
            )}
            {record.status === LabState.pending && (
              <>
                <span className="gx-link" onClick={() => lauchEditLab(record._id)}>
                  Edit
                </span>
              </>
            )}
            {record.status === LabState.ready && record.active === false && (
              <>
                <Divider type="vertical" />
                <span className="gx-link" onClick={() => lauchEditLab(record._id)}>
                  Edit
                </span>
              </>
            )}
          </span>
        ),
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={dataSource}
        columns={columns}
        pagination={pagination}
        loading={loading}
        scroll={{ y: '100%' }}
      />
    );
  };

  const changeActive = (record) => {
    const { page, pageSize } = labContributor;
    subscriptions.push(
      checkStackRunning(record._id).subscribe((stack) => {
        if (Boolean(stack.active) === true) {
          return openNotification('error', 'This lab is running');
        } else {
          confirm({
            title: record.active ? 'Deactive' : 'Active',
            content: `Are you sure ${record.active ? 'deactive' : 'active'} this lab?`,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
              if (record.active) {
                subscriptions.push(
                  deactiveLab(record._id).subscribe(
                    () => {
                      showSuccessToast('Change status success');
                      loadAllLabs(page, pageSize);
                    },
                    () => {
                      openNotification('error', 'Error occured');
                    }
                  )
                );
              } else {
                subscriptions.push(
                  activeLab(record._id).subscribe(
                    () => {
                      showSuccessToast('Change status success');
                      loadAllLabs(page, pageSize);
                    },
                    () => {
                      openNotification('error', 'Error occured');
                    }
                  )
                );
              }
            },
          });
        }
      })
    );
  };

  const lauchEditLab = (recordId) => {
    navigateTo('/edit/' + recordId);
  };

  const onChange = (e) => {
    const { pageSize } = labContributor;
    if (e.length === 0 || e.length === 2) {
      setFilters({});
      loadAllLabs(0, pageSize);
    }
    if (e.length === 1 && e[0] === 'pending') {
      setFilters({ status: 'pending' });
      loadAllLabs(0, pageSize, { status: 'pending' });
    }
    if (e.length === 1 && e[0] === 'ready') {
      setFilters({ status: 'ready' });
      loadAllLabs(0, pageSize, { status: 'ready' });
    }
  };

  const options = [
    { label: 'Pending Labs', value: 'pending' },
    { label: 'Ready Labs', value: 'ready' },
  ];

  return (
    <>
      <div style={{ marginBottom: '1.875rem' }}>
        <Breadcrumbs
          list={[
            { title: 'My Contribution', to: rootUrl },
            { title: 'All lab', to: '/contributes/labs/all' },
            { title: 'Labs' },
          ]}
        />
        <div style={{ fontSize: '1.5rem', fontWeight: 'bold', lineHeight: 1.75, color: '#121619' }}>
          <span>
            Labs Contributes
          </span>
        </div>
      </div>
      <Card className="gx-card" title="Labs Status">
        <div style={{ justifyContent: 'flex-start', display: 'flex' }}>
          <div style={{ marginRight: 60 }}>
            There are 
            {' '}
            <Badge count={labContributor.totalCount} style={{ backgroundColor: '#52c41a' }} />
            {' '}
            labs in list
          </div>
          <Checkbox.Group options={options} onChange={onChange} />
        </div>
      </Card>
      <Card className="gx-card">
        <Row style={{ justifyContent: 'flex-end', display: 'flex' }}>
          <Col span={6}>
            <Input
              type="text"
              onKeyPress={keypressSearchAssessments}
              onChange={searchAssessments}
              placeholder="Type keyword to type"
            />
          </Col>
          <Col span={3}>
            <Button shape="circle" icon={<SearchOutlined />} onClick={onClickSearch} />
            <Button shape="circle" icon={<RedoOutlined />} onClick={onReload} />
          </Col>
        </Row>
        <Divider />
        {buildTableuserInfo()}
      </Card>
    </>
  );
};

export default LabContributor;
