import { switchLanguage } from '@appRedux/actions';
import { SUPPORT_LANGUAGES } from '@common/languages';
import { Popover } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

const SelectLanguages = () => {
  const dispatch = useDispatch();
  let { locale } = useSelector(({ settings }) => settings);

  const languageMenu = () => (
    <ul className="gx-sub-popover">
      {SUPPORT_LANGUAGES.map(language =>
        (<li className="gx-media gx-pointer" key={language.name} onClick={() => dispatch(switchLanguage(language))}>
          <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`} />
          <span className="gx-language-text">
            {language.name}
          </span>
        </li>)
      )}
    </ul>);

  return (
    <Popover
      overlayClassName="gx-popover-horizantal" 
      placement="right"
      content={languageMenu()}
      trigger="click"
    >
      <span className="gx-pointer gx-flex-row gx-align-items-center">
        <i className={`flag flag-24 flag-${locale.icon}`} />
        <span className="gx-pl-2 gx-language-name">
          {locale.name}
        </span>
        <i className="icon icon-chevron-down gx-pl-1" />
      </span>
    </Popover>
  );
};

export default SelectLanguages;
