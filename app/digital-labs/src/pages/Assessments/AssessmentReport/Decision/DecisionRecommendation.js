import React from 'react';

const DecisionRecommendation = (props) => {
  const { recommendation, recommendationIndex } = props;
  const recommendDetail = recommendation.recommendDetail.split('*|');
  return (
    <div id={recommendation.domain} className={'recommendation-report_decision'}>
      <hr className="line" />
      <div className="recommendation-report_content">
        <div className="domain">
          {recommendationIndex + 1}.{recommendation.domain}
        </div>
        <div className="decision">
          <div className="decision_overview">{recommendation.recommendOverview}</div>
          <div className="decision_detail">
            {recommendDetail && recommendDetail.map((decisionDetail, index) => <div key={index}>{decisionDetail}</div>)}
          </div>
        </div>
      </div>
    </div>
  );
};
export default DecisionRecommendation;
