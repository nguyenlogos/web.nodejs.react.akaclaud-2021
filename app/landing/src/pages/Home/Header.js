import { MenuOutlined } from "@ant-design/icons";
import React from "react";
import { Menu, Row, Col, Dropdown } from "antd";
import { LANDING_NAVIGATION } from "@constants/Landing";
// import LanguageSelector from "@components/LanguageSelector";

const Header = ({ user }) => {
  const isLoggedIn = (user) => user && user.roles;
  const userRole = isLoggedIn(user) ? user.roles : "";
  const menu = (
    <Menu>
      {LANDING_NAVIGATION.map((item) => (
        <Menu.Item key={item.id}>
          <a href={item.url}>{item.text}</a>
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Row className="header" justify="center">
      <Col span={20} className="header-container">
        <Row align="middle" justify="space-between" className="header-top">
          <Col xs={6} sm={8} md={9} className="gx-px-0 mobile-layout">
            <Dropdown overlay={menu} trigger={["click"]}>
              <a className="ant-dropdown-link apps-navigation menu" onClick={(e) => e.preventDefault()}>
                <MenuOutlined style={{ fontSize: "1rem" }} />
              </a>
            </Dropdown>
          </Col>
          <Col className="gx-px-lg-0" xs={10} sm={8} md={6} lg={12} xl={12}>
            <div className="akac-logo gx-mb-2">
              <img src="/logo.svg" alt="" />
            </div>
          </Col>
          <Col xs={8} sm={8} md={9} lg={12} xl={12} className="gx-px-0">
            <ul className="akac-header-notifications gx-ml-auto" style={{ justifyContent: "flex-end" }}>
              {/* {ICON_NAVIGATION.map(
                                (item, index) =>
                                filterChildren(item, userRole) && (
                                    <li className="gx-notify" key={index}>
                                        <a href={item.url} target="_blank" rel="noopener noreferrer">
                                            <span className="gx-pointer gx-d-block header-icon">{item.icon}</span>
                                        </a>
                                    </li>
                                )
                            )} */}
              {/* <li className="gx-user-nav">
                                <LanguageSelector
                                    currentLanguage={{ id: "vn", text: "VN" }}
                                    languages={[{ id: "vn", text: "VN" }, { id: "en", text: "EN"}]}
                                />
                            </li> */}
              {/* <li className="gx-user-nav gx-w-100">
                                {isLoggedIn(user) ? (1
                                    <UserMenu user={user} />
                                ) : (
                                        <div className="login-btn-container">
                                            <Button className="login-btn" onClick={login}>
                                                Login
                                        </Button>
                                        </div>
                                    )}
                            </li> */}
            </ul>
          </Col>
        </Row>

        <Menu className="web-layout" mode="horizontal" selectedKeys={["home"]}>
          {LANDING_NAVIGATION.map((item) => (
            <Menu.Item key={item.id}>
              <a href={item.url}>{item.text}</a>
            </Menu.Item>
          ))}
        </Menu>
      </Col>
    </Row>
  );
};
export default Header;
