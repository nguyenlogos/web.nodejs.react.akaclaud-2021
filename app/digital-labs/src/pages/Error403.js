import React from 'react';
import { Link } from 'react-router-dom';

const Error403 = () => (
  <div className="gx-page-error-container">
    <div className="gx-page-error-content">
      <div className="gx-error-code">
        403
      </div>
      <h2 className="gx-text-center">
        permission denied
      </h2>

      <p className="gx-text-center">
        <Link className="gx-btn gx-btn-primary" to="/">
          Go to Home
        </Link>
      </p>
    </div>
  </div>
);

export default Error403;
