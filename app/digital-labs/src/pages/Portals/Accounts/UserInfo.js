import { RedoOutlined, SearchOutlined } from '@ant-design/icons';
import { deleteCloudAccount, editCloudAccount, getAccountsByUser } from '@api';
import { formatDateLocaleString, openNotification } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import { Button, Card, Col, Divider, Input, Modal, Row, Space, Table, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE } from '@constants';

const { confirm } = Modal;

const UserInfo = () => {
  const subscriptions = [];
  const [userInfo, setUserInfo] = useState({
    dataSource: [],
    totalCount: 0,
    page: DEFAULT_ADMIN_PAGE_INDEX,
    pageSize: DEFAULT_ADMIN_PAGE_SIZE,
  });
  const [loading, setLoading] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [visibleEditForm, setVisibleEditForm] = useState(false);
  const [editedCloudAccount, setEditedCloudAccount] = useState(null);

  const fnEffectHook = () => {
    loadUserInfo(keyword, DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE);

    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const handleDeleteAccount = (id) => {
    confirm({
      title: 'Are you sure delete this account?',
      okText: 'Delete',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk: () => {
        setLoading(true);

        subscriptions.push(
          deleteCloudAccount(id).subscribe(
            (data) => {
              const { pageSize, page } = userInfo;

              setLoading(false);
              loadUserInfo(keyword, page, pageSize);
              openNotification('success', 'Delete account cloud success');
            },
            () => {
              const { pageSize, page } = userInfo;

              setLoading(false);
              loadUserInfo(keyword, page, pageSize);
              openNotification('error', 'Delete account cloud failed');
            }
          )
        );
      },
    });
  };

  const handleEditCloudAccount = () => {
    subscriptions.push(
      editCloudAccount(editedCloudAccount._id, { name: editedCloudAccount.name }).subscribe(
        (data) => {
          const { pageSize, page } = userInfo;

          setLoading(false);
          setVisibleEditForm(false);
          loadUserInfo(keyword, page, pageSize);
          openNotification('success', 'Edit account cloud success');
        },
        () => {
          const { pageSize, page } = userInfo;

          setLoading(false);
          setVisibleEditForm(false);
          loadUserInfo(keyword, page, pageSize);
          openNotification('error', 'Edit account cloud failed');
        }
      )
    );
  };

  const handleCancelCloudAccount = () => {
    setVisibleEditForm(false);
    setEditedCloudAccount(null);
  };

  const showFormEditCloudAccount = (cloudAccount) => {
    setVisibleEditForm(true);
    setEditedCloudAccount(cloudAccount);
  };

  const handleChangeUserName = (e) => {
    const newCloudAccount = { ...editedCloudAccount, name: e.target.value };
    setEditedCloudAccount(newCloudAccount);
  };

  const _renderEditAccountForm = () => {
    return (
      <Modal
        title="Edit Cloud Account"
        visible={visibleEditForm}
        onOk={handleEditCloudAccount}
        onCancel={handleCancelCloudAccount}
        className="form-edited-cloud-account"
      >
        <Input addonBefore="Account Name" value={editedCloudAccount.name} onChange={handleChangeUserName} />
        <Input addonBefore="Cloud Account" value={editedCloudAccount.resourceName} disabled={true} />
        <Input addonBefore="Vendor" value={editedCloudAccount.vendor} disabled={true} />
        <Input
          addonBefore="Created Time"
          value={formatDateLocaleString(editedCloudAccount.createdTime)}
          disabled={true}
        />
        <Input addonBefore="Region" value={editedCloudAccount.region} disabled={true} />
      </Modal>
    );
  };

  const loadUserInfo = (keyword, page, pageSize) => {
    setLoading(true);
    subscriptions.push(
      getAccountsByUser(keyword, page, pageSize).subscribe(
        (data) => {
          if (data) {
            const { dataSources: dataSource, count: totalCount, page, size: pageSize } = data;
            const mapDataSources = dataSource
              ? dataSource.map((data, key) => {
                data.key = key;
                return data;
              })
              : [];
            setUserInfo({
              dataSource: mapDataSources,
              totalCount,
              page,
              pageSize,
            });
          }
        },
        () => {
          setLoading(false);
          openNotification('error', 'Loading user information list failed');
        },
        () => {
          setLoading(false);
        }
      )
    );
  };
  const onPageChange = (page) => {
    const { pageSize } = userInfo;
    const newPage = page > 0 ? page - 1 : 0;
    loadUserInfo(null, newPage, pageSize);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadUserInfo(null, 0, pageSize);
  };

  const onClickSearch = () => {
    const { pageSize } = userInfo;
    loadUserInfo(keyword, 0, pageSize);
  };

  const onReload = () => {
    loadUserInfo(null, 0, 10);
  };

  const keypressSearchAssessments = (e) => {
    const { pageSize } = userInfo;
    if (e.key === 'Enter') {
      loadUserInfo(keyword, 0, pageSize);
    }
  };

  const searchAssessments = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    setKeyword(keyword);
  };

  const buildTableuserInfo = () => {
    const { dataSource, totalCount, pageSize, page } = userInfo;
    const from = page * pageSize + 1;
    const pagination = {
      total: totalCount,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        title: '#',
        key: 'index',
        width: '5%',
        render: (text, record, index) => {
          return from + index;
        },
      },
      {
        title: 'Account Name',
        key: 'name',
        dataIndex: 'name',
        width: '20%',
        render: (text) => <>{text ? <span>{text}</span> : <i>Need update ...</i>}</>
      },
      {
        title: 'IAM',
        key: 'resourceName',
        dataIndex: 'resourceName',
        width: '20%',
      },
      {
        title: 'Vendor',
        key: 'vendor',
        dataIndex: 'vendor',
        width: '10%',
        render: (vendor) => {
          return vendor.toUpperCase();
        },
      },
      {
        title: 'Created Time',
        key: 'createdTime',
        dataIndex: 'createdTime',
        width: '10%',
        render: (text) => formatDateLocaleString(text),
      },
      {
        title: 'Region',
        key: 'region',
        dataIndex: 'region',
        width: '12%',
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
        width: '8%',
        render: (text, record, index) => {
          if (Boolean(record.active) === true) {
            return (
              <Tag color="green" style={{ width: 70, textAlign: 'center' }}>
                Active
              </Tag>
            );
          }
          return (
            <Tag color="orange" style={{ width: 70, textAlign: 'center' }}>
              Inactive
            </Tag>
          );
        },
      },
      {
        title: 'Action',
        key: 'action',
        dataIndex: 'status',
        fixed: 'right',
        render: (text, record, index) =>
          record.active && (
            <Space size="middle">
              <a href="#delete" onClick={() => handleDeleteAccount(record['_id'])}>
                Delete
              </a>
              <a href="#edit" onClick={() => showFormEditCloudAccount(record)}>
                Edit
              </a>
            </Space>
          ),
      },
    ];

    return (
      <>
        <Table
          className="gx-table-responsive"
          dataSource={dataSource}
          columns={columns}
          pagination={pagination}
          loading={loading}
        />
        {editedCloudAccount && _renderEditAccountForm()}
      </>
    );
  };

  return (
    <>
      <div className="gx-page-header">
        <Breadcrumbs list={[{ title: 'My Account', to: '/portals/accounts' }, { title: 'My Cloud Account' }]} />
        <div style={{ fontSize: '1.5rem', fontWeight: 'bold', lineHeight: 1.75, color: '#121619' }}>
          <span>My Cloud Account</span>
        </div>
      </div>
      <Card className="gx-card">
        <Row justify="end" align="top">
          <Col span={12}>
            <Space direction="horizontal" align="start" style={{float: 'right'}}>
              <Input
                type="text"
                onKeyPress={keypressSearchAssessments}
                onChange={searchAssessments}
                placeholder="Type keyword to type"
              />
              <Button shape="circle" icon={<SearchOutlined />} onClick={onClickSearch} />
              <Button shape="circle" icon={<RedoOutlined />} onClick={onReload} />
            </Space>
          </Col>
        </Row>
        <Divider />
        {buildTableuserInfo()}
      </Card>
    </>
  );
};

export default UserInfo;
