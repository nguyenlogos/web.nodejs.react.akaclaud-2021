import { RenderInput } from '@common/constants';
import { Col, Row } from 'antd';
import React, { Component } from 'react';

import CloudResource from './CloudResources';

class ResourceTemplate extends Component {
  constructor(props) {
    super(props);

    this.state = props.defaultValue
      ? { ...props.defaultValue }
      : {
        resourcePolicy: '',
      };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.validationInput) {
      const { updateOutputValidation } = this.props;
      const outputValidation = this.validateInput();
      updateOutputValidation(outputValidation);
    }

    if (nextProps.defaultValue) {
      this.setState({
        resourcePolicy: nextProps.defaultValue.resourcePolicy,
      });
    }
  }

  validateInput = () => {
    return this.state.resourcePolicy ? true : false;
  };

  clearInput = () => {
    this.setState({
      resourcePolicy: '',
    });
  };

  onLauchPolicy = (text) => {
    this.setState(
      {
        resourcePolicy: text,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { resourcePolicy } = this.state;

    return (
      <div className="lab-step-lauch-policy">
        <Row>
          <Col span={24}>
            <CloudResource
              renderInput={RenderInput.LAUNCH_POLICY}
              resourcePolicy={resourcePolicy}
              onPolicyChange={this.onLauchPolicy}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ResourceTemplate;
