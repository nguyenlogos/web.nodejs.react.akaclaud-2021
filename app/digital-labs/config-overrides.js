const path = require('path');
const { override, addLessLoader, addBabelPlugins, babelInclude } = require('customize-cra');
const { overrideProcessEnv } = require('octopus-cra-plugins');

const config = override(
  addLessLoader({
    javascriptEnabled: true,
  }),
  addBabelPlugins(
    [
      'module-resolver',
      {
        root: ["./src"],
        alias: {
          "@api": path.resolve(__dirname, 'src/api'),
          "@components": path.resolve(__dirname, 'src/shared/components'),
          "@containers": path.resolve(__dirname, 'src/shared/containers'),
          "@styles": path.resolve(__dirname, 'src/shared/styles'),
          "@assets": path.resolve(__dirname, 'src/shared/assets'),
          "@constants": path.resolve(__dirname, 'src/common/constants'),
          "@appRedux": path.resolve(__dirname, 'src/common/redux'),
          "@lngProvider": path.resolve(__dirname, 'src/common/lngProvider'),
          "@common": path.resolve(__dirname, 'src/common'),
          "@util": path.resolve(__dirname, 'src/common/util'),
          "@helpers": path.resolve(__dirname, 'src/common/helpers'),
          "@pages": path.resolve(__dirname, 'src/pages')
        }
      }
    ],
  ),
  ...addBabelPlugins('@babel/plugin-proposal-class-properties'),
  babelInclude([
    path.resolve(__dirname, 'src'),
    path.resolve(__dirname, 'node_modules/octopus-editor'),
    path.resolve(__dirname, 'node_modules/aka-keycloak-login'),
  ]),
  overrideProcessEnv()
);



module.exports = config;