import enLang from './en_US';
import viLang from './vi_VN';
import jaLang from './ja_JP';

const appLocale = {
  vi: viLang,
  en: enLang,
  ja: jaLang
};

export const SUPPORT_LANGUAGES = [
  {
    languageId: 'english',
    locale: 'en',
    name: 'English',
    icon: 'us'
  },
  {
    languageId: 'japanese',
    locale: 'ja',
    name: '日本人',
    icon: 'jp'
  },
  {
    languageId: 'vietnamese',
    locale: 'vi',
    name: 'Tiếng Việt',
    icon: 'vn'
  },
];

export default appLocale;