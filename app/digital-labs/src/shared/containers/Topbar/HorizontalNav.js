import { PUBLIC_URL } from '@common';
import IntlMessage from '@components/IntlMessage';
import {
  NAV_STYLE_ABOVE_HEADER,
  NAV_STYLE_BELOW_HEADER,
  NAV_STYLE_DEFAULT_HORIZONTAL,
  NAV_STYLE_INSIDE_HEADER_HORIZONTAL,
} from '@constants';
import { Menu } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const HorizontalNav = () => {
  const navStyle = useSelector(({ settings }) => settings.navStyle);
  const { pathTab } = useSelector(({ common, user }) => {
    const pathTab = '/' + common.pathname.split('/')[1];
    return { ...common, ...user, pathTab };
  });

  const getNavStyleSubMenuClass = (navStyle) => {
    switch (navStyle) {
      case NAV_STYLE_DEFAULT_HORIZONTAL:
        return 'gx-menu-horizontal gx-submenu-popup-curve';
      case NAV_STYLE_INSIDE_HEADER_HORIZONTAL:
        return 'gx-menu-horizontal gx-submenu-popup-curve gx-inside-submenu-popup-curve';
      case NAV_STYLE_BELOW_HEADER:
        return 'gx-menu-horizontal gx-submenu-popup-curve gx-below-submenu-popup-curve';
      case NAV_STYLE_ABOVE_HEADER:
        return 'gx-menu-horizontal gx-submenu-popup-curve gx-above-submenu-popup-curve';
      default:
        return 'gx-menu-horizontal';
    }
  };

  return (
    <Menu selectedKeys={[pathTab]} mode="horizontal">
      <Menu.Item className={getNavStyleSubMenuClass(navStyle)} key={`${PUBLIC_URL}/stories`}>
        <IntlMessage id="layout.header.menu.references_solutions" />
        <Link to={`${PUBLIC_URL}/stories`} />
      </Menu.Item>
      <Menu.Item className={getNavStyleSubMenuClass(navStyle)} key={`${PUBLIC_URL}/labs`}>
        <IntlMessage id="layout.header.menu.training_labs" />
        <Link to={`${PUBLIC_URL}/labs`} />
      </Menu.Item>
    </Menu>
  );
};

export default HorizontalNav;
