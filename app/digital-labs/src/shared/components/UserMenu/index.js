import { logOut } from '@common/authorization';
import { USER_MENU } from '@common/constants/Menu';
import SelectLanguages from '@components/SelectLanguages';
import { Avatar, Divider, Popover } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const UserMenu = ({ user }) => {
  const { roles, name: userName } = useSelector(({ user }) => user);
  const [matchingItems, setMatchingItems] = useState([]);
  const [visible, setProfileVisible] = useState(false);
  useEffect(() => {
    if (roles !== null) {
      const matchingRoles = roles.split(',');
      const matchingItems = USER_MENU.filter((item) =>
        item.acceptRoles.some((role) => matchingRoles.indexOf(role) >= 0)
      );
      setMatchingItems(matchingItems);
    }
  }, [roles]);

  const hide = () => {
    setProfileVisible(false);
  };

  const handleVisibleChange = (visible) => {
    setProfileVisible(visible);
  };

  const userMenuOptions = (
    <ul className="akac-user-popover">
      {matchingItems && matchingItems.map((item) => {
        return (
          <li className="akac-header-dropdown-menu " key={item.id}>
            <Link to={item.url} onClick={hide}>
              {item.text}
            </Link>
          </li>
        );
      })}

      <Divider className="akac-divider" />
      <li className="akac-header-dropdown-menu " key="select-langs">
        <SelectLanguages />
      </li>

      <Divider className="akac-divider" />
      <li onClick={logOut} className="akac-header-dropdown-menu ">
        Logout
      </li>
    </ul>
  );

  return (
    <div className="gx-flex-row gx-align-items-center gx-avatar-row">
      <Popover
        placement="bottomRight"
        content={userMenuOptions}
        trigger="click"
        visible={visible}
        onVisibleChange={handleVisibleChange}
      >
        <Avatar
          src={require('@assets/images/avatar/domnic-harris.png')}
          className="gx-size-30 gx-pointer gx-mr-3"
          alt=""
        />
        <span className="gx-avatar-name">
          {userName}
          {' '}
          <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
        </span>
      </Popover>
    </div>
  );
};

export default UserMenu;
