import { Col, Row } from 'antd';
import React, { Component } from 'react';

import HtmlAttachment from '../HtmlAttachment';

class BusinessCase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preRequisite: props.defaultValue ? props.defaultValue.preRequisite : null,
    };
  }

  validateInput = () => {
    return this.state.preRequisite ? true : false;
  };

  onPreRequisiteChange = (context) => {
    this.setState(
      {
        preRequisite: context,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { preRequisite } = this.state;
    return (
      <div className="lab-step-prerequisite">
        <Row>
          <Col span={24}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="guidance">
                  Business Story
                </label>
                <HtmlAttachment source={preRequisite} onBlur={this.onPreRequisiteChange} />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default BusinessCase;
