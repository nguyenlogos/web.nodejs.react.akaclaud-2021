import antdJa from 'antd/lib/locale-provider/ja_JP';

import translates from './Messages.json';

const jaLang = {
  messages: {
    ...translates
  },
  antd: antdJa,
  locale: 'ja-JP'
};
export default jaLang;
