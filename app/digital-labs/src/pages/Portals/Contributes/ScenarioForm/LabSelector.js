import { getAllReadyTrainingLab, getResourceUrl } from '@api';
import Catalog from '@pages/Common/LabCatalog';
import { Button, Col, Divider, Modal, Row } from 'antd';
import React, { useState } from 'react';
import { map } from 'rxjs/operators';

const LabSelector = ({ steps, setSteps, readOnly }) => {
  const [showModal, setShowModal] = useState(false);
  const [labs, setLabs] = useState([]);
  const [count, setCount] = useState(20);

  const addStep = (lab) => {
    if (!steps.find((step) => step._id === lab._id)) {
      setSteps([...steps, lab]);
    }
  };

  const removeStep = (lab) => {
    const nSteps = [...steps];
    var index = nSteps.indexOf(lab);
    if (index > -1) {
      nSteps.splice(index, 1);
      setSteps(nSteps);
    }
  };

  const changeStep = (lab1, lab2) => {
    const nSteps = [...steps];
    var index = nSteps.indexOf(lab1);
    if (index > -1) {
      nSteps[index] = lab2;
      setSteps(nSteps);
    }
  };

  const loadAllLabs = (keyword, page, pageSize, selectedItems) => {
    const filter = {};
    selectedItems.forEach((i) => {
      filter[i.key] = i.value;
    });
    getAllReadyTrainingLab(keyword, page, pageSize, filter)
      .pipe(
        map((docs) => {
          const count = docs.count;
          const labs = docs.dataSources
            ? docs.dataSources.map((lab, index) => {
              lab['key'] = index;
              return lab;
            })
            : [];

          return {
            count,
            labs,
          };
        })
      )
      .subscribe(({ count, labs }) => {
        setCount(count);
        setLabs(labs);
      });
  };
  return (
    <div className="scenario-lab-selector">
      <Row>
        <Col span="24">
          <div>
            <Divider />
            {steps &&
              steps.map((step, i) => (
                <div className="scenario-step" key={step._id}>
                  <div className="scenario-step-number">
                    Step
                    {i + 1}
                  </div>
                  <div className="scenario-step-img">
                    <img src={getResourceUrl(step.code, step.image)} alt="" />
                  </div>
                  <div className="scenario-step-card">
                    <h5>
                      {step.labName}
                    </h5>
                    <p className="gx-text-muted ">
                      {step.businessStory}
                    </p>
                    {!readOnly && (
                      <div className="scenario-step-card-actions gx-text-muted">
                        <span onClick={() => setShowModal(step)}>
                          Change
                        </span>
                        <span onClick={() => removeStep(step)}>
                          Remove
                        </span>
                      </div>
                    )}
                  </div>
                </div>
              ))}
          </div>
        </Col>
      </Row>
      {!readOnly && (
        <Row justify="start">
          <Col span="24">
            <Button size="small" onClick={() => setShowModal('add')}>
              Add Lab
            </Button>
            <span>
              Click here to add a lab to the scenario
            </span>
          </Col>
        </Row>
      )}
      <Modal
        visible={!!showModal}
        title="Select a lab"
        className="lab-selector-modal"
        width="80vw"
        onOk={() => {
          setShowModal(false);
        }}
        onCancel={() => setShowModal(false)}
      >
        <Catalog
          className="mt-10"
          labs={labs}
          count={count}
          getLabs={loadAllLabs}
          selectedItems={[]}
          disableHover={true}
          onClick={(lab) => {
            if (showModal === 'add') {
              addStep(lab);
            } else {
              changeStep(showModal, lab);
            }
            setShowModal(false);
          }}
        />
      </Modal>
    </div>
  );
};

export default LabSelector;
