import { Row } from 'antd';
import * as React from 'react';

import StrategyChart from './StrategyChart';

const StrategyRecommendation = (props) => {
  const { report, title, recommendationIndex, recommendation } = props;
  const recommendDetail = recommendation.recommendDetail.split('|*');
  let alternativeOption;
  report.forEach((reportItem) => {
    alternativeOption = reportItem.readiness[recommendation.name];
  });
  return (
    <div className="recommendation-report_strategy">
      <hr className="line" style={{ display: recommendationIndex === 0 ? 'none' : 'block' }} />
      <div
        className={
          'recommendation-report_strategy_container ' +
          (alternativeOption.finalOption === 1 ? 'recommendation-report_strategy_final' : '')
        }
      >
        <div id={recommendation.name}>
          <div className="recommendation-report_strategy_content">
            <div className="recommendation-title_wrapper">
              {alternativeOption.finalOption === 1 ? (
                <Row justify="space-between">
                  <div className="recommendation_title space">
                    {recommendationIndex + 1}.{recommendation.name}
                  </div>
                  <div>
                    <div className="recommond_solution">
                      <img alt="" src="/icons/ios-star.svg" />
                      <span>Recommended solution</span>
                    </div>
                  </div>
                </Row>
              ) : (
                <div className="recommendation_title">
                  {recommendationIndex + 1}.{recommendation.name}
                </div>
              )}
            </div>
            <div className="decision">
              {recommendDetail && recommendDetail.map((recommendationDetail, recommendationIndex) => (
                <p className="recommendation_content" key={recommendationIndex}>
                  {recommendationDetail}
                </p>
              ))}
            </div>
          </div>
        </div>
        <StrategyChart questions={report} title={title} decision={recommendation} />
      </div>
    </div>
  );
};
export default StrategyRecommendation;
