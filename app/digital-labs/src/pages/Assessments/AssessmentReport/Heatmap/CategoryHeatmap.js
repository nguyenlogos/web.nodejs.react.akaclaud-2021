import * as React from 'react';

const CategoryHeatmap = (props) => {
  const { category, readiness } = props;
  const arrayReport = Object.entries(readiness);
  return (
    <div className="title-subdomain">
      <hr />
      <div className="heatmap-domain-title">{category}</div>
      {arrayReport && arrayReport.map((e, index) => {
        const subDomainName = e[0];
        const value = e[1];
        const percent = parseInt(value);
        const classNamesHeatmap = percent >= 50 ? 'process-heatmap-match' : 'process-heatmap-less';
        return (
          <div className={classNamesHeatmap} key={index}>
            {subDomainName}
          </div>
        );
      })}
    </div>
  );
};
export default CategoryHeatmap;
