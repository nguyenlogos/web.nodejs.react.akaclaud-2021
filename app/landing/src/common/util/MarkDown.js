import React, { Component } from "react";
import { MarkdownPreview } from "react-marked-markdown";

class Markdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: props.src,
    };
  }

  render() {
    const { content } = this.state;
    const { options } = this.props;

    return (
      <div className="md-viewer">
        <MarkdownPreview value={content} options={options} />
      </div>
    );
  }
}

export default Markdown;
