import { ArrowRightOutlined } from '@ant-design/icons';
import { RecommendAction } from '@constants';
import * as React from 'react';

const CategoryRecommendation = (props) => {
  const { recommend, name } = props;
  if (recommend && recommend.length > 0) {
    const recommendDomain = recommend.find((item) => item.domain.trim() === name.trim());
    const recommendDetail = recommendDomain ? recommendDomain.recommendDetail.split('*|') : [];

    if (recommendDetail && recommendDetail.length > 0) {
      return (
        <div className="tooltip-content">
          <div className="tooltip-header">
            <span className="tooltip-title">{recommendDomain.action}</span>
            <p className="tooltip-main-content">{recommendDomain.recommendOverview}</p>
          </div>
          {recommendDetail.map((item, index) => {
            const Recommend = item.split('*:');
            if (recommendDomain.action === RecommendAction.Congratulations) {
              return (
                <div key={index} className="action-pass-Detail">
                  {item}
                </div>
              );
            } else {
              return (
                <div key={index} className="recommend-detail">
                  <hr />
                  <div className="tooltip-details">
                    <div className="re-domain">{Recommend[0]}</div>
                    <div className="recommend-content">{Recommend[1]}</div>
                  </div>
                </div>
              );
            }
          })}
          <div className="tooltip-footer">
            <span>
              Here is a helpful video to assist you with building a business case:
              <br />
              <a className="help-link" href="#help-link">
                Building a Solid Business Case for CloudMigration <ArrowRightOutlined />
              </a>
            </span>
          </div>
        </div>
      );
    }
  }

  return <></>;
};
export default CategoryRecommendation;
