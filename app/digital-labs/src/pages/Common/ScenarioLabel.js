import React from 'react';

const zeroPadding = (i) => (i <= 9 ? '0' + i : i);

const ScenarioLabel = ({ index }) => (
  <div className="step-label">
    <div>
      STEP
    </div>
    <div className="step-label-index">
      {zeroPadding(index)}
    </div>
  </div>
);

export default ScenarioLabel;
