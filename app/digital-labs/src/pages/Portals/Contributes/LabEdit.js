import { getContentGit, getLab, getResourceUrl, storeDocuments, updateLab } from '@api';
import { createTextFile, openNotification } from '@common/helpers';
import { DigitalLabTypeValue } from '@constants';
import { Modal } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { forkJoin, of } from 'rxjs';
import { concatMap } from 'rxjs/operators';

import BreadcumbLab from './BreadcumbLab';
import BusinessCase from './Steps/BusinessCase';
import CloudResource from './Steps/CloudResources';
import GuideLine from './Steps/GuideLine';
import LabOverview from './Steps/LabOverview';
import Solution from './Steps/Solution';
import InputTabPanel from './TabPanel';

const { confirm } = Modal;

const LabEdit = () => {
  let labCommon, labCloudTemplate, labBusinessCase, labSolution, labCode, labGuideLine;
  labCommon = labCloudTemplate = labBusinessCase = labSolution = labGuideLine = labCode = null;

  let isDirty = false;
  const $labCommonRef = useRef(null);
  const $labCloudTemplateRef = useRef(null);
  const $labBusinessCaseRef = useRef(null);
  const $labGuideLineRef = useRef(null);
  const $labSolutionRef = useRef(null);

  const [labEditInfo, setLabEditInfo] = useState(null);

  const history = useHistory();
  const params = useParams();
  const labId = params.id;

  const subscriptions = [];

  const fnEffectHook = () => {
    getLabDetail();
    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const tabs = [
    {
      index: 0,
      title: '1. Lab Overview',
      validate: () => {
        if ($labCommonRef.current == null) {
          return true;
        }

        return $labCommonRef.current.validateInput();
      },
      template: () => (
        <LabOverview
          defaultValue={labEditInfo.labCommon}
          ref={$labCommonRef}
          onChange={(common) => {
            isDirty = true;
            labCommon = common;
          }}
        />
      ),
    },
    {
      index: 1,
      title: '2. Resource Template',
      validate: () => {
        if ($labCloudTemplateRef.current == null) {
          return true;
        }

        return $labCloudTemplateRef.current.validateInput();
      },
      template: () => (
        <CloudResource
          defaultValue={labEditInfo.labCloudTemplate}
          ref={$labCloudTemplateRef}
          onChange={(value) => {
            isDirty = true;
            labCloudTemplate = value;
          }}
        />
      ),
    },
    {
      index: 2,
      title: '3. Business Story',
      validate: () => {
        if ($labBusinessCaseRef.current == null) {
          return true;
        }

        return $labBusinessCaseRef.current.validateInput();
      },
      template: () => (
        <BusinessCase
          defaultValue={labEditInfo.labBusinessCase}
          ref={$labBusinessCaseRef}
          onChange={(value) => {
            isDirty = true;
            labBusinessCase = value;
          }}
        />
      ),
    },
    {
      index: 3,
      title: '4. GuideLine',
      validate: () => {
        if ($labGuideLineRef.current == null) {
          return true;
        }

        if (labCommon && labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions) {
          return true;
        }

        if (labEditInfo.labCommon && labEditInfo.labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions) {
          return true;
        }

        return $labGuideLineRef.current.validateInput();
      },
      template: () => (
        <GuideLine
          defaultValue={labEditInfo.labGuideLine}
          ref={$labGuideLineRef}
          onChange={(value) => {
            isDirty = true;
            labGuideLine = value;
          }}
        />
      ),
    },
    {
      index: 4,
      title: '5. Solution',
      validate: () => {
        if ($labSolutionRef.current == null) {
          return true;
        }

        if (
          labCommon &&
          (labCommon.digitalLabType === DigitalLabTypeValue.Training ||
            labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions)
        ) {
          return true;
        }

        if (
          labEditInfo.labCommon &&
          (labEditInfo.labCommon.digitalLabType === DigitalLabTypeValue.Training ||
            labEditInfo.labCommon.digitalLabType === DigitalLabTypeValue.ReferencesSolutions)
        ) {
          return true;
        }

        return $labSolutionRef.current.validateInput();
      },
      template: () => (
        <Solution
          defaultValue={labEditInfo.labSolution}
          ref={$labSolutionRef}
          onChange={(value) => {
            isDirty = true;
            labSolution = value;
          }}
        />
      ),
    },
  ];

  const fileAndContentType = (resource, nameFile) => {
    let type = '';
    try {
      JSON.parse(resource);
      type = '.json';
    } catch {
      type = '.yaml';
    }

    return nameFile + type;
  };

  const uploadLabResource = (labCode) => {
    const { resourceParameter, resourceTemplate, resourcePolicy } = labCloudTemplate || labEditInfo.labCloudTemplate;
    const { image, attachments } = labCommon || labEditInfo.labCommon;

    let guidline, preRequisite, solution;
    guidline = preRequisite = solution = null;

    guidline = labGuideLine ? labGuideLine.guidline : labEditInfo.labGuideLine.guidline;
    preRequisite = labBusinessCase ? labBusinessCase.preRequisite : labEditInfo.labBusinessCase.preRequisite;
    solution = labSolution ? labSolution.solution : labEditInfo.labSolution.solution;

    const data = new FormData();
    let needUpload = false;

    if (image) {
      data.append('intro-image.png', image);
      needUpload = true;
    }

    if (guidline) {
      if (typeof guidline != 'object') {
        let file = createTextFile('guidance-sub-tab.md', guidline + '', 'text/plain');
        data.append('guidance-sub-tab.md', file);
      } else {
        data.append('guidance-sub-tab.md', guidline);
      }
      needUpload = true;
    }

    if (preRequisite) {
      if (typeof preRequisite != 'object') {
        let file = createTextFile('pre-lab-sub-tab.md', preRequisite + '', 'text/plain');
        data.append('pre-lab-sub-tab.md', file);
      } else {
        data.append('pre-lab-sub-tab.md', preRequisite);
      }
      needUpload = true;
    }

    if (solution) {
      if (typeof solution != 'object') {
        let file = createTextFile('solution-sub-tab.md', solution + '', 'text/plain');
        data.append('solution-sub-tab.md', file);
      } else {
        data.append('solution-sub-tab.md', solution);
      }
      needUpload = true;
    }

    if (resourceTemplate) {
      let content = typeof resourceTemplate == 'object' ? JSON.stringify(resourceTemplate) : resourceTemplate;
      let type = '';
      try {
        JSON.parse(resourceTemplate);
        type = 'json';
      } catch {
        type = 'yaml';
      }
      const fileName = 'resource-template.' + type;
      let file = createTextFile(fileName, content, type);
      data.append(fileName, file);
      needUpload = true;
    }

    if (resourceParameter) {
      let content = typeof resourceParameter == 'object' ? JSON.stringify(resourceParameter) : resourceParameter;
      let type = '';
      try {
        JSON.parse(resourceParameter);
        type = 'json';
      } catch {
        type = 'yaml';
      }
      const fileName = 'param-template.' + type;
      let file = createTextFile(fileName, content, type);
      data.append(fileName, file);
      needUpload = true;
    }

    if (resourcePolicy) {
      let file = createTextFile('launch-policy.json', resourcePolicy, 'json');
      data.append('launch-policy.json', file);
      needUpload = true;
    }

    if (attachments && attachments.length > 0) {
      attachments.forEach((f) => {
        data.append(f.name, f.originFileObj);
      });

      needUpload = true;
    }

    if (needUpload) {
      return storeDocuments(data, labCode);
    }

    return of([]);
  };

  const getLabDetail = () => {
    subscriptions.push(
      getLab(labId).subscribe(
        (lab) => {
          const parameters = lab.parameters.reduce((prev, curr) => {
            prev[curr.key] = curr.value;

            return prev;
          }, {});

          labCommon = {
            labName: lab.labName,
            description: lab.description,
            businessStory: lab.businessStory,
            childCategoryId: lab.childCategoryId,
            complexityId: lab.complexityId,
            vendor: lab.vendor,
            timeInMinutes: lab.timeInMinutes,
            estimateCost: lab.estimateCost,
            notes: lab.notes,
            rating: lab.rating,
            digitalLabType: lab.digitalLabType ? lab.digitalLabType : null,
            attachments: lab.attachments
              ? lab.attachments.map((f, i) => {
                return {
                  uid: i,
                  name: f,
                  status: 'done',
                };
              })
              : [],
            image: getResourceUrl(lab.code, lab.image),
          };
          setLabEditInfo({ labCommon: labCommon });
          labCode = lab.code;
          labCloudTemplate = {
            resourceParameter: JSON.stringify(parameters),
          };
          forkJoin(
            getContentGit(lab.code, lab.preLabSubTabURL ? lab.preLabSubTabURL : ''),
            getContentGit(lab.code, lab.guidanceSubTabURL ? lab.guidanceSubTabURL : ''),
            getContentGit(lab.code, lab.cloudformationURL ? lab.cloudformationURL : ''),
            getContentGit(lab.code, lab.launchPolicy ? lab.launchPolicy : ''),
            getContentGit(lab.code, lab.solutionURL ? lab.solutionURL : '')
          ).subscribe(
            ([preText, guidlineText, resourceTemplateText, launchPolicyText, solutionText]) => {
              labCloudTemplate.resourceTemplate =
                typeof resourceTemplateText === 'object'
                  ? JSON.stringify(resourceTemplateText, null, 2)
                  : resourceTemplateText + '';
              labCloudTemplate.resourcePolicy =
                typeof launchPolicyText == 'object' ? JSON.stringify(launchPolicyText, null, 2) : launchPolicyText + '';

              labSolution = {
                solution: solutionText ? solutionText + '' : '',
              };
              labBusinessCase = {
                preRequisite: preText ? preText + '' : '',
              };
              labGuideLine = {
                guidline: guidlineText ? guidlineText + '' : '',
              };
              setLabEditInfo({
                labCommon: labCommon,
                labCloudTemplate: labCloudTemplate,
                labSolution: labSolution,
                labBusinessCase: labBusinessCase,
                labGuideLine: labGuideLine,
              });
            },
            (err) => {
              openNotification('error', 'Lab detail files not found!');
            }
          );
        },
        (err) => {
          openNotification('error', 'Lab not found!');
        }
      )
    );
  };

  const updatetNewLab = () => {
    const {
      labName,
      description,
      businessStory,
      timeInMinutes,
      childCategoryId,
      complexityId,
      vendor,
      estimateCost,
      notes,
      attachments,
      digitalLabType,
      rating,
    } = labCommon || labEditInfo.labCommon;

    const { resourceParameter, resourceTemplate } = labCloudTemplate || labEditInfo.labCloudTemplate;

    const attachmentNames = attachments
      ? attachments.map((f) => {
        return f.name;
      })
      : [];

    const updatedLab = {
      labName,
      code: labCode,
      description,
      businessStory,
      parameters: resourceParameter,
      timeInMinutes,
      childCategoryId,
      complexityId,
      vendor,
      estimateCost,
      notes,
      attachments: attachmentNames,
      digitalLabType,
      rating,
      cloudformationURL: fileAndContentType(resourceTemplate, 'resource-template'),
      resourceParameterURL: fileAndContentType(resourceParameter, 'param-template'),
      preLabSubTabURL: labBusinessCase || labEditInfo.labBusinessCase.preRequisite ? 'pre-lab-sub-tab.md' : null,
      solutionURL: labSolution || labEditInfo.labSolution.solution ? 'solution-sub-tab.md' : null,
      guidanceSubTabURL: labGuideLine || labEditInfo.labGuideLine.guidline ? 'guidance-sub-tab.md' : null,
    };

    return updateLab(labId, updatedLab);
  };

  const onSubmit = () => {
    confirm({
      title: 'Did you review this lab before submit?',
      okText: 'Submit',
      okType: 'primary',
      cancelText: 'Review again',
      onOk: () => {
        if (isDirty) {
          subscriptions.push(
            updatetNewLab()
              .pipe(
                concatMap((lab) => {
                  return uploadLabResource(lab.code);
                })
              )
              .subscribe(
                (code) => {
                  openNotification('success', 'Lab edit successfully');
                  const locationBack =
                    history.location.state && history.location.state === 'Lab'
                      ? '/portals/contributes/labs'
                      : '/portals/contributes/labs/all';
                  history.push(locationBack);
                },
                (error) => {
                  openNotification('error', 'Edit lab error');
                }
              )
          );
        } else {
          openNotification('error', 'You should edit some fields');
        }
      },
    });
  };

  return (
    <div className="contribute-lab">
      <BreadcumbLab title="Edit lab" />

      <div className="create-lab">
        {labEditInfo && <InputTabPanel tabs={tabs} onCompleted={onSubmit} />}
      </div>
    </div>
  );
};

export default LabEdit;
