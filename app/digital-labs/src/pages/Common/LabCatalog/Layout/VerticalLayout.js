import { getResourceUrl } from '@api';
import awsThumbnail from '@assets/images/thumbnails/aws-thumbnail.png';
import azureThumbnail from '@assets/images/thumbnails/azure-thumbnail.png';
import googleThumbnail from '@assets/images/thumbnails/gcp-thumbnail.png';
import { CloudProvider } from '@constants';
import { shortDescription } from '@helpers';
import { Col, Image } from 'antd';
import React from 'react';

const getDefaultThumbnailUrl = (vendor) => {
  if (vendor === CloudProvider.Aws) {
    return awsThumbnail;
  }

  if (vendor === CloudProvider.Azure) {
    return azureThumbnail;
  }

  return googleThumbnail;
};

const VerticalLayout = ({ labItem, onClick }) => {
  labItem.businessStory = labItem.businessStory.replace(/<p>|<\/p>/gi, '');
  const { labName, credit, vendor, businessStory, timeInMinutes, image, code, complexityId } = labItem;
  return (
    <Col xl={24} lg={24} md={24} sm={24} xs={24}>
      <div className="gx-product-item  gx-product-horizontal lab-card-vetical" onClick={() => onClick(labItem)}>
        <div className="gx-product-image">
          <div className="gx-grid-thumb-equal lab-card-vetical-thumb-equal">
            <span className="gx-link gx-grid-thumb-cover">
              <Image src={getResourceUrl(code, image)} fallback={getDefaultThumbnailUrl(vendor)} />
            </span>
          </div>
        </div>
        <div className="gx-product-body lab-card-vetical-body">
          <h4 className="gx-product-title">
            {labName}
          </h4>
          <div className="ant-row-flex lab-card-vetical-lab-info">
            <h5 className="gx-text-muted ">
              {complexityId}
            </h5>
            <h5 className="gx-text-muted ">
              {credit}
            </h5>
            <h5 className="gx-text-success">
              {vendor}
            </h5>
            <h5 className="gx-text-muted">
              {timeInMinutes}
              {' '}
              Minutes
            </h5>
          </div>
          <p>
            {shortDescription(businessStory, 200)}
          </p>
        </div>
      </div>
    </Col>
  );
};

export default VerticalLayout;
