import { CloudFilled, CloudServerOutlined, DollarCircleFilled, ProfileOutlined, TagsOutlined } from '@ant-design/icons';
import React from 'react';
import {
  PUBLIC_URL,
  REACT_APP_OPERATION_URL,
  REACT_APP_SECURITY_URL,
  // REACT_APP_COST_URL,
  // REACT_APP_CLOUD_URL,
} from '@common';

export const ICON_NAVIGATION = [
  {
    id: 'credits',
    text: 'My Credits',
    url: '/portals/accounts/my-credits',
    icon: <DollarCircleFilled style={{ fontSize: '0.9em' }} />,
    acceptRoles: 'trainee',
  },
  {
    id: 'accounts',
    text: 'My Cloud Accounts',
    url: '/portals/accounts',
    icon: <CloudFilled style={{ fontSize: '0.9em' }} />,
    acceptRoles: 'trainee',
  },
];

export const APPS_NAVIGATION = [
  {
    id: 'operation',
    text: 'Cloud Operation',
    url: REACT_APP_OPERATION_URL,
  },
  {
    id: 'security',
    text: 'Security Assurance',
    url: REACT_APP_SECURITY_URL,
  }
];

export const SIDEBAR_MENU = [
  {
    id: 'Account',
    text: 'My Account',
    icon: <ProfileOutlined style={{ fontSize: '1.25em' }} />,
    children: [
      {
        text: 'Cloud Accounts',
        url: '/portals/accounts',
        roles: ['leader', 'seller', 'trainee', 'contributor'],
      },
      {
        text: 'Credit History',
        url: '/portals/accounts/my-credits',
        roles: ['leader', 'seller', 'trainee', 'contributor'],
      },
      {
        text: 'Credit Management',
        url: '/portals/admin/credit-management',
        roles: ['admin'],
      },
    ],
    acceptRoles: ['admin', 'leader', 'seller', 'trainee', 'contributor'],
  },
  {
    id: 'Lab',
    text: 'Digital Lab',
    icon: <CloudServerOutlined style={{ fontSize: '1.25em' }} />,
    children: [
      {
        text: 'My Learning',
        url: '/portals/labs/stacks',
        roles: ['seller', 'trainee'],
      },
      // {
      //   text: "My Infrastructure",
      //   url: "/labs/my-labs",
      //   roles: ["leader", "seller", "trainee", "contributor"]
      // }
    ],
    acceptRoles: ['leader', 'seller', 'trainee', 'contributor'],
  },
  {
    id: 'Contribute',
    text: 'Contribution',
    icon: <TagsOutlined style={{ fontSize: '1.25em' }} />,
    children: [
      {
        text: 'All Lab',
        url: '/portals/contributes/labs/all',
        roles: ['leader'],
      },
      {
        text: 'My Labs',
        url: '/portals/contributes/labs',
        roles: ['contributor'],
      },
      {
        text: 'Create Lab',
        url: '/portals/contributes/labs/create',
        roles: ['contributor'],
      },
    ],
    acceptRoles: ['leader', 'contributor'],
  },
];

export const USER_MENU = [
  {
    id: 'account',
    text: 'My Cloud Account',
    url: `${PUBLIC_URL}/portals/accounts`,
    acceptRoles: ['leader', 'seller', 'trainee', 'contributor'],
  },
  {
    id: 'onboarding',
    text: 'Account Onboarding',
    url: `${PUBLIC_URL}/accounts/onboarding`,
    acceptRoles: ['seller', 'trainee'],
  },
  {
    id: 'stories',
    text: 'Reference Solutions',
    url: `${PUBLIC_URL}/stories`,
    acceptRoles: ['leader', 'seller', 'contributor'],
  },
  {
    id: 'labs',
    text: 'Training Labs',
    url: `${PUBLIC_URL}/labs`,
    acceptRoles: ['leader', 'seller', 'trainee', 'contributor'],
  },
];
