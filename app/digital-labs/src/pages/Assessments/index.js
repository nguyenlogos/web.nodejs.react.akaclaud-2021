import asyncComponent from '@components/AsyncComponent';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const AccountApp = ({ prefixRoute }) => {
  return (
    <>
      <Switch>
        <Route path={`${prefixRoute}`} exact={true} component={asyncComponent(() => import('./Assessments'))} />
        <Route
          path={`${prefixRoute}/reports/:id`}
          exact={true}
          component={asyncComponent(() => import('./AssessmentReport'))}
        />
        <Route path={`${prefixRoute}/do/:id`} exact={true} component={asyncComponent(() => import('./Questionaire'))} />
      </Switch>
    </>
  );
};

export default AccountApp;
