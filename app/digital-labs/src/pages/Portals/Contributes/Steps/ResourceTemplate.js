import { RenderInput } from '@common/constants';
import { Col, Row } from 'antd';
import React, { Component } from 'react';

import CloudResource from './CloudResources';

class ResourceTemplate extends Component {
  constructor(props) {
    super(props);

    this.state = props.defaultValue
      ? { ...props.defaultValue }
      : {
        resourceTemplate: '',
      };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.validationInput) {
      const { updateOutputValidation } = this.props;
      const outputValidation = this.validateInput();
      updateOutputValidation(outputValidation);
    }

    if (nextProps.defaultValue) {
      this.setState({
        resourceTemplate: nextProps.defaultValue.resourceTemplate,
      });
    }
  }

  validateInput = () => {
    return this.state.resourceTemplate ? true : false;
  };

  clearInput = () => {
    this.setState({
      resourceTemplate: '',
    });
  };

  onTemplateChange = (text) => {
    this.setState(
      {
        resourceTemplate: text,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { resourceTemplate } = this.state;

    return (
      <div className="lab-step-resource-template">
        <Row>
          <Col span={24}>
            <CloudResource
              renderInput={RenderInput.RESOURCE_TEMPLATE}
              resourceTemplate={resourceTemplate}
              onTemplateChange={this.onTemplateChange}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ResourceTemplate;
