import antdEn from 'antd/lib/locale-provider/en_US';

import translates from './Messages.json';

const enLang = {
  messages: {
    ...translates
  },
  antd: antdEn,
  locale: 'en-US'
};
export default enLang;
