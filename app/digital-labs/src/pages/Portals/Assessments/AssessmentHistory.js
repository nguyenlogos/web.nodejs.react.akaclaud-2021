import { ForwardOutlined, PlusOutlined, RedoOutlined, SearchOutlined } from '@ant-design/icons';
import { getAllAssessmentByUser, getAssessmentTemplate, startAssessment } from '@api';
import { formatDateLocaleString, openNotification } from '@common/helpers';
import { Button, Card, Col, Input, Modal, Row, Select, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE } from '@constants';

const Assessments = () => {
  const [assessmentInfo, setAssessmentInfo] = useState({
    dataSource: [],
    totalCount: 0,
    page: DEFAULT_ADMIN_PAGE_INDEX,
    pageSize: DEFAULT_ADMIN_PAGE_SIZE,
  });
  const [assessmentTemplate, setAssessmentTemplate] = useState([]);
  const [assessmentTemplateID, setAssessmentTemplateID] = useState('');
  const [loading, setLoading] = useState(false);
  const [visibleModal, setVisibleModal] = useState(false);
  const [keyword, setKeyword] = useState('');
  const history = useHistory();
  const subscriptions = [];

  const fnEffectHook = () => {
    loadAssessments(keyword, DEFAULT_ADMIN_PAGE_INDEX, DEFAULT_ADMIN_PAGE_SIZE);
    subscriptions.push(
      getAssessmentTemplate(null, 0, 100).subscribe((templ) => {
        let result = templ.dataSources;
        setAssessmentTemplate(result);
      })
    );
    return () => {
      subscriptions.forEach((subscription) => subscription.unsubscribe());
    };
  };
  useEffect(fnEffectHook, []);

  const loadAssessments = (keyword, page, pageSize) => {
    setLoading(true);
    subscriptions.push(
      getAllAssessmentByUser(keyword, page, pageSize).subscribe(
        (data) => {
          if (data) {
            const { dataSources: dataSource, count: totalCount, page, size: pageSize } = data;
            const mapDataSources = dataSource
              ? dataSource.map((data, key) => {
                data.key = key;
                return data;
              })
              : [];
            setAssessmentInfo({
              dataSource: mapDataSources,
              totalCount,
              page,
              pageSize,
            });
          }
        },
        () => {
          setLoading(false);
          openNotification('error', 'Loading assessment list failed');
        },
        () => {
          setLoading(false);
        }
      )
    );
  };

  const onPageChange = (page) => {
    const { pageSize } = assessmentInfo;
    const newPage = page > 0 ? page - 1 : 0;
    loadAssessments(null, newPage, pageSize);
  };

  const onShowSizeChange = (current, pageSize) => {
    loadAssessments(null, 0, pageSize);
  };

  const navigateDetail = (AsssessmentID) => {
    history.push('/assessments/do/' + AsssessmentID);
  };

  const searchAssessments = (e) => {
    let keyword = e.target.value;
    keyword = keyword.trim();
    setKeyword(keyword);
  };

  const onClickSearch = () => {
    const { pageSize } = assessmentInfo;
    loadAssessments(keyword, 0, pageSize);
  };

  const onReload = () => {
    loadAssessments(null, 0, 10);
  };

  const keypressSearchAssessments = (e) => {
    const { pageSize } = assessmentInfo;
    if (e.key === 'Enter') {
      loadAssessments(keyword, 0, pageSize);
    }
  };

  const showModal = () => {
    setVisibleModal(true);
  };

  const handleModalCancel = () => {
    setVisibleModal(false);
  };

  const handleModalOk = () => {
    setVisibleModal(false);
    const assessmentTemlObj = assessmentTemplate.find((tem) => tem._id === assessmentTemplateID);
    startAssessment(assessmentTemplateID, {
      customerName: assessmentTemlObj.customerName,
      assessmentName: assessmentTemlObj.assessmentName,
    }).subscribe(
      (res) => {
        openNotification('success', 'Assessment is created!');
        navigateDetail(res.assessment);
      },
      (error) => {
        openNotification('error', 'Error occured');
      }
    );
  };

  const handleChangeSelect = (value) => {
    setAssessmentTemplateID(value);
  };

  const buildTableAssessment = () => {
    const { dataSource, totalCount, pageSize, page } = assessmentInfo;
    const from = page * pageSize + 1;
    const pagination = {
      total: totalCount,
      pageSize,
      current: page + 1,
      onChange: onPageChange,
      showSizeChanger: true,
      pageSizeOptions: ['10', '20', '50', '100'],
      onShowSizeChange: onShowSizeChange,
    };

    const columns = [
      {
        title: '#',
        key: 'index',
        width: 60,
        render: (text, record, index) => {
          return (<span>
            {from + index}
          </span>);
        },
      },
      {
        title: 'Assessment name',
        key: 'name',
        dataIndex: 'assessmentName',
      },
      {
        title: 'Assessment name',
        key: 'account',
        dataIndex: 'userId',
      },
      {
        title: 'Last updated time',
        key: 'modified-time',
        dataIndex: 'updatedTime',
        render: (text) => {
          const timeStr = formatDateLocaleString(text);
          return timeStr;
        },
      },
      {
        title: 'Quick action',
        key: 'actions',
        render: (record) => {
          return (
            <Button type="primary" icon={<ForwardOutlined />} size="default" onClick={() => navigateDetail(record._id)}>
              Continue
            </Button>
          );
        },
      },
    ];

    return (
      <Table
        className="gx-table-responsive"
        dataSource={dataSource}
        columns={columns}
        pagination={pagination}
        loading={loading}
        scroll={{ y: 350 }}
      />
    );
  };

  const buildSelectAssessmentTemplate = () => {
    const options = assessmentTemplate
      ? assessmentTemplate.map((temp) => {
        return (
          <Select.Option value={temp._id} key={temp._id}>
            {temp.assessmentName}
          </Select.Option>
        );
      })
      : [];

    return (
      <Select style={{ width: '80%' }} placeholder="Please select an assessment" onChange={handleChangeSelect}>
        {options}
      </Select>
    );
  };

  return (
    <Row>
      <Col span={24}>
        <Button icon={<PlusOutlined />} onClick={showModal}>
          Start New Assessment
        </Button>
      </Col>
      <Modal title="Assessment Name" visible={visibleModal} onOk={handleModalOk} onCancel={handleModalCancel}>
        <Row justify="center">
          {buildSelectAssessmentTemplate()}
        </Row>
      </Modal>

      <Col span={24}>
        <Card title="Assessment List">
          <Space direction="vertical">
            <Row>
              <Col span={10}>
                <Input
                  type="text"
                  onKeyPress={keypressSearchAssessments}
                  onChange={searchAssessments}
                  placeholder="type keyword to type"
                />
              </Col>
              <Col span={6}>
                <Button shape="circle" icon={<SearchOutlined />} onClick={onClickSearch} />

                <Button shape="circle" icon={<RedoOutlined />} onClick={onReload} />
              </Col>
            </Row>

            {buildTableAssessment()}
          </Space>
        </Card>
      </Col>
    </Row>
  );
};

export default Assessments;
