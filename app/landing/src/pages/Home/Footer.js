import React from "react";
import { Col, Row } from "antd";
import { LANDING_FOOTER_1, LANDING_FOOTER_2, LANDING_FOOTER_3, LANDING_FOOTER_ICONS } from "@constants/Landing"

const Footer = () => (<div className="footer">
    <Row justify="center" className="web-layout">
        <Col md={24} xl={20} className="gx-px-md-5 gx-px-xl-3">
            <Row>
                <Col md={8}>
                    <div className="temp-footer-layout">
                        <img src="/logo.svg" alt="akaClaud" className="footer-logo-aka" />
                        <span>
                            <span className="product-by">A PRODUCT BY</span>
                            <img src="/landing/logo-FSOFT.svg" alt="FPT Software" className="footer-logo-fsoft" />
                        </span>
                    </div>
                </Col>
                <Col span="16">
                    <div className="footer-section">
                        <h6><a href="/">HOME</a></h6>
                        <h6><a href="https://www.fpt-software.com/service/cloud-professional-services/">ABOUT US</a></h6>
                        <h6><a href="https://www.fpt-software.com/">COMPANY</a></h6>
                        <h6><a href={process.env.REACT_APP_DIGITAL_LABS}>DIGITAL LAB</a></h6>
                        <h6><a href={process.env.REACT_APP_CLOUD_OPERATION}>OPERATION</a></h6>
                        <h6><a href="https://www.fpt-software.com/contact-us/">CONTACT US</a></h6>
                    </div>
                </Col>
            </Row>
        </Col>
    </Row>
    <div className="mobile-layout footer-container">
        <Row className="gx-pb-3">
            <Col span={10}>
                <img src="/logo.svg" alt="akaClaud" className="footer-logo-aka" />
            </Col>
            <Col span={14}>
                <span>
                    <span className="product-by">A PRODUCT BY</span>
                    <img src="/landing/logo-FSOFT.svg" alt="FPT Software" className="footer-logo-fsoft" />
                </span>
            </Col>
        </Row>
        <Row className="gx-py-2">
            <Col span={10}>
                <h6><a href="/">HOME</a></h6>
            </Col>
            <Col span={14}>
                <h6><a href={process.env.REACT_APP_DIGITAL_LABS}>DIGITAL LAB</a></h6>
            </Col>
        </Row>
        <Row className="gx-py-2">
            <Col span={10}>
                <h6><a href="https://www.fpt-software.com/service/cloud-professional-services/">ABOUT US</a></h6>
            </Col>
            <Col span={14}>
                <h6><a href={process.env.REACT_APP_CLOUD_OPERATION}>OPERATION</a></h6>
            </Col>
        </Row>
        <Row className="gx-py-2">
            <Col span={10}>
                <h6><a href="https://www.fpt-software.com/">COMPANY</a></h6>
            </Col>
            <Col span={14}>
                <h6><a href="https://www.fpt-software.com/contact-us/">CONTACT US</a></h6>
            </Col>
        </Row>
    </div>
    <Row className="footer-bottom" justify="center">
        <Col xs={24} md={20}>
            <p>&copy; akaClaud, FPT Software. All Rights Reserved.</p>
        </Col>
    </Row>
</div>);

export default Footer;