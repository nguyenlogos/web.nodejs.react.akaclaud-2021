import React from "react";
import { Row, Col, Carousel } from "antd";

const Slider = () => (
    <Row className="slider" justify="center">
        <Col xs={24} lg={20}>
            <Carousel autoplasy>
                <div className="slide">           
                    <img className="slide-image" src="/landing/banner.png" alt="Cloud Journey with FPT Digital Kaizen" />        
                    <div className="slide-text">
                        <h1>Cloud Journey with FPT Digital Kaizen&trade;</h1>
                        <p>COVID-19 came and has changed almost everything, including Cloud journey. FPT Software have also adjusted ourselves to quickly adapt to the new normal and provide more agility processes, thereby helping our customers speed up adaptation which makes it become a continuous improvement process.</p>
                    </div>
                </div>
                <div className="slide">                    
                    <img className="slide-image" src="/landing/banner.png" alt="Cloud Journey with FPT Digital Kaizen" />
                    <div className="slide-text">
                        <h1>Cloud Journey with FPT Digital Kaizen&trade;</h1>
                        <p>Migration to the cloud doesn’t have to be difficult, if planning is executed properly. Our methodology choices will help the customer to increase speed and reduce risk in moving cloud process. A well-designed cloud strategy should do much more than keep you competitive.</p>
                    </div>
                </div>
                <div className="slide">                    
                    <img className="slide-image" src="/landing/banner.png" alt="Cloud Journey with FPT Digital Kaizen" />
                    <div className="slide-text">
                        <h1>Cloud Journey with FPT Digital Kaizen&trade;</h1>
                        <p>The world of cloud is very different from its equivalent data center world, I keep iterating it "Cloud is not your data center run by someone else, it’s much more than that". FPT Software offers the Cloud Operation as the toolset which helps leverage in Cloud Operational excellence.</p>
                    </div>
                </div>
            </Carousel>
        </Col>
    </Row>
);

export default Slider;