import {
  FileImageOutlined,
  FileMarkdownOutlined,
  FilePdfOutlined,
  FilePptOutlined,
  FileTextOutlined,
  FileWordOutlined,
  PaperClipOutlined,
} from '@ant-design/icons';
import { getPublicResourceUrl } from '@api';
import { truncateFileName } from '@common/helpers';
import TableOfContent from '@components/TableOfContent';
import { TARGET_OFFSET } from '@constants';
import { Affix } from 'antd';
import React from 'react';

const TableContentLab = ({ attachments, commonInfo, itemTableContent }) => {
  const getContainer = () => {
    return document.getElementById('body-content');
  };

  const attachIcon = (fileName) => {
    if (/.?\.(png|jpg|gif|svg)/g.test(fileName)) {
      return <FileImageOutlined style={{ color: '#DA955B' }} />;
    }

    if (/.?\.(pdf)/g.test(fileName)) {
      return <FilePdfOutlined style={{ color: '#F89C98' }} />;
    }

    if (/.?\.(doc|docx|xls|xlsx)/g.test(fileName)) {
      return <FileWordOutlined style={{ color: '#89DF9E' }} />;
    }

    if (/.?\.(ppt|pptx)/g.test(fileName)) {
      return <FilePptOutlined style={{ color: '#89DF9E' }} />;
    }

    if (/.?\.(md)/g.test(fileName)) {
      return <FileMarkdownOutlined style={{ color: '#89DF9E' }} />;
    }

    if (/.?\.(txt)/g.test(fileName)) {
      return <FileTextOutlined style={{ color: '#89DF9E' }} />;
    }

    return <PaperClipOutlined />;
  };

  return (
    <>
      <Affix offsetTop={TARGET_OFFSET} target={getContainer}>
        {attachments != null && attachments.length > 0 && (
          <div className="lab-detail-resource">
            <h2>
              Resources
            </h2>

            {attachments.map((f, index) => {
              return (
                <a
                  className="file"
                  key={index}
                  title={f}
                  href={getPublicResourceUrl(commonInfo.code, f)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {attachIcon(f)}
                  {' '}
                  &nbsp;
                  {truncateFileName(f)}
                </a>
              );
            })}
          </div>
        )}

        <div className="table-of-content">
          <h2>
            Table of contents
          </h2>
          {itemTableContent.length > 0 && <TableOfContent items={itemTableContent} />}
        </div>
      </Affix>
    </>
  );
};

export default TableContentLab;
