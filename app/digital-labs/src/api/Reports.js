import { Report } from '@common/models';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthHttp } from './client';

// GET: /api/v1/reports
export function getReport(keyword, page, limit) {
  let url = `reports?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `reports?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(AuthHttp.get(url)).pipe(
      map((res) => {
        res.dataSources = res.dataSources ? res.dataSources.map((d) => new Report().fromJson(d)) : [];
        return res;
      })
    );
  }

  return from(AuthHttp.get(url)).pipe(
    map((res) => {
      res.dataSources = res.dataSources ? res.dataSources.map((d) => new Report().fromJson(d)) : [];
      return res;
    })
  );
}

// DELETE: /api/v1/reports/:id
export function deleteAssessment(id) {
  let url = `reports/${id}`;
  return from(AuthHttp.delete(url));
}

// GET: /api/v1/reports/:id
export function getReportById(id) {
  let url = `reports/${id}`;
  return from(AuthHttp.get(url));
}

export function reCalculate(id) {
  let url = `reports/recalculate/${id}`;
  return from(AuthHttp.put(url));
}

export function getReportTemplates() {
  let url = 'reports/admin/templates';
  return from(AuthHttp.get(url));
}

export function sendEmailReport(reportId, userInfor) {
  let url = `reports/send-email/${reportId}`;
  return from(AuthHttp.put(url, userInfor));
}
