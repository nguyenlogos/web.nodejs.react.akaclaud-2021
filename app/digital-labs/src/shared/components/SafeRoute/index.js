import { dispatchAuthorizationAction } from '@appRedux/actions';
import { authorValid } from '@common/authorization';
import { navigateTo } from '@common/helpers';
import { AUTHORIZATION_CODE } from '@constants';
import { login } from 'aka-keycloak-login';
import * as React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

class SafeRoute extends React.Component {
  componentDidMount() {
    this.checkAuthentication()
      .then(() => {
        this.checkValidAuthor(this.props);
      })
      .catch((e) => {
        console.log('Not authenticated.', this._currentLoginRoles());
      });
  }

  _currentLoginRoles = () => {
    return this.props.user != null ? this.props.user.roles : null;
  };

  checkAuthentication() {
    return new Promise((resolve, reject) => {
      const roles = this._currentLoginRoles();

      if (roles == null) {
        login().then(() => {
          console.log('Redirect login...');
        });

        reject(false);
      } else {
        resolve(true);
      }
    });
  }

  checkValidAuthor(props) {
    const { history, dispatchAuthorization, roles: expectedRoles } = props;
    const roles = this._currentLoginRoles();
    if (!authorValid(expectedRoles, roles || '')) {
      navigateTo('/403');
      // TODO: handle permission denied
      if (history) {
        // history.push("/permission-denied");
      }
      dispatchAuthorization('401', AUTHORIZATION_CODE.PermissionMissing);
    }
  }

  render() {
    const { exact, path, render: childRender, component: Component } = this.props;
    const childRenderFunction = (matchProps) => (childRender ? childRender(matchProps) : <Component {...matchProps} />);

    return exact ? (
      <Route exact={true} path={path} render={childRenderFunction} />
    ) : (
      <Route path={path} render={childRenderFunction} />
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, {
  dispatchAuthorization: dispatchAuthorizationAction,
})(SafeRoute);
