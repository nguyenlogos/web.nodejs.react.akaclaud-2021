import { getFilteredMenu } from '@common/helpers';
import Breadcrumbs from '@components/Breadcrumbs';
import React from 'react';
import { useSelector } from 'react-redux';

import ScenarioForm from './ScenarioForm';

const ScenarioCreate = () => {
  const { roles } = useSelector(({ user }) => user);

  // get url from first item of Contribution
  var rootUrl = '/';
  const menuContribution = getFilteredMenu(roles).filter((item) => item.id === 'Contribute');
  if (menuContribution.length !== 0) {
    if (menuContribution[0].children.length !== 0) {
      rootUrl = menuContribution[0].children[0].url;
    }
  }
  return (
    <div>
      <div className="contribute-scenario-banner">
        <Breadcrumbs list={[{ title: 'My Contribution', to: rootUrl }, { title: 'Add new scenario' }]} />
        <h1>
          Add new scenario
        </h1>
      </div>
      <ScenarioForm />
    </div>
  );
};

export default ScenarioCreate;
