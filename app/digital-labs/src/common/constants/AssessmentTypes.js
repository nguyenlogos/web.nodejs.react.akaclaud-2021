export const ReportAssessment = {
  Readiness: 'Cloud Readiness Assessment',
  Decision: 'Cloud Migration Decision',
  Strategy: 'Cloud Migration Strategy',
};

export const RecommendAction = {
  Recommend: 'Recommended Actions',
  Congratulations: 'Congratulations',
};
