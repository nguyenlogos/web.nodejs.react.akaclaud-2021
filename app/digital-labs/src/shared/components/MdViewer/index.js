import axios from 'axios';
import { MarkdownPreview } from 'octopus-marked';
import React, { Component, useEffect, useState } from 'react';

export const AsyncMarkdownViewer = ({ contentUrl }) => {
  const [loading, setLoading] = useState(true);
  const [content, setContent] = useState(null);

  useEffect(() => {
    axios
      .get(contentUrl)
      .then((res) => res.data)
      .then((text) => {
        setContent(text);
        setLoading(false);
      });
  }, [contentUrl]);

  return (
    <div className="md-viewer async">
      {loading ? (
        <>
          loading...
        </>
      ) : <MarkdownPreview value={content} />}
    </div>);
};

class Markdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: props.src,
    };
  }

  render() {
    const { content } = this.state;
    const { options } = this.props;

    const mdOptions = {
      baseUrl: null,
      breaks: false,
      gfm: true,
      headerIds: true,
      headerPrefix: '',
      highlight: null,
      langPrefix: 'language-',
      mangle: true,
      pedantic: false,
      sanitize: false,
      sanitizer: null,
      silent: false,
      smartLists: false,
      smartypants: false,
      tokenizer: null,
      walkTokens: null,
      xhtml: false,
    };

    return (
      <div className="md-viewer">
        <MarkdownPreview value={content} options={options} markedOptions={mdOptions} />
      </div>
    );
  }
}

export default Markdown;
