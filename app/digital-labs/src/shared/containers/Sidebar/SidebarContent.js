import { PUBLIC_URL } from '@common';
import CustomScrollbars from '@components/CustomScrollbars';
import UserMenu from '@components/UserMenu';
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  SIDEBAR_MENU,
  THEME_TYPE_LITE,
} from '@constants';
import { Menu } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import SidebarLogo from './SidebarLogo';

const SubMenu = Menu.SubMenu;

const SidebarContent = ({ user }) => {
  let { pathname } = useSelector(({ common }) => common);
  let { navStyle, themeType } = useSelector(({ settings }) => settings);
  const { roles } = useSelector(({ user }) => user);
  const [matchingItems, setMatchingItems] = useState([]);

  useEffect(() => {
    if (roles !== null) {
      const matchingRoles = roles.split(',');
      const matchingItems = SIDEBAR_MENU.filter((item) => {
        if (item.acceptRoles.some((role) => matchingRoles.indexOf(role) >= 0)) {
          item.children = item.children.filter((c) => c.roles.some((role) => matchingRoles.indexOf(role) >= 0));
          return item;
        }
        return null;
      });
      const filteredMatchingItems = matchingItems.filter((item) => item.children.length > 0);
      setMatchingItems(filteredMatchingItems);
    }
  }, [roles]);

  const getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return 'gx-no-header-notifications';
    }
    return '';
  };

  const getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return 'gx-no-header-submenu-popup';
    }
    return '';
  };

  const isLoggedIn = (user) => user && user.roles;

  const defaultOpenKeys = matchingItems
    ? matchingItems
      .map((menu) => {
        const matches = menu.children && menu.children.some((c) => `${PUBLIC_URL}${c.url}` === pathname);

        if (matches) {
          return menu.id;
        }

        return null;
      })
      .filter((k) => k != null)
    : [];

  return (
    <>
      <SidebarLogo />
      <div className="gx-sidebar-content">
        <div className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}>
          {isLoggedIn(user) && <UserMenu user={user} />}
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar admin-menu-left">
          <Menu
            defaultOpenKeys={defaultOpenKeys}
            selectedKeys={[pathname]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            className="sidebar-menu"
            mode="inline"
          >
            {matchingItems &&
              matchingItems.map((submenu) => (
                <SubMenu
                  key={submenu.id}
                  popupClassName={getNavStyleSubMenuClass(navStyle)}
                  title={
                    <span>
                      {submenu.icon}
                      <span>
                        {submenu.text}
                      </span>
                    </span>
                  }
                >
                  {submenu.children &&
                    submenu.children.map((link) => (
                      <Menu.Item key={`${PUBLIC_URL}${link.url}`}>
                        <Link to={`${PUBLIC_URL}${link.url}`}>
                          <span>
                            {link.text}
                          </span>
                        </Link>
                      </Menu.Item>
                    ))}
                </SubMenu>
              ))}
          </Menu>
        </CustomScrollbars>
      </div>
    </>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;
