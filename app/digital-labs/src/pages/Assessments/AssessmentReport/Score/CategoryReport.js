import { InfoCircleOutlined } from '@ant-design/icons';
import { ReportAssessment } from '@constants';
import { Button, Col, Progress, Row } from 'antd';
import * as React from 'react';

import CategoryRecommendation from './CategoryRecommendation';

const CategoryReport = (props) => {
  const {
    readiness,
    categoryIndex,
    category,
    percent,
    activeRecommendation,
    reportName,
    recommend,
    openRecommendation,
    closeRecommendation,
  } = props;
  const arrayReport = Object.entries(readiness);
  return (
    <>
      <hr className="line" />
      <Row className="domain-progress">
        <Col className="line-process" xl={16} sm={18}>
          <span className="domain-title">
            {categoryIndex + 1}.{category}
          </span>
          {arrayReport && arrayReport.map((e, index) => {
            const subDomainName = e[0];
            const value = e[1];
            const percent = parseInt(value);
            const classNames = percent >= 50 ? 'process match' : 'process less';
            return (
              <div key={index} className="report-list">
                <span className="sub-domain-title">{subDomainName}</span>
                <Progress
                  className={classNames}
                  percent={percent}
                  showInfo={false}
                  strokeColor={percent < 50 ? '#F6CC16' : '#49C758'}
                />
                <br />
              </div>
            );
          })}
        </Col>
        <Col className="cirle-process" xl={{ span: 8, offset: 0 }} sm={{ span: 4, offset: 1 }}>
          <div className="caculate-progress">
            <Progress type="circle" percent={percent} strokeColor={percent < 60 ? '#F6CC16' : '#49C758'} />
          </div>
        </Col>
      </Row>
      {reportName && reportName === ReportAssessment.Readiness ? (
        <div>
          {activeRecommendation !== categoryIndex && (
            <Button type="link" shape="round" size="small" icon={<InfoCircleOutlined />} onClick={openRecommendation}>
              See recommendation
            </Button>
          )}
          {activeRecommendation === categoryIndex && (
            <>
              <Button
                type="primary"
                shape="round"
                size="small"
                icon={<InfoCircleOutlined />}
                onClick={closeRecommendation}
              >
                See recommendation
              </Button>
              <div className="collapsable-recommendation-arrow">
                <div className="inner" />
              </div>
              <div className="collapsable-recommendation">
                <CategoryRecommendation name={category} recommend={recommend} />
              </div>
            </>
          )}
        </div>
      ) : null}
    </>
  );
};
export default CategoryReport;
