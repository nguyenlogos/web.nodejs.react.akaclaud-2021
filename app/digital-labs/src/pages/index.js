import { PUBLIC_URL } from '@common';
import { cleanURL } from '@common/helpers';
import asyncComponentLoader from '@components/AsyncComponent';
import SafeRoute from '@components/SafeRoute';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const App = ({ match }) => {
  const asyncComponent = (path, prefix) => {
    return asyncComponentLoader(() => import(`${path}`), { prefixRoute: cleanURL(`${matchUrl}/${prefix}`) });
  };

  const matchUrl = cleanURL(`${match.url}${PUBLIC_URL}`);

  return (
    <div className="gx-main-content-wrapper">
      <Switch>
        <Route path={`${matchUrl}`} exact={true}>
          <Redirect to={cleanURL(`${matchUrl}/stories`)} />
        </Route>
        <SafeRoute path={cleanURL(`${matchUrl}/portals`)} component={asyncComponent('./Portals', 'portals')} />
        <SafeRoute path={cleanURL(`${matchUrl}/accounts`)} component={asyncComponent('./Accounts', 'accounts')} />

        <Route path={cleanURL(`${matchUrl}/stories`)} component={asyncComponent('./BusinessStory', 'stories')} />
        <Route path={cleanURL(`${matchUrl}/labs`)} component={asyncComponent('./TrainingLabs', 'labs')} />
      </Switch>
    </div>
  );
};

export default App;
