// hook popstate event - base on 'history' from 'react-router' concepts
// block navigation if hash routing with anchor
export default function blockHashRoute() {
  if (window._hookPopStateListener != null) {
    return;
  }

  const handleBlockHashRoute = (e) => {
    window._hookPopStateListener = handleBlockHashRoute;
    const hash = e.target.location.hash;

    if (hash != null) {
      const hashes = hash.split('#');

      // hash routing
      if (hashes.some((h) => h.length > 0)) {
        e.preventDefault();
        e.stopImmediatePropagation();
        e.stopPropagation();
      }
    }
  };

  window.addEventListener('popstate', handleBlockHashRoute);
}
