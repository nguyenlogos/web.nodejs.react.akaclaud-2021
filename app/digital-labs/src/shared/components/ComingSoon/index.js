import React from 'react';

const CommingSoon = () => (
  <div className="coming-soon-page">
    <h2 className="coming-soon-page-title">
      COMING SOON
    </h2>
  </div>
);

export default CommingSoon;
