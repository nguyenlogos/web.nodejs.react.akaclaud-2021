import React from "react";
import { Row, Col, Carousel } from "antd";
import { CUSTOMERS } from "@constants/Landing";

const Customers = () => (<>
  <Row className="customers" justify="center">
    <Col xs={22} md={20}>
      <div className="customers-title">
        <h2>Our Customers</h2>
      </div>
      <Row gutter={{ md: 50 }} className="web-layout">
        {CUSTOMERS.map((customer, i) => (
          <Col xs={24} md={8} key={i}>
            <div className="customer-card">
              <img className="customer-card-image" src={customer.image} alt={customer.title} />

              <div className="customer-card-text">
                <h3>{customer.title}</h3>
                <p>{customer.description}</p>
                {customer.companyName && (<div className="customer-card-footer">
                  <div className="company">
                    <img src={customer.companyImage} alt={customer.companyName} />
                    <p>{customer.companyName}</p>
                  </div>
                  <div className="hashtags">{customer.hashtags}</div>
                </div>)}
              </div>
            </div>
          </Col>
        ))}
      </Row>
      <Row className="mobile-layout">
        <Carousel autoplasy>
          {CUSTOMERS.map((customer, i) => (
            <Col xs={24} md={8} key={i}>
              <div className="customer-card">
                <img className="customer-card-image" src={customer.image} alt={customer.title} />

                <div className="customer-card-text">
                  <h3>{customer.title}</h3>
                  <p>{customer.description}</p>
                  {customer.companyName && (<div className="customer-card-footer">
                    <div className="company">
                      <img src={customer.companyImage} alt={customer.companyName} />
                      <p>{customer.companyName}</p>
                    </div>
                    <div className="hashtags">{customer.hashtags}</div>
                  </div>)}
                </div>
              </div>
            </Col>
          ))}
        </Carousel>
      </Row>
    </Col>
  </Row>
</>);

export default Customers;