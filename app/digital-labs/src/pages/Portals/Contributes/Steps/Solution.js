import { Col, Row } from 'antd';
import React, { Component } from 'react';

import HtmlAttachment from '../HtmlAttachment';

class Solution extends Component {
  constructor(props) {
    super(props);

    this.state = {
      solution: props.defaultValue ? props.defaultValue.solution : null,
    };
  }

  validateInput = () => {
    return this.state.solution ? true : false;
  };

  onSolutionChange = (context) => {
    this.setState(
      {
        solution: context,
      },
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange(this.state);
        }
      }
    );
  };

  render() {
    const { solution } = this.state;
    return (
      <div className="lab-step-prerequisite">
        <Row>
          <Col span={24}>
            <div className="lab-overview-form_group">
              <div className="lab-overview-form_item">
                <label className="required" htmlFor="guidance">
                  Solution
                </label>
                <HtmlAttachment source={solution} onBlur={this.onSolutionChange} />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Solution;
