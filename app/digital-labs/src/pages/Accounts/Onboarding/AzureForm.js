import { AZURE_GUIDELINE_URL } from '@common';
import { Button, Form, Input, Spin } from 'antd';
import classnames from 'classnames';
import React from 'react';

const FormItem = Form.Item;

const AzureOnBoardingForm = ({ className, onSubmit, onSubmitFailed, isLoading }) => {
  const formClassnames = classnames('onboarding-form', className);

  return (
    <Form name="azure-onboarding" className={formClassnames} onFinish={onSubmit} onFinishFailed={onSubmitFailed}>
      <p className="sm-notes">
        <b>
          Note:
        </b>
        &nbsp;You can use existing cloud account to study&nbsp;
        <a href={AZURE_GUIDELINE_URL} target="_blank" className="guideline" rel="noopener noreferrer">
          (Guideline)
        </a>
      </p>

      <FormItem name="accessKey" rules={[{ required: true, message: 'Please input your Assess key' }]}>
        <Input placeholder="Enter Azure Assess key" />
      </FormItem>

      <FormItem name="secretKey" rules={[{ required: true, message: 'Please input your Secret key!' }]}>
        <Input placeholder="Enter Azure Secret key" />
      </FormItem>

      <FormItem name="tenantId" rules={[{ required: true, message: 'Please input your Ternant ID!' }]}>
        <Input placeholder="Enter Azure Ternant ID" />
      </FormItem>

      <FormItem name="subscriptionId" rules={[{ required: true, message: 'Please input your Subscription ID!' }]}>
        <Input placeholder="Enter Azure Subscription ID" />
      </FormItem>

      {isLoading && (
        <div className="gx-text-center">
          <Spin tip="Account is creating. Please wait until process is complete" />
        </div>
      )}

      <FormItem>
        <Button className="gx-full-width" type="primary" htmlType="submit">
          Save
        </Button>
      </FormItem>
    </Form>
  );
};

export default AzureOnBoardingForm;
