import { from } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthHttp } from './client';

/**
 * GET /accounts/my-credits
 * Get my credit info
 */
export function getMyCredit() {
  let url = 'accounts/my-credits';
  return from(AuthHttp.get(url));
}

/**
 * POST /accounts/credits
 * Add user credit
 * @param userName
 * @param credit
 * @param description
 */
export function addUserCredit(userName, credit, description) {
  return from(AuthHttp.post('accounts/credits', { userName, credit, description }));
}

/**
 * GET /accounts/my-credits/history
 * Get my credit changed history
 * @param keyword action note regex
 * @param page page index
 * @param limit page size
 */
export function getMyCreditHistory(keyword, page, limit) {
  let url = `accounts/my-credits/history?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `accounts/my-credits/history?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(AuthHttp.get(url));
  }

  return from(AuthHttp.get(url));
}

/**
 * GET /accounts/allstatus _page
 *  + _keyword: string
 *  + _page: number
 *  + _limit: number
 * [Admin] Get all cloud account info
 * @param keyword account name regex
 * @param page page index
 * @param limit page size
 */
export function getAllAccount(keyword, page, limit) {
  let url = `accounts/allstatus?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `accounts/allstatus?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(AuthHttp.get(url));
  }

  return from(AuthHttp.get(url));
}

/**
 * PUT /accounts/onboarding
 * Register cloud account - onboarding flow
 * @param data register info
 */
export function registerAccount(data) {
  return from(AuthHttp.put('accounts/onboarding', data)).pipe(map(() => true));
}

/**
 * GET /accounts
 *  + _keyword: string
 *  + _page: number
 *  + _limit: number
 * Get current user cloud accounts info
 * @param keyword account name regex
 * @param page page index
 * @param limit page size
 */
export function getAccountsByUser(keyword, page = 0, limit = 100) {
  let url = `accounts?_page=${page}&_limit=${limit}`;
  if (keyword && keyword.length > 0) {
    url = `accounts?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
    return from(AuthHttp.get(url));
  }
  return from(AuthHttp.get(url));
}

export function deleteCloudAccount(id) {
  const url = `accounts/${id}`;

  return from(AuthHttp.delete(url));
}

export function editCloudAccount(id, params) {
  const url = `accounts/${id}`;

  return from(AuthHttp.put(url, params));
}

/**
 * GET /accounts/status/{vendor}
 *  + vendor: string
 * Get cloud accounts by vendor
 * @param vendor
 */
export function getCloudAccountByVendor(vendor) {
  let url = `accounts/status/${vendor}`;
  return from(AuthHttp.get(url));
}

/**
 * GET /accounts/status
 *  + vendor: string
 * Get cloud accounts status
 */
export function getStatus() {
  let url = 'accounts/status';
  return from(AuthHttp.get(url));
}

/**
 * GET /accounts/template
 * Get cloud accounts template
 */
export function getTemplate() {
  let url = 'accounts/template';
  return from(AuthHttp.get(url));
}

/**
 * POST /accounts/MBI
 * Create cloud accounts MBI
 */
export function addMBI(data) {
  return from(AuthHttp.post('accounts/MBI', data)).pipe(map(() => true));
}

export function getCreditAccount(page, limit, keyword) {
  let url = `accounts/credits/accounting?_page=${page}&_limit=${limit}`;
  if (keyword != null && keyword.length > 0) {
    url = `accounts/credits/accounting?_keyword=${encodeURIComponent(keyword)}&_page=${page}&_limit=${limit}`;
  }
  return from(AuthHttp.get(url));
}
