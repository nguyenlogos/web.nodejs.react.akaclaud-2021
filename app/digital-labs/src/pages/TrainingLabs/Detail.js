import {
  checkStacksUser,
  cleanResource,
  createStack,
  describeStack,
  describeStackResources,
  generateSignURL,
  getCategories,
  getLab,
  getLevels,
  getMyCredit,
  getPublicDoc,
  deactiveLab
} from '@api';
import { hideAuthLoader, showAuthLoader } from '@appRedux/actions';
import { AccountBilling, CloudProvider, TrainingLabIndex, LabDetailState, ResourceState } from '@common/constants';
import { matchesHeader, openNotification } from '@common/helpers';
import { login } from 'aka-keycloak-login';
import Breadcrumbs from '@components/Breadcrumbs';
import { Card, Col, Modal, Row, Tabs, Tooltip } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forkJoin } from 'rxjs';

import BusinessStory from '../Common/BusinessStory';
import Guidance from '../Common/Guidance';
import LabAccount from '../Common/LabAccount';
import ProvisionResources from '../Common/ProvisionResources';
import ResourceOutputs from '../Common/ResourceOutputs';
import TableContentLab from '../Common/TableContentLab';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const TabPane = Tabs.TabPane;

class LabDetail extends Component {
  _subscriptions = [];

  constructor(props) {
    super(props);

    this.state = {
      startingLab: false,
      labId: props.match.params.id,
      status: LabDetailState.Ready,
      commonInfo: {
        labName: '',
        code: '',
        timeInMinutes: 0,
        childCategoryId: [],
        complexityId: 0,
        vendor: '',
        credit: 0,
      },
      businessStory: '',
      guideline: ' ',
      userCredit: null,
      provisonResources: null,
      resourceOutputs: [],
      attachments: [],
      activeStack: null,
      activeTab: '1',
      storyContentItems: [],
      trainingLabContentItems: [
        {
          href: '#provision',
          title: 'Provision Resource',
        },
      ],
      existingTemplate: false,
      isEdit: false
    };

    this.autoSubscribeStack = 15000;
  }

  componentDidMount() {
    this._subscriptions.push(this.$loadDetailLab());
    this.$loadLoginUserInfo();
  }

  componentWillUnmount() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  handleRedirectMainPage = () => {
    const { history } = this.props;
    history.push('/labs');
  }

  $loadDetailLab = () => {
    showAuthLoader();
    const { labId } = this.state;
    return forkJoin(getLab(labId), getLevels(), getCategories()).subscribe(
      ([labInfo, levels, categories]) => {
        if (!labInfo.active) {
          Modal.warning({
            title: 'As this Training lab is being deactivated mode, it can\'t be displayed. ' +
              'You will be redirected to the main page immediately.',
            onOk: this.handleRedirectMainPage,
            width: 652
          });
        } else {
          const { user } = this.props;
          const { roles, email } = user;
          const currentLevel = levels.findIndex((l) => l.levelId === labInfo.complexityId + '');
          const currentCategories = categories
            ? categories
              .filter((c) => labInfo.childCategoryId.includes(c.categoryId))
              .map((c) => {
                if (c.categoryId) {
                  return c.name;
                }
                return '';
              })
            : [];

          const commonInfo = {
            labName: labInfo.labName,
            code: labInfo.code,
            description: labInfo.description,
            timeInMinutes: labInfo.timeInMinutes,
            vendor: labInfo.vendor,
            credit: labInfo.credit,
            estimateCost: labInfo.estimateCost,
            complexityId: parseInt(currentLevel) !== -1 ? levels[currentLevel].name : '',
            childCategoryId: currentCategories,
          };

          const { code, preLabSubTabURL, guidanceSubTabURL, cloudformationURL } = labInfo;

          this.setState(
            {
              commonInfo: commonInfo,
              attachments: labInfo.attachments,
              isEdit: roles && (roles.includes('leader') ||
                (roles.includes('contributor') && email === labInfo.createdBy))
            },
            () => this.$loadLabContent(code, preLabSubTabURL, guidanceSubTabURL, cloudformationURL)
          );
        }
      },
      (err) => {
        hideAuthLoader();
        openNotification('error', 'Lab not found!');
      }
    );
  };

  $loadLoginUserInfo = () => {
    const { user } = this.props;
    const { roles } = user;

    // has role 'trainee'
    if (roles && roles.indexOf('trainee') >= 0) {
      this._subscriptions.push(this.$loadActiveStacks());
      this._subscriptions.push(this.getUserCredit());
    }
  };

  $loadActiveStacks = () => {
    const { labId } = this.state;

    return checkStacksUser(labId).subscribe((stacks) => {
      if (stacks && stacks.length > 0) {
        const runningStack = stacks[0];
        this.setState(
          {
            provisonResources: {
              startTime: runningStack.createdTime != null ? new Date(runningStack.createdTime) : null,
              portal: null,
              stackName: runningStack.stackName,
              resources: [],
            },
            activeStack: runningStack,
            startingLab: true,
          },
          () => {
            this.autoSubcribeStack();
          }
        );
      }
    });
  };

  $loadProvision = () => {
    const { provisonResources } = this.state;

    if (provisonResources) {
      const { stackName } = provisonResources;
      this._subscriptions.push(
        describeStackResources(stackName).subscribe(
          (res) => {
            provisonResources.resources = res.resources;
            this.setState({
              provisonResources: provisonResources,
            });
          },
          (error) => {
            this.setState({ status: LabDetailState.Failed, startingLab: false });
          }
        )
      );
    }
  };

  $loadLabContent(labCode, businessStoryUrl, guidelineUrl, cloudformationURL) {
    this._subscriptions.push(
      getPublicDoc(labCode, businessStoryUrl).subscribe((story) => {
        if (story != null) {
          const { storyContentItems } = this.state;
          const tocHeaders = matchesHeader(story);
          this.setState({
            businessStory: story,
            storyContentItems: storyContentItems.concat(tocHeaders),
          });
        }
      })
    );

    this._subscriptions.push(
      getPublicDoc(labCode, guidelineUrl).subscribe((guideline) => {
        if (guideline != null) {
          const { trainingLabContentItems } = this.state;
          const tocHeaders = matchesHeader(guideline);
          this.setState({
            guideline: guideline,
            trainingLabContentItems: trainingLabContentItems.concat(tocHeaders),
          });
        }
      })
    );

    this._subscriptions.push(
      getPublicDoc(labCode, cloudformationURL).subscribe((template) => {
        if (
          template != null &&
          ((typeof template == 'object' && Object.keys(template).length > 0) || template.length > 0)
        ) {
          this.setState({ existingTemplate: true });
        }
      })
    );
  }

  autoSubcribeStack = () => {
    const { provisonResources } = this.state;

    if (provisonResources) {
      const { stackName } = provisonResources;
      this._subscriptions.push(
        describeStack(stackName).subscribe(
          (res) => {
            let currentStackStatus = parseInt(res.stackStatus + '');

            if (currentStackStatus === ResourceState.Failed) {
              this.setState({ status: LabDetailState.Failed, startingLab: false }, () => {
                this.$loadProvision();
              });
            }

            // if stack stopped => ready for start lab
            if (currentStackStatus === ResourceState.Stopped) {
              this.setState({ status: LabDetailState.Ready });
            }

            if (currentStackStatus === ResourceState.Running) {
              this.getSignInURL();
              this.setState(
                {
                  status: LabDetailState.Running,
                  resourceOutputs: res.resourceOutputs.output,
                },
                () => {
                  this.$loadProvision();
                }
              );
            }

            if (currentStackStatus === ResourceState.Creating) {
              this.setState(
                {
                  status: LabDetailState.Creating,
                },
                () => {
                  this.$loadProvision();
                  // auto subscribe stack
                  if (this.autoSubscribeStack > 0) {
                    setTimeout(() => {
                      this.autoSubcribeStack();
                    }, this.autoSubscribeStack);
                  }
                }
              );
            }
          },
          (err) => {
            console.error('Describe stack failure...');
            this.setState({ status: LabDetailState.Failed, startingLab: false });
          }
        )
      );
    }
  };

  getSignInURL = () => {
    const { provisonResources, commonInfo } = this.state;

    if (provisonResources) {
      const { stackName, portal } = provisonResources;

      if (!portal) {
        this._subscriptions.push(
          generateSignURL(stackName).subscribe((res) => {
            if (commonInfo.vendor === CloudProvider.Gcp) {
              provisonResources.portal = {
                signInUrl: res.url.loginUrl,
                loginId: res.url.serviceId,
                loginPw: res.url.secretKey,
              };
            } else {
              provisonResources.portal = {
                signInUrl: res.url,
              };
            }

            this.setState({
              provisonResources: provisonResources,
            });
          })
        );
      }
    }
  };

  getUserCredit = () => {
    return getMyCredit().subscribe((res) => {
      this.setState({
        userCredit: res,
      });
    });
  };

  startLab = (selectedAccount) => {
    const { labId } = this.state;
    const accountId = selectedAccount._id;

    this.setState({ startingLab: true }, () => {
      this._subscriptions.push(
        createStack(labId, accountId).subscribe(
          (res) => {
            const provisonResources = {
              startTime: res.startTime ? new Date(res.startTime) : null,
              stackName: res.stackName,
              resources: [],
              portal: null,
            };
            this.setState({ status: res.status, provisonResources }, () => {
              this.autoSubcribeStack();
            });
          },
          (error) => {
            openNotification('error', error.response.data ? `${error.response.data}` : 'Cannot create stack');
            this.setState({ status: LabDetailState.Failed, startingLab: false });
          }
        )
      );
    });
  };

  onStartLabClick = (selectedAccountId, accounts, runningLab) => {
    const { labId } = this.state;
    const { user, history } = this.props;
    const { roles } = user;
    if (runningLab && runningLab.labId !== labId) {
      Modal.confirm({
        title: 'WARNING',
        content:
          'We recommend you should not run two labs simultaneously. Please end up the current running lab before starting off the new one. Would you like to switch back to the running lab?',
        onOk: () => {
          history.replace('/labs/' + runningLab.labId);
        },
      });
    } else {
      const selectedAccount = accounts.find((a) => a._id === selectedAccountId);

      if (!roles || roles.indexOf('trainee') < 0) {
        return login().then(() => {
          console.log('Redirect login...');
        });
      }
      const { commonInfo, userCredit } = this.state;
      const labCredit = commonInfo.credit ? commonInfo.credit : 0;
      const balance = userCredit.balance;

      // free lab not need check credit
      let requiredCredits = 0;
      if (labCredit !== 0) {
        requiredCredits =
          selectedAccount.billingOwner === AccountBilling.CustomerBilling ? userCredit.defaultInfraCredit : labCredit;
      }
      const availableCredit = balance - requiredCredits >= 0;

      if (availableCredit) {
        const contentHolder = requiredCredits === 0 ? 'is Free' : 'required ' + requiredCredits + ' Credit';
        Modal.confirm({
          title: 'Notification',
          content: 'This lab ' + contentHolder + '. Are you sure to continue?',
          onOk: () => this.startLab(selectedAccount),
          onCancel: () => this.setState({ status: LabDetailState.Ready }),
        });
      } else {
        Modal.warning({
          title: 'Notification',
          content: 'You don\'t have enough credit to start this lab.',
          onOk: () => this.setState({ status: LabDetailState.Ready }),
        });
      }
      this.setState({ activeTab: TrainingLabIndex.BusinessStory });
    }
  };

  onFinishLabClick = () => {
    const { provisonResources } = this.state;
    if (provisonResources) {
      const { stackName } = provisonResources;

      this._subscriptions.push(
        cleanResource(stackName).subscribe(
          (res) => {
            // need remove resources
            this.setState({ status: LabDetailState.Ready, startingLab: false });
            window.location.reload();
          },
          (error) => {
            openNotification('error', 'Clean error');
          }
        )
      );
    }
  };

  onChangeTab = (key) => {
    this.setState({ activeTab: key });
  };

  handleBeforeEditLab = () => {
    const { user, history } = this.props;
    const { roles } = user;
    const { labId } = this.state;
    this._subscriptions.push(
      deactiveLab(labId).subscribe(
        () => {
          openNotification('success', 'Change status success');
          history.push(`/portals/contributes/labs/edit/${labId}`,
            roles && roles.includes('leader') ? 'AllLab' : 'Lab');
        },
        () => {
          openNotification('error', 'Error occured');
        }
      )
    );
  }

  handleEditLab = () => {
    Modal.confirm({
      title: 'Are you sure deactive this lab?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleBeforeEditLab
    });
  }

  renderVendor(vendor) {
    switch (vendor) {
      case CloudProvider.Aws:
        return 'AWS';

      case CloudProvider.Azure:
        return 'Azure';

      case CloudProvider.Gcp:
        return 'Google';

      default:
        return (
          <i>
            Vendor not right
          </i>
        );
    }
  }

  render() {
    const {
      provisonResources,
      status,
      commonInfo,
      businessStory,
      guideline,
      startingLab,
      activeStack,
      resourceOutputs,
      attachments,
      storyContentItems,
      trainingLabContentItems,
      activeTab,
      existingTemplate,
    } = this.state;
    const { user } = this.props;
    const { roles } = user;

    return (
      <div className="lab-detail">
        <Row align="middle" className="page-breadcrumbs">
          <Col span={24}>
            <Breadcrumbs
              list={[
                { title: 'Digital Lab', to: '/' },
                { title: 'Training Lab', to: '/labs' },
              ]}
            />
            <div className="nav-bar gx-py-3">
              <div className="nav-bar_title">
                <span>
                  {commonInfo.labName}
                </span>
                {this.state.isEdit &&
                  <span className="lab-edit-text gx-pl-5" onClick={this.handleEditLab}>
                    (Edit)
                  </span>}
              </div>
            </div>
            <div className="description">
              {!!commonInfo.vendor && (
                <Tooltip
                  placement="bottom"
                  color="white"
                  trigger={['hover']}
                  overlayClassName="lab-detail-tooltip"
                  overlay={
                    <span>
                      Cloud Vendor
                    </span>
                  }
                >
                  <div>
                    <div className="vendor" />
                    <p>
                      {this.renderVendor(commonInfo.vendor)}
                    </p>
                  </div>
                </Tooltip>
              )}
              {commonInfo.childCategoryId && commonInfo.childCategoryId.length > 0 && (
                <div>
                  <Tooltip
                    placement="bottom"
                    color='white'
                    trigger={['hover']}
                    overlayClassName="lab-detail-tooltip"
                    overlay={<span>
                      Category
                    </span>}
                  >
                    <div className="category" />
                    <span>
                      {commonInfo.childCategoryId.slice(0, 2).join(', ')}
                    </span>
                  </Tooltip>
                  {commonInfo.childCategoryId.length > 3 && (
                    <>
                      <span>
                        &nbsp;and&nbsp;
                      </span>
                      <Tooltip
                        placement="bottom"
                        color="white"
                        trigger={['click', 'hover']}
                        overlayClassName="lab-detail-tooltip"
                        overlay={
                          <span>
                            {commonInfo.childCategoryId.slice(2).join(', ')}
                          </span>
                        }
                      >
                        <span className="show-more-categories">
                          more
                        </span>
                      </Tooltip>
                    </>
                  )}
                </div>
              )}
              {!!commonInfo.complexityId && (
                <Tooltip
                  placement="bottom"
                  color="white"
                  trigger={['hover']}
                  overlayClassName="lab-detail-tooltip"
                  overlay={
                    <span>
                      Level
                    </span>
                  }
                >
                  <div>
                    <div className="complexity" />
                    <p>
                      {commonInfo.complexityId}
                    </p>
                  </div>
                </Tooltip>
              )}
              {!!commonInfo.credit && (
                <Tooltip
                  placement="bottom"
                  color="white"
                  trigger={['hover']}
                  overlayClassName="lab-detail-tooltip"
                  overlay={
                    <span>
                      Basically, the credit includes the cost that you have to allocate for this Digital Lab.
                    </span>
                  }
                >
                  <div>
                    <div className="credit" />
                    <p>
                      {commonInfo.credit}
                      {' '}
                      Credits
                    </p>
                  </div>
                </Tooltip>
              )}
              {!!commonInfo.timeInMinutes && (
                <Tooltip
                  placement="bottom"
                  color="white"
                  trigger={['hover']}
                  overlayClassName="lab-detail-tooltip"
                  overlay={
                    <span>
                      The timer, which starts when you click Start Lab, shows how long Cloud resources will be made
                      available to you.
                    </span>
                  }
                >
                  <div>
                    <div className="time" />
                    <p>
                      {commonInfo.timeInMinutes}
                      {' '}
                      Minutes
                    </p>
                  </div>
                </Tooltip>
              )}
            </div>
          </Col>
        </Row>

        <Row align="center">
          <Col span={24}>
            <Card className="lab-detail-card">
              <Row>
                <Col md={16} xs={24} className="left-content">
                  <Tabs onChange={this.onChangeTab} activeKey={activeTab} tabBarGutter={50} className="lg-tabs-wrapper">
                    <TabPane tab="Business Story" key="1">
                      <div className="content-wrapper">
                        <BusinessStory story={businessStory} />
                      </div>
                    </TabPane>

                    <TabPane tab="Training Lab" key="2">
                      <div className="content-wrapper">
                        <div className="guidance">
                          <h1 id="provision">
                            Provision resource
                          </h1>
                          {status === LabDetailState.Ready && (
                            <p>
                              When you click the [Start Lab], your lab resources are provisioned automatically here.
                            </p>
                          )}

                          {provisonResources && status === LabDetailState.Creating && (
                            <div className="provision-table">
                              <ProvisionResources provisonResources={provisonResources} />
                            </div>
                          )}

                          {resourceOutputs.length > 0 && provisonResources && status === LabDetailState.Running && (
                            <ResourceOutputs
                              provisonResources={provisonResources}
                              commonInfo={commonInfo}
                              resourceOutputs={resourceOutputs}
                            />
                          )}

                          {guideline && <Guidance guideline={guideline} />}
                        </div>
                      </div>
                    </TabPane>
                  </Tabs>
                </Col>

                <Col md={8} xs={24} className="right-content">
                  <div className="header">
                    {commonInfo.vendor && (
                      <LabAccount
                        labStatus={status}
                        labVendor={commonInfo.vendor}
                        timeInMinutes={commonInfo.timeInMinutes}
                        activeStack={activeStack}
                        onStartLabClick={this.onStartLabClick}
                        onFinishLabClick={this.onFinishLabClick}
                        labStarting={startingLab}
                        history={this.props.history}
                        provisonResources={provisonResources}
                        isAlreadyLab={!guideline || Boolean(existingTemplate) === false}
                        {...this.props}
                      />
                    )}
                  </div>
                  <div className="content-wrapper">
                    {storyContentItems != null
                      && storyContentItems.length > 1
                      && activeTab === TrainingLabIndex.BusinessStory && (
                      <TableContentLab
                        attachments={attachments}
                        itemTableContent={storyContentItems}
                        commonInfo={commonInfo}
                      />
                    )}

                    {trainingLabContentItems != null
                      && trainingLabContentItems.length > 1
                      && activeTab === TrainingLabIndex.TrainingLab && (
                      <TableContentLab
                        attachments={attachments}
                        itemTableContent={trainingLabContentItems}
                        commonInfo={commonInfo}
                      />
                    )}

                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div >
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(LabDetail);
