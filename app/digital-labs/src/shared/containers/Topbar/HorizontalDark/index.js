import akaClaudLogo from '@assets/images/logo.svg';
import { LANDING_URL } from '@common';
import { login } from 'aka-keycloak-login';
import UserMenu from '@components/UserMenu';
import { APPS_NAVIGATION } from '@constants';
import { Button, Layout, Popover } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';

import HorizontalNav from '../HorizontalNav';

const { Header } = Layout;

const HorizontalDark = () => {
  const user = useSelector(({ user }) => user);

  const isLoggedIn = (user) => user && user.roles;
  // const userRole = isLoggedIn(user) ? user.roles : '';
  return (
    <div className="akac-header-horizontal-dark">
      <Header className="gx-header-horizontal-main">
        <div className="gx-container">
          <div className="gx-header-horizontal-main-flex">
            <a
              href={LANDING_URL}
              target="_blank" rel="noopener noreferrer"
              className="gx-d-lg-block gx-pointer gx-mr-xs-5 gx-logo"
            >
              <img alt="" src={akaClaudLogo} />
            </a>
            <div className="akac-header-horizontal-nav">
              <div className="gx-header-horizontal-nav-flex">
                <HorizontalNav />
              </div>
            </div>
            <ul className="akac-header-notifications gx-ml-auto">
              <Popover
                placement="bottom"
                content={APPS_NAVIGATION.map((item, index) => (
                  <li className="gx-popover-notify" key={index}>
                    <a className="gx-pointer gx-d-block header-icon" href={item.url} target="_blank" rel="noopener noreferrer" >
                      {item.text}
                    </a>
                  </li>
                ))}
                trigger="click"
              >
                <div className="gx-logo-switch-app" />
              </Popover>
              <li className="gx-user-nav">
                {isLoggedIn(user) ? (
                  <UserMenu user={user} />
                ) : (
                  <Button className="login-btn" onClick={login}>
                    Login
                  </Button>
                )}
              </li>
            </ul>
          </div>
        </div>
      </Header>
    </div>
  );
};
export default HorizontalDark;
