import React from "react";
import { Row, Col, Carousel } from "antd";
import { VALUES } from "@constants/Landing"

const Values = () => (
    <Row className="values" justify="center">
        <Col xs={22} md={20}>
            <div className="values-title">
                <h2>Values We Can Bring</h2>
            </div>
            <Row gutter={{ md: 16 }} className="web-layout">
                {VALUES.map((value, i) => (
                    <Col xs={24} sm={24} md={12} lg={6} key={i}>
                        <div className="values-item">
                            <img src={value.image} alt={value.title} />
                            <h3>{value.title}</h3>
                            <p className="description">{value.description}</p>
                            <ul className="checklist">
                                {value.checklist.map((item, j) => (
                                    <li key={j}>{item}</li>
                                ))}
                            </ul>
                        </div>
                    </Col>
                ))}
            </Row>
            <Row className="mobile-layout">
                <Col xs={24} sm={24} md={12} lg={6}>
                    <Carousel autoplasy>
                        {VALUES.map((value, i) => (
                            <div className="values-item" key={i}>
                                <img className="aglity-icons" src={value.image} alt={value.title} />
                                <h3>{value.title}</h3>
                                <p className="description">{value.description}</p>
                                <ul className="checklist">
                                    {value.checklist.map((item, j) => (
                                        <li key={j}>{item}</li>
                                    ))}
                                </ul>
                            </div>
                        ))}
                    </Carousel>
                </Col>
            </Row>
        </Col>
    </Row>
);

export default Values;