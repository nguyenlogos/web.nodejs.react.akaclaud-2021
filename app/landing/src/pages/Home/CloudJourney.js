import React from "react";
import { Row, Col } from "antd";

const CloudJourney = () => (<>
    <Row className="journey journey-image" justify="center">
        <Col xs={24} md={18}>
            <div className="journey-image-title">
                <div className="title-container">
                    <h2 className="title-text">Cloud Journey with FPT Digital Kaizen&trade;</h2>
                </div>
                <p>Think Big - Start Smart - Scale Fast</p>
            </div>
            <img src="/landing/CSJ.svg" width="100%" alt="Cloud Journey with FPT Digital Kaizen" />
        </Col>
    </Row>
    <Row className="journey journey-text web-layout" justify="center">
        <Col span={9}>
            <div className="journey-text-item">
                <h6>Success with F500 companies</h6>
                <ul>
                    <li>THINK BIG by setting high target for Cloud & Digital initiatives</li>
                    <li>START SMART by quick win with reference solutions</li>
                    <li>SCALE FAST by replicating and optimizing solution to other workloads</li>
                </ul>
            </div>
        </Col>
        <Col span={9}>
            <div className="journey-text-item">
                <ul>
                    <li>Focus on critical mission (operational task and digital initiatives)</li>
                    <li>Resolve pain points and improve productivities</li>
                    <li>Multi-Cloud is default environment &amp; culture</li>
                    <li>Compliance by default &amp; security by design</li>
                </ul>
            </div>
        </Col>
    </Row>
    <Row className="journey journey-text mobile-layout">
        <Col span={24}>
            <div className="journey-text-item">
                <h6>Success with F500 companies</h6>
                <ul>
                    <li>THINK BIG by setting high target for Cloud & Digital initiatives</li>
                    <li>START SMART by quick win with reference solutions</li>
                    <li>SCALE FAST by replicating and optimizing solution to other workloads</li>
                    <li>Focus on critical mission (operational task and digital initiatives)</li>
                    <li>Resolve pain points and improve productivities</li>
                    <li>Multi-Cloud is default environment &amp; culture</li>
                    <li>Compliance by default &amp; security by design</li>
                </ul>
            </div>
        </Col>
    </Row>
</>);

export default CloudJourney;