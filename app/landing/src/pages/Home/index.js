import React from "react";
import { connect, useSelector } from "react-redux";
import Header from "./Header";
import Slider from "./Slider";
import Services from "./Services";
import CloudJourney from "./CloudJourney";
import Values from "./Values";
import Accelerators from "./Accelerators";
import Footer from "./Footer";
import "@styles/landing.less";
import Customers from "./Customers";

document.body.classList.add("full-scroll");

const LandingPage = () => {
    const user = useSelector((state) => state.user)
    return (
        <div className="landingpage">
            <Header user={user} />
            <Slider />
            <Services />
            <CloudJourney />
            <Values />
            <Accelerators />
            <Customers />
            <Footer />
        </div>
    );
};

const mapStateToProps = (state) => ({
    user: state.common,
  });
  
  export default connect(mapStateToProps, null)(LandingPage);
  