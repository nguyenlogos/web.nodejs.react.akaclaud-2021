import { DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZES } from '@constants';
import { Pagination } from 'antd';
import React, { Component } from 'react';

import FilterMenu from './FilterMenu';
import LabList from './LabList';

export default class labCatalog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: null,
      isFlexLayout: true,
      selectedItems: props.selectedItems,
    };
  }

  componentDidMount() {
    const { keyword, selectedItems } = this.state;
    this._$loadAllLabs(keyword, DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, selectedItems);
  }

  _$loadAllLabs = (keyword, page, pageSize, selectedItems) => {
    const { getLabs } = this.props;
    getLabs(keyword, page, pageSize, selectedItems);
  };

  onPageChanged = (page, pageSize) => {
    if (page > 0) {
      page = page - 1;
    }
    const { selectedItems, keyword } = this.state;
    this._$loadAllLabs(keyword, page, pageSize, selectedItems);
  };

  onPageSizeChanged = (current, pageSize) => {
    const { selectedItems, keyword } = this.state;
    this._$loadAllLabs(keyword, 0, pageSize, selectedItems);
  };

  onSelectedChange = (keyword, selectedItems) => {
    this.setState({ selectedItems, keyword }, () =>
      this._$loadAllLabs(keyword, DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, selectedItems)
    );
  };

  onChangeLayout = () => {
    const { isFlexLayout } = this.state;
    this.setState({ isFlexLayout: !isFlexLayout });
  };

  render() {
    const { onClick, count, labs, selectedItems } = this.props;
    const { isFlexLayout } = this.state;
    const mapSelectedItems = selectedItems
      ? selectedItems.map((i) => {
        return {
          type: 'category',
          key: i.value,
          objectId: i.value,
        };
      })
      : [];

    return (
      <>
        <FilterMenu
          selectedItems={mapSelectedItems}
          onSelectedChange={this.onSelectedChange}
          onChangeLayout={this.onChangeLayout}
          isFlexLayout={isFlexLayout}
        />
        {labs.length > 0 && <LabList labList={labs} isFlexLayout={isFlexLayout} onClick={onClick} />}
        <div className="labcatalog-footer">
          <Pagination
            total={count}
            defaultPageSize={DEFAULT_PAGE_SIZE}
            showSizeChanger={true}
            pageSizeOptions={DEFAULT_PAGE_SIZES}
            onChange={this.onPageChanged}
            onShowSizeChange={this.onPageSizeChanged}
            className="pagination-center"
          />
        </div>
      </>
    );
  }
}
