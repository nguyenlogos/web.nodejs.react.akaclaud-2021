import { saveAnswer } from '@api';
import { Radio } from 'antd';
import * as React from 'react';

export default class RateQuestion extends React.Component {
  subscriptions = [];
  constructor(props) {
    super(props);

    this.state = {
      answer: props.question.answer,
      showQuestion: true,
    };
  }
  componentWillUnmount() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onChange = (e) => {
    const { question, assessmentId } = this.props;
    let questionObject = {
      id: question.id,
      domain: question.domain,
      subDomain: question.subDomain,
      type: question.type,
      answer: e.target.value,
    };

    this.subscriptions.push(
      saveAnswer(assessmentId, questionObject).subscribe(
        () => {
          this.props.callbackFromParent(question);
          this.setState({ answer: e.target.value });
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      )
    );
  };

  _renderRateGuide(guide) {
    let arrGuide = guide.map((guideData, guideIndex) => {
      return (
        <div className="answer" key={guideData.points}>
          <Radio className="radio-button" value={guideData.points}>
            {guideData.points} -{guideData.guidetext}
          </Radio>
        </div>
      );
    });
    return <>{arrGuide}</>;
  }

  render() {
    const { answer } = this.state;
    const { question, unanswered } = this.props;

    return (
      <div className={unanswered === question.key ? 'unanswered' : null}>
        <div ref={this._questionRef} className="title-question">
          <div className="content-question">{question.question}</div>
          <Radio.Group className="radio" onChange={this.onChange} value={answer}>
            {this._renderRateGuide(question.guide)}
          </Radio.Group>
        </div>
      </div>
    );
  }
}
